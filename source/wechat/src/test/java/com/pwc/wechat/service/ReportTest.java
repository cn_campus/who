package com.pwc.wechat.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.pwc.wechat.config.spring.WebContext;
import com.pwc.wechat.dao.candidate.HybridDao;
import com.pwc.wechat.service.hr.report.SearchReport;
import com.pwc.wechat.util.JsonUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebContext.class)
@WebAppConfiguration
public class ReportTest {
	
	@Autowired
	private SearchReport reportGenerator;
	
	@Autowired
	private HybridDao hybridDao;
	
	@Test
	public void testReport(){
		System.out.println("");
	}

}
