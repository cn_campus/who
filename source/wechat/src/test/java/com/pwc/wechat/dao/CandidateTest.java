package com.pwc.wechat.dao;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Commit;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.config.spring.WebContext;
import com.pwc.wechat.entity.candidate.Candidate;
import com.pwc.wechat.entity.candidate.EducationExperience;
import com.pwc.wechat.entity.candidate.WorkExperience;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebContext.class)
@WebAppConfiguration
@Transactional
public class CandidateTest {

	
	@Rollback
	@Test
	public void testInsert() {
		Candidate app = new Candidate();
		app.setNationalId("366888");
		app.setCellPhone("rrr");
		app.setCurrentProvince("ee");
		app.setName("eee");
		app.setBirthday("20120202");
		app.setGender("male");
		app.setMailAddress("rrr");
	}
	
	@Commit
	@Test
	public void testUpdate() {
		Candidate app = new Candidate();
		app.setNationalId("3666");
		app.setCellPhone("rr");
		app.setCurrentProvince("e");
		app.setName("e");
		app.setBirthday("20120203");
		app.setGender("ma");
		app.setMailAddress("r");
	}
	
	@Commit
	@Test
	public void testFind() {
		Candidate app = new Candidate();
		app.setNationalId("310105101010100001");
	}
}