package com.pwc.wechat.dao;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.config.spring.WebContext;
import com.pwc.wechat.entity.candidate.WorkExperience;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebContext.class)
@WebAppConfiguration
@Transactional
public class WorkExperienceTest {

	
	@Commit
	@Test
	public void testInsert() {
		WorkExperience app = new WorkExperience();
		app.setNationalId("31010520160101000X");
		app.setExperienceId(123);
		app.setPosition("Intern");
		app.setPeriod("Success");
		app.setDetails("hahaha");
		app.setCompany("uu");
	}
	
	@Commit
	@Test
	public void testUpdate() {
		WorkExperience app = new WorkExperience();
		app.setNationalId("31010520160101000X");
		app.setExperienceId(123);
		app.setPosition("In");
		app.setPeriod("Su");
		app.setDetails("ha");
		app.setCompany("usst");
	}
	
	@Commit
	@Test
	public void testFind() {
		WorkExperience app = new WorkExperience();
		app.setNationalId("31010520160101000X");
		app.setExperienceId(123);
	}
}
