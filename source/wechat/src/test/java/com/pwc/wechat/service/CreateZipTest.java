package com.pwc.wechat.service;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.pwc.wechat.config.spring.WebContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebContext.class)
@WebAppConfiguration
public class CreateZipTest {
	

	@Test
	public void testNioService() {
		try {
		Path path = Paths.get("/XXX");
		System.out.println(path);
		System.out.println(System.getProperty("user.dir"));
		Resource resource = new ClassPathResource("/ResumeSample.xml");
		String addr;
			addr = Paths.get(resource.getFile().getAbsolutePath()).resolveSibling("ResumeDoc").toString();
			System.out.println(addr);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
