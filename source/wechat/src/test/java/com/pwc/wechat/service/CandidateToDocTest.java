package com.pwc.wechat.service;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.pwc.wechat.config.spring.WebContext;
import com.pwc.wechat.entity.candidate.Candidate;
import com.pwc.wechat.service.export.writer.CandidateToDocx;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebContext.class)
@WebAppConfiguration
public class CandidateToDocTest {

	@Autowired
	private CandidateToDocx candidateToDocTest;

	@Test
	public void testDocExport() {
		Candidate candidate = new Candidate();
		candidate.setGender("Male");
		candidate.setName("OwenChen");
		candidate.setNationalId("3101051111111");
		candidate.setNationality("China");
		candidate.setBirthProvince("Shanghai");
		candidate.setCellPhone("138888888");
		candidate.setHomePhone("021-65552322");
		candidate.setCurrentProvince("Shanghai");
		candidate.setMailAddress("owen@owen.com");
		candidate.setPersonalPage("www.owen.com");
		candidate.setSkillsDescribe("A skill, B skill, C skill");
		Path resumeDoc = Paths.get("C:/ResumeDoc");
	}
	
	@Test
	public void testPath() {
		String[] nationalIds = new String[]{"310105101010100001","310105101010100002"};
		System.out.println(nationalIds.toString());
		Path path = Paths.get("D:/Resume");
		path.resolve("anotherFile");
		System.out.println(path);
		System.out.println(path.resolve("anotherFile"));
	}
	
	@Test
	public void testClass(){
		Resource resource = new ClassPathResource("/ResumeSample.xml");
			try {
				System.out.println(resource.getFile().getAbsolutePath());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	

}
