package com.pwc.wechat.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.config.spring.WebContext;
import com.pwc.wechat.dao.candidate.HybridDao;
import com.pwc.wechat.entity.candidate.Candidate;
import com.pwc.wechat.entity.candidate.WorkExperience;
import com.pwc.wechat.util.DateUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebContext.class)
@WebAppConfiguration
public class HibernateTemplateTest {

	@Autowired
	private HybridDao hybridDao;
	
	@Test
	@Commit
	@Transactional(readOnly = false)
	public void testSaveOrUpdate() {
		WorkExperience app1 = new WorkExperience();
		app1.setNationalId("310105101010100001");
		app1.setExperienceId(4);
		app1.setPosition("Intern");
		app1.setPeriod("Success");
		app1.setDetails("hahaha");
		app1.setCompany("uu");
	}
	
	@Test
	@Commit
	@Transactional(readOnly = false)
	public void updateVer() {
	}
	
	@Test
	public void findCan() {
	}
	
	@Test
	public void random() {
		
	}
}
