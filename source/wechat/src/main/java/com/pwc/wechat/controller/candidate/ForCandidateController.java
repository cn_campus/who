package com.pwc.wechat.controller.candidate;

import com.pwc.wechat.entity.candidate.Candidate;
import com.pwc.wechat.entity.candidate.CandidateAllInfo;
import com.pwc.wechat.entity.candidate.QuestAndAnswer;
import com.pwc.wechat.service.candidate.IApplicationHandler;
import com.pwc.wechat.service.candidate.ICandidateHandler;
import com.pwc.wechat.service.candidate.IQuestionnaireHandler;
import com.pwc.wechat.service.upload.IUploadAndProcess;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
@RequestMapping(value = "/forCandidate")
public class ForCandidateController {

    private static final Logger logger = LogManager.getLogger(ForCandidateController.class);

    @Autowired
    private ICandidateHandler candidateService;

    @Autowired
    private IQuestionnaireHandler questionnaireService;

    @Autowired
    private IUploadAndProcess<String> candidatePhotoService;

    @Autowired
    private IApplicationHandler iApplicationHandler;

    @RequestMapping(value = {"", "/"}, produces = "text/html; charset=utf-8")
    public ModelAndView visitCandidatePage(@RequestParam(defaultValue = "") String code) throws IOException {

        logger.info("Welcome");
        logger.info("code:{}", code);
        ModelAndView candidatePage = new ModelAndView("candidate/candidate");
        return candidatePage;
    }

    @RequestMapping(value = "/showQuestionnaire", produces = "text/html; charset=utf-8")
    public ModelAndView showQuestionnaire(@RequestParam String nationalId) throws IOException {

        logger.info("Welcome Questionnaire Page");
        ModelAndView questionnairePage = new ModelAndView("hr/questionnaireWechat");
        questionnairePage.addObject("nationalId", nationalId);
        return questionnairePage;
    }

    @RequestMapping(value = "/getQuestions", produces = "application/json; charset=utf-8")
    public QuestAndAnswer getQuestions(@RequestParam String language) {

        logger.info("get question");
        return questionnaireService.searchQuestAndAnswer(language);
    }

    @RequestMapping(value = "/insertQuestionnaire", produces = "text/html; charset=utf-8")
    public String insertQuestionnaire(@RequestBody QuestAndAnswer questAndAnswer) {

        questionnaireService.saveQuestAndAnswer(questAndAnswer);
        return "success";
    }

    @RequestMapping(value = "/sendInfo", produces = "text/html; charset=utf-8")
    public String insertApplicationInfo(@RequestBody Candidate candidate, @RequestParam String position, @RequestParam(required = false) String source) {

        candidate.setSource(source);
        return String.valueOf(candidateService.saveCandidateFromPage(candidate, position));
    }

    @RequestMapping(value = "/updateInfo", produces = "text/html; charset=utf-8")
    public String updateInfo(@RequestBody CandidateAllInfo candidateinfo) {

        //candidate.setSource("hrm input");
        candidateinfo.getApplication().setNationalId(candidateinfo.getCandidate().getNationalId());
        candidateinfo.getQuestAndAnswer().setAnswer(candidateinfo.getCandidate().getNationalId());
        if (candidateService.saveOrUpdateCandidate(candidateinfo.getCandidate())) {
            questionnaireService.saveQuestAndAnswer(candidateinfo.getQuestAndAnswer());
            return String.valueOf(iApplicationHandler.updateApplication(candidateinfo.getApplication()));
        }
        return String.valueOf(false);
    }

    @RequestMapping(value = "/searchWechatUid", produces = "application/json; charset=utf-8")
    public Candidate searchWechatUid(@RequestParam(required = false) String wechatUid) {

        return candidateService.findCandidateByWechatUid(wechatUid);
    }

    @RequestMapping(value = "/uploadPhoto", produces = "text/html; charset=utf-8")
    public String uploadPhoto(@RequestParam(value = "nationalId") String nationalId, HttpServletRequest request) {

        logger.info("upload candidate photo");
        logger.info("nationalId is " + nationalId);
        if (candidatePhotoService.getUploadFile(request, nationalId).equals("success"))
            return "success";
        else
            return "failed";
    }
}
