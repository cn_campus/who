package com.pwc.wechat.service.hr.round;

import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.entity.hr.MultiCondition;
import com.pwc.wechat.entity.hr.TalentDataSet;
import com.pwc.wechat.entity.report.ReportConfig;

public interface IMultiSearch<T, M extends MultiCondition> {

	@Transactional(readOnly = true)
	public T searchByMultiCondition(M multiCondition);
	
	@Transactional(readOnly = true)
	public T searchAllByMultiCondition(M multiCondition);

	
}
