package com.pwc.wechat.dao.hr;

import com.pwc.wechat.entity.user.Avatar;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IAvatarCrud
        extends JpaRepository<Avatar, String> {

}
