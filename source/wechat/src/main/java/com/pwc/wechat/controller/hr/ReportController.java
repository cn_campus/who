package com.pwc.wechat.controller.hr;

import com.pwc.wechat.entity.report.Report;
import com.pwc.wechat.entity.report.ReportColumn;
import com.pwc.wechat.entity.report.ReportConditionSet;
import com.pwc.wechat.entity.report.ReportConfig;
import com.pwc.wechat.service.hr.report.IReportConfigSearch;
import com.pwc.wechat.service.hr.report.IReportConfigUpdate;
import com.pwc.wechat.service.hr.round.IMultiSearch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping(value = "forHr")
public class ReportController {

    private static final Logger logger = LogManager.getLogger(ReportController.class);

    @Autowired
    private IMultiSearch<Report, ReportConditionSet> searchReport;

    @Autowired
    private IMultiSearch<Report, ReportConditionSet> searchGroupEvalReport;

    @Autowired
    private IReportConfigSearch iReportConfigSearch;

    @Autowired
    private IReportConfigUpdate iReportConfigUpdate;

    @RequestMapping(value = "/reportlist", produces = "text/html; charset=utf-8")
    public ModelAndView reportModelAndView(HttpServletRequest request) {

        return new ModelAndView("hr/report");
    }

    @RequestMapping(value = "/getReportColumn", produces = "application/json; charset=utf-8")
    public List<ReportConfig> getReportColumn(HttpServletRequest request) {

        return iReportConfigSearch.searchReportColumn();
    }

    @RequestMapping(value = "/getAllColumns", produces = "application/json; charset=utf-8")
    public ReportColumn[] getAllColumns(HttpServletRequest request) {

        return ReportColumn.values();
    }

    @RequestMapping(value = "/confirmReportConfig", produces = "text/html; charset=utf-8")
    public String confirmReportConfig(@RequestBody ReportConfig reportConfig) {

        iReportConfigUpdate.updateReportConfig(reportConfig);
        logger.info("Confirm ReportConfig", reportConfig.getTitle());
        return reportConfig.getTitle() + "success";
    }

    @RequestMapping(value = "/genReport", produces = "application/json; charset=utf-8")
    public Report queryReport(@RequestBody ReportConditionSet reportConditionSet) {
        for (ReportConfig config : reportConditionSet.getReportConfigs()) {
            if (config.getCheck() != null && config.getCheck() && config.getTitle().equals("Group Interview Evaluation Sheet")) {
                return searchGroupEvalReport.searchByMultiCondition(reportConditionSet);
            }
        }
        return searchReport.searchByMultiCondition(reportConditionSet);
    }
}
