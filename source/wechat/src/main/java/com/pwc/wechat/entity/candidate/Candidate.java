package com.pwc.wechat.entity.candidate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "candidate", schema = "wechat")
public class Candidate implements Serializable {

    private static final long serialVersionUID = 1094146326556193345L;

    @Id
    @Column(name = "national_id")
    private String nationalId;

    @Column(name = "name")
    private String name;

    @Column(name = "gender")
    private String gender;

    @Column(name = "wechat_uid")
    private String wechatUid;

    @Column(name = "current_province")
    private String currentProvince;

    @Column(name = "birth_province")
    private String birthProvince;

    @Column(name = "birthday")
    private String birthday;// yyyy-MM-dd

    @Column(name = "nationality")
    private String nationality;

    @Column(name = "passport_number")
    private String passportNumber;

    @Column(name = "home_phone")
    private String homePhone;

    @Column(name = "cell_phone")
    private String cellPhone;

    @Column(name = "mail_address")
    private String mailAddress;

    @Column(name = "personal_page")
    private String personalPage;

    @Column(name = "skills_describe")
    private String skillsDescribe;

    @Column(name = "graduate_plan")
    private String graduatePlan;

    @Column(name = "available_schedule")
    private String availableSchedule;

    @Column(name = "language_level")
    private String languageLevel;

    @Column(name = "source")
    private String source;

    @Column(name = "opt_time")
    private String optTime;

    @Column(name = "graduate_date")
    private String graduateDate;// yyyy-MM-dd

    @Transient
    private List<WorkExperience> workExperiences;

    @Transient
    private List<EducationExperience> educationExperiences;

    public String getNationalId() {
        return nationalId;
    }

    public String getGraduateDate() {
        return graduateDate;
    }

    public void setGraduateDate(String graduateDate) {
        this.graduateDate = graduateDate;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWechatUid() {
        return wechatUid;
    }

    public void setWechatUid(String wechatUid) {
        this.wechatUid = wechatUid;
    }

    public String getCurrentProvince() {
        return currentProvince;
    }

    public void setCurrentProvince(String currentProvince) {
        this.currentProvince = currentProvince;
    }

    public String getBirthProvince() {
        return birthProvince;
    }

    public void setBirthProvince(String birthProvince) {
        this.birthProvince = birthProvince;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getMailAddress() {
        return mailAddress;
    }

    public void setMailAddress(String mailAddress) {
        this.mailAddress = mailAddress;
    }

    public String getPersonalPage() {
        return personalPage;
    }

    public void setPersonalPage(String personalPage) {
        this.personalPage = personalPage;
    }

    public String getSkillsDescribe() {
        return skillsDescribe;
    }

    public void setSkillsDescribe(String skillsDescribe) {
        this.skillsDescribe = skillsDescribe;
    }

    public String getGraduatePlan() {
        return graduatePlan;
    }

    public void setGraduatePlan(String graduatePlan) {
        this.graduatePlan = graduatePlan;
    }

    public String getAvailableSchedule() {
        return availableSchedule;
    }

    public void setAvailableSchedule(String availableSchedule) {
        this.availableSchedule = availableSchedule;
    }

    public String getLanguageLevel() {
        return languageLevel;
    }

    public void setLanguageLevel(String languageLevel) {
        this.languageLevel = languageLevel;
    }

    public List<WorkExperience> getWorkExperiences() {
        return workExperiences;
    }

    public void setWorkExperiences(List<WorkExperience> workExperiences) {
        this.workExperiences = workExperiences;
    }

    public List<EducationExperience> getEducationExperiences() {
        return educationExperiences;
    }

    public void setEducationExperiences(List<EducationExperience> educationExperiences) {
        this.educationExperiences = educationExperiences;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getOptTime() {
        return optTime;
    }

    public void setOptTime(String optTime) {
        this.optTime = optTime;
    }

}
