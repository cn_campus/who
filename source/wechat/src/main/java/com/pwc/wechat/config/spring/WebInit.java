package com.pwc.wechat.config.spring;

import com.pwc.wechat.config.constant.BaseConst;
import com.pwc.wechat.util.CommonUtil;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

@Order(Ordered.LOWEST_PRECEDENCE)
public class WebInit implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext container)
            throws ServletException {

        // Create the 'root' Spring application context
        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
        rootContext.scan("com.pwc.wechat.config.spring");
        // Manage the lifecycle of the root application context
        container.addListener(new ContextLoaderListener(rootContext));
        // Listener that exposes the request to the current thread
        container.addListener(new RequestContextListener());
        // Create the dispatcher servlet's Spring application context
        AnnotationConfigWebApplicationContext dispatcherContext = new AnnotationConfigWebApplicationContext();
        // dispatcherContext.register(com.travelsky.ray.config.spring.BeanConfig.class);
        // XmlWebApplicationContext dispatcherContext = new
        // XmlWebApplicationContext();
        // dispatcherContext.setConfigLocation("/resource/applicationContext.xml");
        // Register and map the dispatcher servlet
        DispatcherServlet dispatcherServlet = new DispatcherServlet(dispatcherContext);
        dispatcherServlet.setThrowExceptionIfNoHandlerFound(true);//for 404 exception
        ServletRegistration.Dynamic registration = container.addServlet("dispatcher", dispatcherServlet);
        registration.setMultipartConfig(CommonUtil.genUploadConfig(container.getRealPath(BaseConst.UPLOAD_FOLDER)));//8Mb
        registration.setLoadOnStartup(1);
        registration.addMapping("/");
    }
}