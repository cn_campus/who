package com.pwc.wechat.service.export;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.Assert;

import com.pwc.wechat.service.export.writer.IEntityWriter;
import com.pwc.wechat.util.JsonUtil;

public abstract class AbstractExport<T, O extends OutputStream> implements IExportByNationalIds {
	
	private static final Logger logger = LogManager.getLogger(AbstractExport.class);
	
	abstract protected void initResponse(HttpServletResponse response);
	
	abstract protected T searchByNationalId(String nationalId);
	
	abstract protected O getOutputStream(HttpServletResponse response) throws IOException;
	
	abstract protected IEntityWriter<T> initWriter();
	
	@Override
	final public void executeFlush(String[] nationalIds, HttpServletResponse response) throws Exception {
		Assert.notNull(nationalIds, "NATIONAL_IDS MUST NOT BE NULL");
		logger.info("Export File");
		this.initResponse(response);
		O outputStream = this.getOutputStream(response);
		this.writeAll(outputStream, this.queryEntityList(nationalIds));
		outputStream.close();
		response.flushBuffer();
		logger.info("Export Done");
	}
	
	protected void writeAll(O outputStream, List<T> entityList) throws Exception {
		entityList.stream().filter(entity -> entity != null).forEach(entity -> {
			try {
				writeOne(outputStream, entity);
			} catch (Exception e) {
				logger.info("Write Entity Failed {}", JsonUtil.toNormalJson(entity), e);
			}
		});
	}
	
	protected void writeOne(O outputStream, T entity) throws Exception {
		
	}
	
	protected List<T> queryEntityList(String[] nationalIds) {
		List<T> entityList = new ArrayList<T>();
		Arrays.stream(nationalIds).filter(id -> id != null && !id.isEmpty())
				.forEach(id -> entityList.add(this.searchByNationalId(id)));
		return entityList;
	}
	
	
}
