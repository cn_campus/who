package com.pwc.wechat.service.hr.report;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.entity.report.ReportConfig;

public interface IReportConfigSearch {
	
	@Transactional(readOnly = true)
	public List<ReportConfig> searchReportColumn();

}
