package com.pwc.wechat.dao.candidate;

import com.pwc.wechat.entity.candidate.WorkExperience;
import com.pwc.wechat.entity.candidate.WorkExperiencePk;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IWorkExpCrud
        extends JpaRepository<WorkExperience, WorkExperiencePk> {

    public List<WorkExperience> findAllByNationalId(
            String nationalId);

    public void deleteAllByNationalId(
            String nationalId);

}
