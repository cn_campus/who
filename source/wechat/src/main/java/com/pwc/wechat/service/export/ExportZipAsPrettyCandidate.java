package com.pwc.wechat.service.export;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pwc.wechat.entity.candidate.Candidate;
import com.pwc.wechat.service.candidate.ICandidateHandler;
import com.pwc.wechat.service.export.writer.IEntityWriter;

@Service("exportZipAsPrettyCandidate")
public class ExportZipAsPrettyCandidate extends AbstractExportZip<Candidate> {
	
	@Autowired
	private ICandidateHandler candidateService;
	
	@Autowired
	private IEntityWriter<Candidate> candidateToPrettyDocx;

	@Override
	protected Candidate searchByNationalId(String nationalId) {
		return candidateService.findCandidateByNationalId(nationalId);
	}

	@Override
	protected String getFileName(Candidate entity) {
		return entity.getName() + entity.getNationalId();
	}

	@Override
	protected IEntityWriter<Candidate> initWriter() {
		return candidateToPrettyDocx;
	}

}
