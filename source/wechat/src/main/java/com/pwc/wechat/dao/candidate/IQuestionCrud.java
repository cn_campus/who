package com.pwc.wechat.dao.candidate;

import com.pwc.wechat.entity.candidate.Questions;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IQuestionCrud
        extends JpaRepository<Questions, Long> {

}
