package com.pwc.wechat.service.mail;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.file.Path;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class JavaMail {
	public static class MailSenderinfo {
		// 发送邮件的服务器的IP和端口
		//private String mailServerHost;
		private String mailServerHost = "smtp.gmail.com";
//		private String mailServerPort = "25";
		private String mailServerPort = "465";
		// 邮件发送者的地址
		private String fromAddress;
		// 邮件接收者的地址
		private String toAddress;
		// 登陆邮件发送服务器的用户名和密码
		private String userName;
		private String password;
		// 是否需要身份验证
		private boolean validate = false;
		// 邮件主题
		private String subject;
		// 邮件的文本内容
		private String content;
		// 邮件附件的文件名
		private String[] attachFileNames;

		/**
		 * 获得邮件会话属性
		 */
		public Properties getProperties() {
			Properties p = new Properties();
			p.put("mail.smtp.host", this.mailServerHost);
			p.put("mail.smtp.port", this.mailServerPort);
			p.put("mail.smtp.auth", validate ? "true" : "false");
			p.put("mail.smtp.starttls.enable", "true");
			p.put("mail.smtp.auth", "true");
			p.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			// p.put("mail.smtp.socketFactory.fallback", "false");
			// p.put("mail.smtp.socketFactory.class",
			// "javax.net.ssl.SSLSocketFactory");
			return p;
		}

		public String getMailServerHost() {
			return mailServerHost;
		}

		public void setMailServerHost(String mailServerHost) {
			this.mailServerHost = mailServerHost;
		}

		public String getMailServerPort() {
			return mailServerPort;
		}

		public void setMailServerPort(String mailServerPort) {
			this.mailServerPort = mailServerPort;
		}

		public boolean isValidate() {
			return validate;
		}

		public void setValidate(boolean validate) {
			this.validate = validate;
		}

		public String[] getAttachFileNames() {
			return attachFileNames;
		}

		public void setAttachFileNames(String[] fileNames) {
			this.attachFileNames = fileNames;
		}

		public String getFromAddress() {
			return fromAddress;
		}

		public void setFromAddress(String fromAddress) {
			this.fromAddress = fromAddress;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getToAddress() {
			return toAddress;
		}

		public void setToAddress(String toAddress) {
			this.toAddress = toAddress;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

		public String getSubject() {
			return subject;
		}

		public void setSubject(String subject) {
			this.subject = subject;
		}

		public String getContent() {
			return content;
		}

		public void setContent(String textContent) {
			this.content = textContent;
		}
	}

	public static class MyAuthenticator extends Authenticator {
		String userName = null;
		String password = null;

		public MyAuthenticator() {
		}

		public MyAuthenticator(String username, String password) {
			this.userName = username;
			this.password = password;
		}

		protected PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(userName, password);
		}
	}

	public static class SimpleMailSender {
		/**
		 * 以文本格式发送邮件
		 * 
		 * @param mailInfo
		 *            待发送的邮件的信息
		 */
		public boolean sendTextMail(MailSenderinfo mailInfo) {
			// 判断是否需要身份认证
			MyAuthenticator authenticator = null;
			Properties pro = mailInfo.getProperties();
			// pro.put("mail.smtp.auth", "true");
			if (mailInfo.isValidate()) {
				// 如果需要身份认证，则创建一个密码验证器
				authenticator = new MyAuthenticator(mailInfo.getUserName(), mailInfo.getPassword());
			}
			// 根据邮件会话属性和密码验证器构造一个发送邮件的session
			Session sendMailSession = Session.getDefaultInstance(pro, authenticator);
			try {
				// MimeMessage mimeMessage = new MimeMessage(sendMailSession);
				// 根据session创建一个邮件消息
				Message mailMessage = new MimeMessage(sendMailSession);
				// 创建邮件发送者地址
				Address from = new InternetAddress(mailInfo.getFromAddress());
				// 设置邮件消息的发送者
				mailMessage.setFrom(from);
				// 创建邮件的接收者地址，并设置到邮件消息中
				Address to = new InternetAddress(mailInfo.getToAddress());
				mailMessage.setRecipient(Message.RecipientType.TO, to);
				// 设置邮件消息的主题
				mailMessage.setSubject(mailInfo.getSubject());
				// 设置邮件消息发送的时间
				mailMessage.setSentDate(new Date());
				// 设置邮件消息的主要内容
				String mailContent = mailInfo.getContent();
				mailMessage.setText(mailContent);
				// 发送邮件
				Transport.send(mailMessage);
				return true;
			} catch (MessagingException ex) {
				ex.printStackTrace();
			}
			return false;
		}

		/**
		 * 以HTML格式发送邮件
		 * 
		 * @param mailInfo
		 *            待发送的邮件信息
		 */

		/**
		 * 以文本格式发送邮件
		 * 
		 * @param mailInfo
		 *            待发送的邮件的信息
		 *
		 *            /** 以HTML格式发送邮件
		 * @param mailInfo
		 *            待发送的邮件信息
		 */
		public int sendMimeMail(MailSenderinfo mailInfo,List<Path> resource) throws IOException {
			// 判断是否需要身份认证
			AtomicInteger failNum = new AtomicInteger(0);
			MyAuthenticator authenticator = null;
			Properties pro = mailInfo.getProperties();
			 pro.put("mail.smtp.auth", "true");
			if (mailInfo.isValidate()) {
				// 如果需要身份认证，则创建一个密码验证器
				authenticator = new MyAuthenticator(mailInfo.getUserName(), mailInfo.getPassword());
			}
			// 根据邮件会话属性和密码验证器构造一个发送邮件的session
			Session sendMailSession = Session.getDefaultInstance(pro, authenticator);
			try {
				MimeMessage mimeMessage = new MimeMessage(sendMailSession);
				//mimeMessage.setSubject("Face-to-Face Interview Invitation from PwC SDC Shanghai- 6/8/2016 (Wednesday) -14:45 ", "UTF-8");
				//mimeMessage.setSubject("等我等我", "UTF-8");
				mimeMessage.setSubject(mailInfo.getSubject(),"utf-8");
				
				Address from = new InternetAddress(mailInfo.getFromAddress());
				// 设置邮件消息的发送者
				mimeMessage.setFrom(from);
				// 设置回复栏显示的回复人地址，不设置默认回复人就是发件人

				// 设置收件人
				Address to = new InternetAddress(mailInfo.getToAddress());
				mimeMessage.setRecipient(RecipientType.TO,
						to);
				MimeMultipart mimeMultipart = new MimeMultipart("mixed");

//				MimeBodyPart attch1 = new MimeBodyPart();
				MimeBodyPart attch2 = new MimeBodyPart();
				MimeBodyPart content = new MimeBodyPart();
//				mimeMultipart.addBodyPart(attch1);
				
				mimeMultipart.addBodyPart(content);
//				mimeMessage.setContent(mimeMultipart);

				// 构建附件1
				//DataSource ds1 = new FileDataSource("src/main/webapp/resources/images/pwcemail.jpg");
				mimeMessage.setContent(mimeMultipart);
				resource.forEach(one->{
					MimeBodyPart attch1 = new MimeBodyPart();
					DataHandler dataHandler1 = null;
					DataSource ds2 = null;
//					Resource img = new ClassPathResource("pwcemail.jpg");
					try {
						ds2= new FileDataSource(one.toFile());
						dataHandler1 =  new DataHandler(ds2);
						attch1.setDataHandler(dataHandler1);
						//attch1.setFileName(one.getFileName().toString());
						attch1.setFileName(MimeUtility.encodeText(one.getFileName().toString(),"utf-8","B"));
//						attch1.setFileName(one.getFileName());
						mimeMultipart.addBodyPart(attch1);
						
					} catch (MessagingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						failNum.getAndAdd(1);
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						failNum.getAndAdd(1);
					}
					
					//attch1.setFileName(MimeUtility.encodeText("map.jpg"));
//					mimeMultipart.addBodyPart(content);
					
					
				});
				Resource img = new ClassPathResource("pwcemail.jpg");
				try {
					DataSource ds2 = new FileDataSource(img.getFile().getAbsolutePath());
					DataHandler dataHandler1 = new DataHandler(ds2);
					attch2.setDataHandler(dataHandler1);
					attch2.setFileName(MimeUtility.encodeText("map.jpg"));
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					failNum.getAndAdd(1);
				}
				mimeMultipart.addBodyPart(attch2);
				// 构建附件2
//				DataSource ds2 = new FileDataSource("C:\\David Dai\\Java类加载器机制.txt");
//				DataHandler dataHandler2 = new DataHandler(ds2);
//				attch2.setDataHandler(dataHandler2);
//				try {
//					attch2.setFileName(MimeUtility.encodeText("Java类加载器机制.txt"));
//				} catch (UnsupportedEncodingException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}

				// 构建正文部分
				MimeMultipart bodyMultipart = new MimeMultipart("related");
				content.setContent(bodyMultipart);
				// html部分
				MimeBodyPart htmlPart = new MimeBodyPart();
				htmlPart.setContent(mailInfo.getContent(), "text/html;charset=utf-8");
				// 图片部分
				//MimeBodyPart picPart = new MimeBodyPart();
				bodyMultipart.addBodyPart(htmlPart);
				//bodyMultipart.addBodyPart(picPart);
				// 构建图片资源
				//byte[] imgbytes = getBytesFromFile(new File("D:\\aweil.jpg"));
//				byte[] imgbytes = getBytesFromFile(new File("src/main/webapp/resources/images/pwcemail.jpg"));
//				
//				DataSource picds = new ByteArrayDataSource(imgbytes, "application/octet-stream");
//				DataHandler picDataHandler = new DataHandler(picds);
//				picPart.setDataHandler(picDataHandler);
//				picPart.setFileName("pwcemail.jpg");
//				picPart.setHeader("Content-ID", "pwcemail");
				//picPart.setDisposition(disposition);
				// picPart.setHeader("Content-Location", "");
				// picPart.setHeader("Content-Location", "");
//				htmlPart.setContent(
//						"<p>"+mailInfo.getContent()
//						+ "<br style=\"font-size:12.8px\"><br style=\"font-size:12.8px\"><font face=\"sans-serif\" size=\"2\">Thank you for your interest in PwC China Service Delivery Center.</font><span style=\"font-size:12.8px\">&nbsp;</span><br style=\"font-size:12.8px\"><font face=\"sans-serif\" size=\"2\">We would like to invite you to attend the Face-to-face Interview for the position of<b>&nbsp;&nbsp;Admin Intern</b></font><font face=\"sans-serif\" size=\"2\">&nbsp;in Shanghai office. Below are the details for the interview:</font><span style=\"font-size:12.8px\">&nbsp;</span><br style=\"font-size:12.8px\"><font face=\"sans-serif\" size=\"2\">Date: 6/8/2016 (Wednesday)</font><font face=\"Arial\" size=\"2\">&nbsp;</font><font face=\"sans-serif\" size=\"2\">Time: 14:45 p.m.</font><br style=\"font-size:12.8px\"><font face=\"sans-serif\" size=\"2\">Venue: 上海市浦东新区金科路2889号长泰广场办公楼B座6楼PwC</font><span style=\"font-size:12.8px\">&nbsp;</span><br style=\"font-size:12.8px\"><font face=\"sans-serif\" size=\"2\">Transportation: 地铁二号线至金科路站 4 号出口左转直走100米，上电梯即到。</font><span style=\"font-size:12.8px\">&nbsp;</span><br style=\"font-size:12.8px\"><br style=\"font-size:12.8px\"><font face=\"sans-serif\" size=\"2\">To Do List:</font><span style=\"font-size:12.8px\">&nbsp;</span><br style=\"font-size:12.8px\"><br style=\"font-size:12.8px\"><font face=\"sans-serif\" size=\"2\">1) Please reply to this email by stating \"<u>&nbsp;I confirm that I will attend the interview at the PwC SDC office</u>\".</font><span style=\"font-size:12.8px\">&nbsp;</span></p>"
//						+"<br/>"
//						+"<font face=\"sans-serif\" size=\"2\"><b>You can find our location in the map below attachments.</b></font>"
//						
//						+"<font face=\"sans-serif\" size=\"2\">If you have any question, please feel free to contact me at: TEL:&nbsp;<a href=\"tel:021-60366800\" value=\"+862160366800\" target=\"_blank\">021-60366800</a>×6307.<br><br>Looking forward to meeting you soon.&nbsp;</font>"
//						,"text/html;charset=utf-8"
//						);
				htmlPart.setContent(mailInfo.getContent(), "text/html;charset=utf-8");
//				String aString  = "<div style="text-align: justify;">wef<strong>2d2d</span>wef<strong>wefwef<span style="font-style: italic; font-weight: bold;">wefwef</span><span style="font-style: italic; font-weight: bold; text-decoration: underline;">wefwe</span><span style="font-family: Arial; font-style: italic; font-weight: bold; text-decoration: underline;">wefwef</span></strong><span style="font-family: Arial; font-style: italic; font-weight: bold; text-decoration: underline; font-size: large;">wefwefw</strong>efwe</div>";
				//Transport.send(mailMessage);
				mimeMessage.saveChanges();
				   //开始发送
				  Transport.send(mimeMessage);
				return failNum.get();
			} catch (MessagingException ex) {
				ex.printStackTrace();
				failNum.getAndAdd(1);
				return failNum.get();
			}
		}
		
		
	

	
		public static byte[] getBytesFromFile(File f) {
	         if (f == null) {
	             return null;
	         }
	         try {
	             InputStream stream = new FileInputStream(f);
	             ByteArrayOutputStream out = new ByteArrayOutputStream(1000);
	             byte[] b = new byte[1000];
	             int n;
	             while ((n = stream.read(b)) != -1) {
	                 out.write(b, 0, n);
	             }
	             stream.close();
	             out.close();
	             return out.toByteArray();
	         } catch (IOException e){
	         }
	         return null;
	     }
	 }
}

