package com.pwc.wechat.entity.hr;

import java.util.List;

public class TalentEmail extends TalentDataSet {
	
	private String Context;
	
	private String Subject;
	
	private List<TalentFile> attachment;
	
	
	public String getContext() {
		return Context;
	}

	public void setContext(String context) {
		Context = context;
	}

	public String getSubject() {
		return Subject;
	}

	public void setSubject(String subject) {
		Subject = subject;
	}

	public List<TalentFile> getAttachment() {
		return attachment;
	}

	public void setAttachment(List<TalentFile> attachment) {
		this.attachment = attachment;
	}

}
