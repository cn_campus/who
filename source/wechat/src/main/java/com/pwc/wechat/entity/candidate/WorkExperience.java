package com.pwc.wechat.entity.candidate;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "work_experience")
@IdClass(WorkExperiencePk.class)
public class WorkExperience implements Serializable {

	private static final long serialVersionUID = 6689484553013388134L;

	@Id
	@Column(name = "national_id")
	private String nationalId;

	@Id
	@Column(name = "experience_id")
	private Integer experienceId;

	@Column(name = "company")
	private String company;

	@Column(name = "position")
	private String position;

	@Column(name = "period")
	private String period;

	@Column(name = "details")
	private String details;

	@Column(name = "last_exp")
	private Boolean lastExp;

	@Column(name = "opt_time")
	private String optTime;

	public String getNationalId() {
		return nationalId;
	}

	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}

	public Integer getExperienceId() {
		return experienceId;
	}

	public void setExperienceId(Integer experienceId) {
		this.experienceId = experienceId;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public Boolean getLastExp() {
		return lastExp;
	}

	public void setLastExp(Boolean lastExp) {
		this.lastExp = lastExp;
	}

	public String getOptTime() {
		return optTime;
	}

	public void setOptTime(String optTime) {
		this.optTime = optTime;
	}

}
