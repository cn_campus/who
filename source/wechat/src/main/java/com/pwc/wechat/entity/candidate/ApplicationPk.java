package com.pwc.wechat.entity.candidate;

import java.io.Serializable;

public class ApplicationPk implements Serializable {

    private static final long serialVersionUID = 3888463543589069696L;

    public ApplicationPk() {
        super();
    }

    public ApplicationPk(String applyDate, Integer applyId) {
        super();
        this.applyDate = applyDate;
        this.applyId = applyId;
    }

    private String applyDate;// yyyy-MM-dd
    private Integer applyId;

    public String getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(String applyDate) {
        this.applyDate = applyDate;
    }

    public Integer getApplyId() {
        return applyId;
    }

    public void setApplyId(Integer applyId) {
        this.applyId = applyId;
    }

    @Override
    public int hashCode() {
        // TODO Auto-generated method stub
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {

        if (obj instanceof ApplicationPk) {
            return ((ApplicationPk) obj)
                    .applyDate.equals(this.applyDate) && ((ApplicationPk) obj)
                    .applyId.equals(this.applyId) ? true : false;
        }
        return false;
    }
}
