package com.pwc.wechat.config.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Component
public class AuthenticationFailureHandlerImpl
        extends SimpleUrlAuthenticationFailureHandler {

    @Autowired
    private LoginAttemptService loginAttemptService;

    @Override
    public void onAuthenticationFailure(
            HttpServletRequest request,
            HttpServletResponse response,
            AuthenticationException exception)
            throws IOException,
            ServletException {

        Map<String, Integer> hrAttemptAccountMap = loginAttemptService.getHrAttemptAccountMap();
        String thisUser = request.getParameter("username");
        String thisPsw = request.getParameter("password");
        boolean isBlocked = false;
        int count = -1;
        if (hrAttemptAccountMap.containsKey(thisUser)) {
            count = hrAttemptAccountMap.get(thisUser);
            if (count > 3)
                isBlocked = true;
            else {
                count = hrAttemptAccountMap.get(thisUser);
                count++;
                hrAttemptAccountMap.put(thisUser, count);
            }
        }

        if (isBlocked) {
            request.getRequestDispatcher("/login?error=blocked").forward(request, response);
        } else {
            if (hrAttemptAccountMap.containsKey(thisUser)) {
                request.getRequestDispatcher("/login?error=wrongPsw&count=" + count).forward(request, response);
            } else
                request.getRequestDispatcher("/login?error=generalUserWrongPsw").forward(request, response);
        }
    }
}
