package com.pwc.wechat.dao.candidate;

import com.pwc.wechat.entity.candidate.Application;
import com.pwc.wechat.entity.candidate.ApplicationPk;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IApplicationCrud
        extends JpaRepository<Application, ApplicationPk> {

    public List<Application> findAllByNationalIdOrderByApplyDateDesc(
            String nationalId);

    public List<Application> findAllByNationalIdAndApplyDateAfter(
            String nationalId,
            String afterDate);

}
