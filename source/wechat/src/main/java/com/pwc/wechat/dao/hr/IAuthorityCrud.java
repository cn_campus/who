package com.pwc.wechat.dao.hr;

import com.pwc.wechat.entity.user.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IAuthorityCrud
        extends JpaRepository<Authority, Integer> {

    public Authority findOneByUserNameAndUserAuth(
            String userName,
            String userAuth);

}
