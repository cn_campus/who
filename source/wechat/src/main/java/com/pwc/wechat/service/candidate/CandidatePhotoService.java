package com.pwc.wechat.service.candidate;

import com.pwc.wechat.dao.candidate.ICandidatePhotoCrud;
import com.pwc.wechat.entity.candidate.CandidatePhoto;
import com.pwc.wechat.service.upload.IGetUploadFile;
import com.pwc.wechat.service.upload.IUploadAndProcess;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@Service("candidatePhotoService")
public class CandidatePhotoService implements ICandidatePhotoHandler, IUploadAndProcess<String> {


    private static final Logger logger = LogManager.getLogger(CandidatePhotoService.class);
    private static final String PHOTONOTEXISTS = "photo_not_exists";

    @Autowired
    private IGetUploadFile<CandidatePhoto> iGetUploadFile;

    @Autowired
    private ICandidatePhotoCrud iCandidatePhotoCrud;


    @Override
    public String getUploadFile(HttpServletRequest request, String... nationalId) {
        iGetUploadFile.getUploadFile(request, file -> {
            logger.info("Get File and originalFileName {}", file.getOriginalFilename());
            //获取文件名
            InputStream in = null;
            CandidatePhoto photo = new CandidatePhoto();
            try {
                in = file.getInputStream();
                byte[] image = new byte[in.available()];
                in.read(image);
                in.close();
                photo.setNationalId(nationalId[0]);
                photo.setImage(image);
            } catch (IOException e) {
                logger.info("Read Candidate Photo Failed");
            }
            logger.info("Save Candidate Photo Success");
            return photo;
        }).forEach(photo -> this.saveCandidatePhoto(photo));
        return "success";
    }

    @Override
    public void saveCandidatePhoto(CandidatePhoto candidatePhoto) {
        logger.info("Save Candidate Photo");
        iCandidatePhotoCrud.save(candidatePhoto);
    }

    @Override
    public String createPhoto(String nationalId, HttpServletResponse response) {
        CandidatePhoto photo = iCandidatePhotoCrud.findOne(nationalId);
        if (photo == null) {
            logger.info("Failed to get avatar from DataBase");
            return PHOTONOTEXISTS;
        } else {
            try {
                OutputStream out = response.getOutputStream();
                out.write(photo.getImage());
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            response.setDateHeader("expries", -1);
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Pragma", "no-cache");
            return "";
        }

    }
}
