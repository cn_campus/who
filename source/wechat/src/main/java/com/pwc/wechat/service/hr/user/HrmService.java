package com.pwc.wechat.service.hr.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pwc.wechat.config.spring.LoginAttemptService;
import com.pwc.wechat.dao.hr.IAuthorityCrud;
import com.pwc.wechat.dao.hr.IAvatarCrud;
import com.pwc.wechat.dao.hr.IUserCrud;
import com.pwc.wechat.entity.user.Authority;
import com.pwc.wechat.entity.user.Avatar;
import com.pwc.wechat.entity.user.User;
import com.pwc.wechat.entity.user.UserRole;
import com.pwc.wechat.util.CommonUtil;
import com.pwc.wechat.util.JsonUtil;

@Service("hrmService")
public class HrmService implements IUserRoleHandler, IUserHandler {
	
	private static final Logger logger = LogManager.getLogger(HrmService.class);
	
	@Autowired
	private IUserCrud iUserCrud;
	
	@Autowired
	private IAvatarCrud iAvatarCrud;
	
	@Autowired
	private IAuthorityCrud iAuthorityCrud;
	
	@Autowired
	private LoginAttemptService loginAttemptService;
	
	private static String HR = "ROLE_HR";
	private static String HRM = "ROLE_HRM";
	
	@Override
	public void insertUser(User user) {
		user.setPassword(CommonUtil.encodePassword(user.getPassword()));
		user.setEnabled(true);
		iUserCrud.save(user);
		logger.info("new user {}", JsonUtil.toNormalJson(user));
		this.updateUserAuth(user.getUserName(), HR, true);
		this.updateUserAuth(user.getUserName(), HRM, false);
		//add the new HR account to security Map
		loginAttemptService.getHrAttemptAccountMap().put(user.getUserName(), 0);
	}
    @Override
    public boolean verifyPassword(User user) {	     
       String lastPwd= CommonUtil.encodePassword(user.getPassword());
       String dbPwd =  iUserCrud.findOne(user.getUserName()).getPassword();
       if (lastPwd.equals(dbPwd))
          return true;
       else
    	  return false;
    }
    
    @Override
    public void changePassword(User user){
    	logger.info(user.getUserName());
        User userdb = iUserCrud.findOne(user.getUserName());
        userdb.setPassword(CommonUtil.encodePassword(user.getPassword()));
        iUserCrud.save(userdb);
    }
    
    @Override
    public void saveAvatar(Avatar avatar){
    	logger.info("Save Avatar");
    	iAvatarCrud.save(avatar);
    }
    
    @Override
    public Avatar getAvatar(String userName){
    	logger.info("Get Avatar"); 	
    	return iAvatarCrud.findOne(userName);
    }
    
	@Override
	public List<UserRole> searchUserRoles() {
		List<UserRole> userRoles = new ArrayList<>();
		Map<String, UserRole> userNameMap = new HashMap<>();
		iAuthorityCrud.findAll().forEach((Authority auth) -> {
			if (userNameMap.containsKey(auth.getUserName())) {
				UserRole userRole = userNameMap.get(auth.getUserName());
				userRole.setHrFlag(userRole.getHrFlag() ? true : auth.getUserAuth().equals(HR) && auth.getEnabled());
				userRole.setHrmFlag(userRole.getHrmFlag() ? true : auth.getUserAuth().equals(HRM) && auth.getEnabled());
			} else {
				UserRole userRole = new UserRole();
				userRole.setUserName(auth.getUserName());
				userRole.setHrFlag(auth.getUserAuth().equals(HR) && auth.getEnabled());
				userRole.setHrmFlag(auth.getUserAuth().equals(HRM) && auth.getEnabled());
				userNameMap.put(auth.getUserName(), userRole);
			}
		});
		userRoles.addAll(userNameMap.values());
		return userRoles;
	}
	
	@Override
	public void updateUserRole(UserRole userRole) {
		logger.info("update UserRole {}", JsonUtil.toNormalJson(userRole));
		this.updateUserAuth(userRole.getUserName(), HR, userRole.getHrFlag());
		this.updateUserAuth(userRole.getUserName(), HRM, userRole.getHrmFlag());
	}
	
	private void updateUserAuth(String userName, String auth, boolean enable) {
		Authority authority = iAuthorityCrud.findOneByUserNameAndUserAuth(userName, auth);
		authority = authority == null ? new Authority() : authority;
		authority.setEnabled(enable);
		authority.setUserName(userName);
		authority.setUserAuth(auth);
		iAuthorityCrud.save(authority);
	}

}
