package com.pwc.wechat.service.mail;

import java.nio.file.Path;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.config.constant.BaseConst;
import com.pwc.wechat.entity.hr.Config;
import com.pwc.wechat.entity.hr.TalentDataSet;
import com.pwc.wechat.entity.hr.TalentEmail;

public interface IEmailHandler {
	
	@Transactional(rollbackFor = Exception.class)
	public int sendEmail(TalentEmail talentemail,List<Path> paths);

	@Transactional(rollbackFor = Exception.class)
	public Config getEmailTemplate(String emailStatusTemplate);
	
	@Transactional(readOnly = false,rollbackFor = Exception.class)
	public String updateEmailTemplate(Config config);
	
	public TalentDataSet configToSet(TalentDataSet set);
}