package com.pwc.wechat.config.spring;

import com.pwc.wechat.config.constant.BaseConst;
import com.pwc.wechat.config.constant.MySQLConst;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.http.converter.xml.SourceHttpMessageConverter;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import javax.sql.DataSource;
import javax.xml.transform.Source;
import java.util.List;
import java.util.Properties;

@Configuration
@Profile(value = {"default"})
@EnableWebMvc
@EnableScheduling
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.pwc.wechat.dao")
@ComponentScan(basePackages = "com.pwc.wechat")
public class WebContext
        extends WebMvcConfigurerAdapter {

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {

        StringHttpMessageConverter stringHttpMessageConverter = new StringHttpMessageConverter();
        stringHttpMessageConverter.setWriteAcceptCharset(false);  // see SPR-7316
        converters.add(new ByteArrayHttpMessageConverter());
        converters.add(stringHttpMessageConverter);
        converters.add(new SourceHttpMessageConverter<Source>());
        converters.add(new AllEncompassingFormHttpMessageConverter());
        converters.add(new MappingJackson2HttpMessageConverter());
        converters.add(new MappingJackson2XmlHttpMessageConverter());
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
        registry.addResourceHandler("/html/**").addResourceLocations("/html/");
    }

    @Bean
    public ViewResolver htmlViewResolver() {

        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setViewClass(HtmlResourceView.class);
        viewResolver.setPrefix("/html/");
        viewResolver.setSuffix(".html");
        viewResolver.setOrder(0);
        return viewResolver;
    }

    @Bean
    public ViewResolver jspViewResolver() {

        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setViewClass(JstlView.class);
        viewResolver.setPrefix("/pages/");
        viewResolver.setSuffix(".jsp");
        viewResolver.setOrder(1);
        return viewResolver;
    }

    @Bean(name = "dataSourceMySQL")
    public DataSource dataSourceMySQL() {

        DriverManagerDataSource source = new DriverManagerDataSource();
        source.setUrl(MySQLConst.MYSQL_URL);
        source.setDriverClassName(MySQLConst.MYSQL_DRIVER);
        source.setUsername(MySQLConst.MYSQL_USER_NAME);
        source.setPassword(MySQLConst.MYSQL_PASSWORD);
        return source;
    }

    @Bean
    public PlatformTransactionManager transactionManager(DataSource dataSourceMySQL) {

        JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
        jpaTransactionManager.setDataSource(dataSourceMySQL);
        return jpaTransactionManager;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSourceMySQL) {

        LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();
        bean.setDataSource(dataSourceMySQL);
        bean.setPackagesToScan("com.pwc.wechat.entity");
        bean.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        Properties properties = new Properties();
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
        properties.setProperty("hibernate.show_sql", BaseConst.HIBERNATE_SHOW_SQL.toString());
        properties.setProperty("hibernate.format_sql", BaseConst.HIBERNATE_SHOW_SQL.toString());
        properties.setProperty("hibernate.default_schema", "wechat");
        bean.setJpaProperties(properties);
        return bean;
    }

    @Bean
    public MultipartResolver multipartResolver() {

        StandardServletMultipartResolver resolver = new StandardServletMultipartResolver();
        return resolver;
    }

    /**
     * spring线程池
     *
     * @return
     */
    @Bean(name = "threadPoolTaskExecutor")
    public ThreadPoolTaskExecutor threadPoolTaskExecutor() {

        ThreadPoolTaskExecutor poolTaskExecutor = new ThreadPoolTaskExecutor();
        poolTaskExecutor.setCorePoolSize(5);  //线程池维护线程的最少数量
        poolTaskExecutor.setQueueCapacity(5); //线程池所使用的缓冲队列
        poolTaskExecutor.setMaxPoolSize(10);  //线程池维护线程的最大数量
        poolTaskExecutor.setKeepAliveSeconds(30000); //线程池维护线程所允许的空闲时间
        return poolTaskExecutor;
    }
}
