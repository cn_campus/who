package com.pwc.wechat.service.hr.user;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pwc.wechat.entity.user.Avatar;
import com.pwc.wechat.service.upload.IGetUploadFile;
import com.pwc.wechat.service.upload.IUploadAndProcess;

@Service("uploadAvatar")
public class UploadAvatar implements IAvatarHandler, IUploadAndProcess<String>{

	private static final Logger logger = LogManager.getLogger(UploadAvatar.class);
	private static final String AVATARNOTEXISTS = "avatar_not_exists";
	
	@Autowired
	private IGetUploadFile<Avatar> iGetUploadFile;
	
	@Autowired
	private IUserHandler iUserHandler;
	
	@Override
	public String getUploadFile(HttpServletRequest request, String... userName) {
        iGetUploadFile.getUploadFile(request, file ->{
        	logger.info("Get File");
        	logger.info("OriginalFilename {}",file.getOriginalFilename());
        	InputStream in = null;
        	Avatar avatar = new Avatar();
        	try{
        	    in = file.getInputStream();
        		byte[] image = new byte[in.available()];
        		in.read(image);
        		in.close();
        		avatar.setUserName(userName[0]);
        		avatar.setImage(image);
        	}catch(IOException e){
        		logger.info("Read Avatar Failed");
        	}
        	logger.info("Save Avatar Success");
        	return avatar;
        }).forEach(avatar -> iUserHandler.saveAvatar(avatar));
        return "";
	}
	
	@Override
	public String createAvatar(String userName, HttpServletRequest request, HttpServletResponse response){
		Avatar avatar = iUserHandler.getAvatar(userName);
		if (avatar==null){
			   logger.info("Fail to get avatar from DataBase");
			   return AVATARNOTEXISTS;
		}else{
                try{
                	OutputStream out = response.getOutputStream();
            		out.write(avatar.getImage());
            		out.close();
                }catch(IOException e){
                	e.printStackTrace();
                }
                
            	response.setDateHeader("expries", -1);
        		response.setHeader("Cache-Control","no-cache");
        		response.setHeader("Pragma", "no-cache");
        		return "";
		}	
	
	}

}
