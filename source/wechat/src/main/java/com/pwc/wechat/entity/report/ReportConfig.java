package com.pwc.wechat.entity.report;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.pwc.wechat.entity.hr.TalentPage;
import com.pwc.wechat.util.JsonUtil;

@Entity
@Table(name = "report_config", schema = "wechat")
public class ReportConfig {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "title")
	private String title;

	@Column(name = "configuration")
	private String configsJson;

	@Transient
	private ReportColumn[] configs;

	@Transient
	private String[] conditions;

	@Transient
	private TalentPage talentPage;

	@Transient
	private Boolean check;

	public ReportConfig() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ReportConfig(String title, ReportColumn[] configs) {
		this.title = title;
		this.configs = configs;
	}

	public Boolean getCheck() {
		return check;
	}

	public void setCheck(Boolean check) {
		this.check = check;
	}

	public TalentPage getTalentPage() {
		return talentPage;
	}

	public void setTalentPage(TalentPage talentPage) {
		this.talentPage = talentPage;
	}

	public String[] getConditions() {
		return conditions;
	}

	public void setConditions(String[] conditions) {
		this.conditions = conditions;
	}

	public String getConfigsJson() {
		return configsJson;
	}

	public void setConfigsJson(String configsJson) {
		this.configsJson = configsJson;
		this.configs = JsonUtil.toNormalObject(configsJson, ReportColumn[].class);
	}

	public ReportColumn[] getConfigs() {
		return configs;
	}

	public void setConfigs(ReportColumn[] configs) {
		this.configs = configs;
		this.configsJson = JsonUtil.toNormalJson(configs);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
