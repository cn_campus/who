package com.pwc.wechat.entity.hr;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "config", schema = "wechat")
public class Config implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8469188117105864399L;

	@Column(name = "email_context_template")
	private String emailContextTemplate;
	
	@Column(name = "email_subject_template")
	private String emailSubjectTemplate;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "email_status_template")
	private String emailStatusTemplate;
	
	@Column(name = "send_email_flag")
	private String sendEmailFlag;
	
	
	@Column(name = "round_move_flag")
	private String roundMoveFlag;
	
	@Column(name = "apply_number")
	private String applyNumber;
	
	@Column(name = "move_back_flag")
	private String moveBackFlag;
	
	@Column(name = "opt")
	private String opt;

	
	
	public String getOpt() {
		return opt;
	}

	public void setOpt(String opt) {
		this.opt = opt;
	}

	public String getSendEmailFlag() {
		return sendEmailFlag;
	}

	public void setSendEmailFlag(String sendEmailFlag) {
		this.sendEmailFlag = sendEmailFlag;
	}

	public String getRoundMoveFlag() {
		return roundMoveFlag;
	}

	public void setRoundMoveFlag(String roundMoveFlag) {
		this.roundMoveFlag = roundMoveFlag;
	}

	

	public String getApplyNumber() {
		return applyNumber;
	}

	public void setApplyNumber(String applyNumber) {
		this.applyNumber = applyNumber;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getMoveBackFlag() {
		return moveBackFlag;
	}

	public void setMoveBackFlag(String moveBackFlag) {
		this.moveBackFlag = moveBackFlag;
	}

	
	public String getEmailContextTemplate() {
		return emailContextTemplate;
	}

	public void setEmailContextTemplate(String emailContextTemplate) {
		this.emailContextTemplate = emailContextTemplate;
	}

	public String getEmailSubject() {
		return emailSubjectTemplate;
	}

	

	public String getEmailSubjectTemplate() {
		return emailSubjectTemplate;
	}

	public void setEmailSubjectTemplate(String emailSubjectTemplate) {
		this.emailSubjectTemplate = emailSubjectTemplate;
	}

	public String getEmailStatusTemplate() {
		return emailStatusTemplate;
	}

	public void setEmailStatusTemplate(String emailStatusTemplate) {
		this.emailStatusTemplate = emailStatusTemplate;
	}


	

}

