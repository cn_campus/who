package com.pwc.wechat.config.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import javax.sql.DataSource;

@EnableWebSecurity
public class SecurityConfig
        extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSourceMySQL;

    @Autowired
    private AuthenticationFailureHandlerImpl failHandler;

    @Autowired
    private AuthenticationSuccessHandlerImpl successHandler;


    @Autowired
    public void configureGlobal(
            AuthenticationManagerBuilder auth)
            throws Exception {

        auth.jdbcAuthentication()
                .dataSource(dataSourceMySQL)
                .usersByUsernameQuery(this.getUserQuery())
                .passwordEncoder(new Md5PasswordEncoder())
                .authoritiesByUsernameQuery(this.getAuthoritiesQuery());
    }

    /**
     * when authentication is required, redirect the browser to /login
     * <p>
     * we are in charge of rendering the login page when /login is requested
     * <p>
     * when authentication attempt fails, redirect the browser to /login?error
     * (since we have not specified otherwise)
     * <p>
     * we are in charge of rendering a failure page when /login?error is
     * requested
     * <p>
     * when we successfully logout, redirect the browser to /login?logout (since
     * we have not specified otherwise)
     * <p>
     * we are in charge of rendering a logout confirmation page when
     * /login?logout is requested
     */

    @Override
    protected void configure(
            HttpSecurity http)
            throws Exception {

        http
                .csrf()
                .disable()
                .authorizeRequests()
                .antMatchers("/resources/**", "/html/**", "/forCandidate/*", "/forCandidate*", "/talk/*", "/applyOnPC/*", "/applyOnPC*")
                .permitAll()
                .and()
                .authorizeRequests()
                .antMatchers("/forHr/*")
                .hasRole("HR")
                .and()
                .authorizeRequests()
                .antMatchers("/forHrm/*")
                .hasRole("HRM")
                .anyRequest()
                .authenticated()
                .and()
                .exceptionHandling()
                .accessDeniedPage("/html/error/accessDenied.html")
                .and()
                .formLogin()
                .loginPage("/login")
                .failureHandler(failHandler)
                .successHandler(successHandler)
                .permitAll()
                .and()
                .logout()
                .permitAll()
                .deleteCookies("JSESSIONID")
                .and()
                .sessionManagement()
                .maximumSessions(1).expiredUrl("/html/error/sessionTimeout.html").and()
                .invalidSessionUrl("/login");
    }

    private String getUserQuery() {

        return "SELECT t.user_name as 'username', t.pass_word as 'password', t.enabled as 'enabled' FROM wechat.users t WHERE t.user_name = ?";
    }

    private String getAuthoritiesQuery() {

        return "SELECT t.user_name as 'username', t.user_auth as 'authority' FROM wechat.authorities t WHERE t.enabled = '1' and t.user_name = ?";
    }
}