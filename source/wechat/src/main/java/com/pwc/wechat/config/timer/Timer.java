package com.pwc.wechat.config.timer;

import com.pwc.wechat.config.constant.BaseConst;
import com.pwc.wechat.util.HttpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class Timer {

    private static final Logger logger = LoggerFactory.getLogger(Timer.class);

    // 秒（0~59） /分钟（0~59）/小时（0~23）/天（月）(0~31)/月（0~11）/天（星期）（1~7)
    // as well as minute, hour, day of month, month
    // * and day of week. e.g. {@code "0 * * * * MON-FRI"}
    @Scheduled(cron = "*/20 * * * * ?")
    public void refreshAccessToken() {

        try {
            BaseConst.WEIXIN_EXPIRES_IN = BaseConst.WEIXIN_EXPIRES_IN - 20;
            if (BaseConst.WEIXIN_EXPIRES_IN <= 60) {
                HttpUtil.refreshToken();
            }
        } catch (IOException e) {
            logger.info("Refresh AccessToken Fialed");
        }
    }
}
