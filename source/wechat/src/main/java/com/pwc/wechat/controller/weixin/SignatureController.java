package com.pwc.wechat.controller.weixin;

import com.pwc.wechat.entity.weixin.Message;
import com.pwc.wechat.service.weixin.AccessVerify;
import com.pwc.wechat.util.DateUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

@RestController
@RequestMapping(value = "/talk")
public class SignatureController {

    private static final Logger logger = LogManager.getLogger(SignatureController.class);

    @Autowired
    private AccessVerify accessVerify;

    @RequestMapping(value = "/", produces = "text/html; charset=utf-8", method = RequestMethod.GET)
    public String verifySignature(
            @RequestParam(defaultValue = "") String signature,
            @RequestParam(defaultValue = "") String timestamp,
            @RequestParam(defaultValue = "") String nonce,
            @RequestParam(defaultValue = "") String echostr)
            throws NoSuchAlgorithmException, UnsupportedEncodingException {

        logger.info("Verify Signature {} {} {} {}", signature, timestamp, nonce, echostr);
        return accessVerify.excuteVerify(signature, timestamp, nonce, echostr);
    }

    @RequestMapping(value = "/", produces = "application/xml; charset=utf-8", method = RequestMethod.POST)
    public Message getMessage(@RequestBody Message getMessage) {

        logger.info("Get message from WeiXin {}", getMessage.getFromUserName());
        // getMessage.getFromUserName() is WeiXin openId
        Message sendMessage = new Message();
        sendMessage.setFromUserName(getMessage.getToUserName());
        sendMessage.setToUserName(getMessage.getFromUserName());
        sendMessage.setCreateTime(DateUtil.getEpochHour());
        sendMessage.setMsgType("text");
        sendMessage.setContent("welcome" + getMessage.getFromUserName());
        return sendMessage;
    }
}
