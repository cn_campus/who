package com.pwc.wechat.service.hr.round;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pwc.wechat.dao.hr.IHrCommentCrud;
import com.pwc.wechat.entity.hr.HrComment;

@Service("hrCommentService")
public class HrCommentService implements IHrCommentHandler {
	
	@Autowired
	private IHrCommentCrud iHrCommentCrud;
	
	@Override
	public HrComment searchHrComment(String nationalId) {
		HrComment hrComment = iHrCommentCrud.findOne(nationalId);
		if (hrComment == null) {
			hrComment = new HrComment();
			hrComment.setNationalId(nationalId);
			hrComment.setAppearance(0);
			hrComment.setDemeanour(0);
			hrComment.setEnglish(0);
		}
		return hrComment;
	}

	@Override
	public void updateHrComment(HrComment hrComment) {
		iHrCommentCrud.save(hrComment);
	}
	
}
