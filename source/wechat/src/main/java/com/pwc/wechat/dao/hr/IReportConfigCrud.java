package com.pwc.wechat.dao.hr;

import com.pwc.wechat.entity.report.ReportConfig;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IReportConfigCrud
        extends JpaRepository<ReportConfig, Integer> {

}
