package com.pwc.wechat.entity.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users", schema = "wechat")
public class User implements Serializable {

	private static final long serialVersionUID = -8080869802249973076L;

	@Id
	@Column(name = "user_name", unique = true, nullable = false)
	private String username;

	@Column(name = "pass_word", nullable = false)
	private String password;

	@Column(name = "enabled")
	private boolean enabled;

	public String getUserName() {
		return username;
	}

	public void setUserName(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
