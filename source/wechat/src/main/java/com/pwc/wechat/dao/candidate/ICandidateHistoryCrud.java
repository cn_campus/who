package com.pwc.wechat.dao.candidate;

import com.pwc.wechat.entity.candidate.CandidateHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ICandidateHistoryCrud
        extends JpaRepository<CandidateHistory, Long> {

    public List<CandidateHistory> findAllByNationalId(
            String nationalId);

}
