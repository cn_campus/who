package com.pwc.wechat.config.constant;

import com.pwc.wechat.entity.report.ReportColumn;

public class BaseConst {

    private BaseConst() {

    }

    public static final Integer APPLY_TIME_LIMIT = 30;
    public static final Boolean HIBERNATE_SHOW_SQL = false;
    public static final String WEIXIN_CUSTOM_TOKEN = "COM_PWC_WECHAT_TOKEN";
    public static final String WEIXIN_APP_ID = "wx42d60487f60c19fb";
    public static final String WEIXIN_APP_SECRET = "288689ef57a1dfcf10182430b8656d35";
    public static final String WEIXIN_TOKEN_URL = "api.weixin.qq.com/cgi-bin/token";
    public static final String WEIXIN_USER_TOKEN_URL = "api.weixin.qq.com/sns/oauth2/access_token";
    public static final String WEIXIN_TOKEN_GRANT_TYPE = "client_credential";
    public static final String WEIXIN_USER_TOKEN_GRANT_TYPE = "authorization_code";
    public static String WEIXIN_ACCESS_TOKEN = "access_token";
    public static int WEIXIN_EXPIRES_IN = 0;
    public static int EXCEL_START_ROW = 3;
    public static final String UPLOAD_FOLDER = "/upload";
    public static final int TRANSACTION_TIMEOUT = 5;

    public static ReportColumn[] EXCEL_COLUMNS = {
            ReportColumn.APPLICATION_APPLY_DATE,
            ReportColumn.APPLICATION_APPLY_ID,
            ReportColumn.APPLICATION_APPLY_POSITION,
            ReportColumn.CANDIDATE_AVAILABLE_SCHEDULE,
            ReportColumn.CANDIDATE_NAME,
            ReportColumn.CANDIDATE_CURRENT_PROVINCE,
            ReportColumn.CANDIDATE_GENDER,
            ReportColumn.CANDIDATE_NATIONALITY,
            ReportColumn.CANDIDATE_NATIONAL_ID,
            ReportColumn.CANDIDATE_PASSPORT_NUMBER,
            ReportColumn.CANDIDATE_BIRTHDAY,
            ReportColumn.CANDIDATE_BIRTH_PROVINCE,
            ReportColumn.CANDIDATE_HOME_PHONE,
            ReportColumn.CANDIDATE_CELL_PHONE,
            ReportColumn.CANDIDATE_MAIL_ADDRESS,
            ReportColumn.CANDIDATE_PERSONAL_PAGE,
            ReportColumn.CANDIDATE_SKILLS_DESCRIBE,
            ReportColumn.CANDIDATE_GRADUATE_PLAN,
            ReportColumn.CANDIDATE_GRADUATE_DATE,
            ReportColumn.CANDIDATE_LANGUAGE_LEVEL,
            ReportColumn.EDUCATION_EXPERIENCE_DEGREE,
            ReportColumn.EDUCATION_EXPERIENCE_UNIVERSITY,
            ReportColumn.EDUCATION_EXPERIENCE_MAJOR,
            ReportColumn.EDUCATION_EXPERIENCE_PERIOD,
            ReportColumn.EDUCATION_EXPERIENCE_GPA,
            ReportColumn.EDUCATION_EXPERIENCE_DEGREE,
            ReportColumn.WORK_EXPERIENCE_COMPANY,
            ReportColumn.WORK_EXPERIENCE_POSITION,
            ReportColumn.WORK_EXPERIENCE_PERIOD,
            ReportColumn.WORK_EXPERIENCE_DETAILS
    };
}
