package com.pwc.wechat.dao.candidate;

import com.pwc.wechat.config.constant.BaseConst;
import com.pwc.wechat.dao.hr.IEmailCrud;
import com.pwc.wechat.entity.candidate.Application;
import com.pwc.wechat.entity.candidate.ApplicationPk;
import com.pwc.wechat.entity.candidate.QuestAndAnswer;
import com.pwc.wechat.entity.hr.TalentData;
import com.pwc.wechat.entity.report.ReportColumn;
import com.pwc.wechat.util.ApplicationContextUtil;
import com.pwc.wechat.util.DateUtil;
import com.pwc.wechat.util.JsonUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository("hybridDao")
public class HybridDao {

    private static final Logger logger = LogManager.getLogger(HybridDao.class);

    @Autowired
    private IApplicationCrud iApplicationCrud;

    @Autowired
    private IQuestionnaireCrud iQuestionnaireCrud;

    @Autowired
    private IEmailCrud iEmailCrud;

    public int updateTalentData(
            TalentData talentData) {

        Application application = iApplicationCrud.findOne(new ApplicationPk(talentData.getDate(), Integer.valueOf(talentData.getAppId())));
        application.setRound1Score(talentData.getQ1());
        application.setRound2Score(talentData.getQ2());
        application.setRound3Score(talentData.getQ3());
        application.setStatus(talentData.getStatus());
        application.setResult(talentData.getResult());
        iApplicationCrud.save(application);
        return 1;
    }

    /**
     * whether some one's application is acceptable
     * if the last application date is within BaseConst.APPLY_TIME_LIMIT
     * then false
     * otherwise true
     *
     * @param nationalId
     * @return
     */
    public boolean applicationAcceptable(
            String nationalId) {

        return iApplicationCrud.findAllByNationalIdAndApplyDateAfter(nationalId, DateUtil.plusDays(DateUtil.getLocalDate(), -BaseConst.APPLY_TIME_LIMIT)).isEmpty();
    }

    /**
     * for uploadApplication judgment
     * Acceptable when the new apply date is
     * for each old apply date
     * earlier than old apply date -30
     * or later than old apply date +30
     *
     * @param nationalId
     * @param applyDate
     * @return
     */
    public boolean uploadApplicationAcceptable(
            String nationalId,
            String applyDate) {

        return iApplicationCrud.findAllByNationalIdOrderByApplyDateDesc(nationalId).stream().allMatch(oldApplication ->
                DateUtil.isEarlier(applyDate, DateUtil.plusDays(oldApplication.getApplyDate(), -BaseConst.APPLY_TIME_LIMIT))
                        || DateUtil.isEarlier(DateUtil.plusDays(oldApplication.getApplyDate(), BaseConst.APPLY_TIME_LIMIT), applyDate));
    }

    public Application findLatestApplication(
            String nationalId) {

        return iApplicationCrud.findAllByNationalIdOrderByApplyDateDesc(nationalId).stream().findFirst().orElse(null);//peak the latest one
    }

    public QuestAndAnswer findLatestQuestAndAnswer(
            String nationalId) {

        QuestAndAnswer questAndAnswer = new QuestAndAnswer();
        questAndAnswer.setAnswer(nationalId);
        iQuestionnaireCrud.findAllByNationalIdOrderByIdDesc(nationalId).stream().findFirst().ifPresent(questionnaire -> {
            QuestAndAnswer fromDataBase = JsonUtil.toNormalObject(questionnaire.getContent(), QuestAndAnswer.class);//peak the latest one
            for (int i = 0; i < fromDataBase.getQuestAndAnswerList().size(); i++) {
                QuestAndAnswer quest = fromDataBase.getQuestAndAnswerList().get(i);
                quest.setQuestion((i + 1) + ". " + quest.getQuestion());// add index to QuestAndAnswer
                questAndAnswer.getQuestAndAnswerList().add(quest);
            }
        });
        return questAndAnswer;
    }

    public int getDataTotal(
            final ReportColumn[] columns,
            final String[] conditions) {

        EntityManager entityManager = ApplicationContextUtil.creatEntityManager();
        Query query = entityManager.createNativeQuery("SELECT COUNT(*) FROM (" + this.getSql(columns, conditions) + ") AS t ");
        String total = String.valueOf(query.getSingleResult());
        entityManager.close();
        return Integer.valueOf(total);
    }

    public <T> List<T> querySqlWithJpa(
            final ReportColumn[] columns,
            final String[] conditions,
            ResultMapping<T> resultMapping,
            int dataIndex,
            int pageLines) {

        String pagedSql = "SELECT * FROM (" + this.getSql(columns, conditions) + ") AS t LIMIT " + dataIndex + "," + pageLines + " ";
        return querySqlWithJpa(pagedSql, columns, conditions, resultMapping);
    }

    public <T> List<T> querySqlWithJpa(
            final ReportColumn[] columns,
            final String[] conditions,
            ResultMapping<T> resultMapping) {

        String sqlString = this.getSql(columns, conditions);
        return querySqlWithJpa(sqlString, columns, conditions, resultMapping);
    }

    public <T> List<T> querySqlWithJpa(
            final String sqlString,
            final ReportColumn[] columns,
            final String[] conditions,
            ResultMapping<T> resultMapping) {

        EntityManager entityManager = ApplicationContextUtil.creatEntityManager();
        Query query = entityManager.createNativeQuery(sqlString);
        @SuppressWarnings("unchecked")
        List<Object[]> sqlRes = query.getResultList();
        List<T> datas = new LinkedList<>();
        for (int i = 0; i < sqlRes.size(); i++) {
            datas.add(resultMapping.mapResult(sqlRes.get(i), i));
        }
        entityManager.close();
        return datas;
    }

    private String getSql(
            final ReportColumn[] columns,
            final String[] conditions) {

        logger.info(JsonUtil.toNormalJson(conditions));
        StringBuilder factor = new StringBuilder();
        StringBuilder whereCondition = new StringBuilder();
        List<String> colStrings = Arrays.stream(columns).map(one -> one.getColumnData()).collect(Collectors.toList());
        Optional.ofNullable(columns).ifPresent(cols -> {
            Arrays.stream(cols).forEach(col -> {
                factor.append(factor.length() == 0 ? uniqueField(colStrings, col) : ", " + uniqueField(colStrings, col));
            });
        });
        Optional.ofNullable(conditions).ifPresent(cons -> {
            Arrays.stream(cons).filter(con -> con != null && !con.isEmpty())
                    .forEach(con -> whereCondition.append(" AND " + con));
        });
        return "SELECT " + factor.toString() + " FROM wechat.candidate LEFT JOIN wechat.application ON candidate.national_id = application.national_id LEFT JOIN wechat.education_experience ON candidate.national_id = education_experience.national_id AND education_experience.last_exp = TRUE LEFT JOIN wechat.work_experience ON candidate.national_id = work_experience.national_id AND work_experience.last_exp = TRUE WHERE 1=1 "
                + whereCondition.toString();
    }

    private String uniqueField(
            List<String> columns,
            ReportColumn col) {

        String field = col.getColumnData();
        if (field.indexOf(".") != -1) {
            field = field.substring(field.indexOf(".") + 1);
        } else if (field.equals("''")) {
            field = "BLANK";
        } else {
            field = field.replace("'", "");
        }
        return col.getColumnData() + " as " + this.nested(columns, field, 0);
    }

    private String nested(
            List<String> columns,
            String field,
            int num) {

        String nested = field + "_" + num;
        if (columns.stream().anyMatch(col -> col.contains(nested))) {
            return nested(columns, field, num + 1);
        } else {
            columns.add(nested);
            return nested;
        }
    }
}
