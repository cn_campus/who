package com.pwc.wechat.entity.candidate;

public enum AppResult {

    RESULT_UNKNOWN("unknown"), RESULT_PASS("pass"), RESULT_FAIL("fail");

    private String resultName;

    public String getResultName() {

        return resultName;
    }

    public void setResultName(String resultName) {

        this.resultName = resultName;
    }

    private AppResult(String resultName) {

        this.resultName = resultName;
    }

}
