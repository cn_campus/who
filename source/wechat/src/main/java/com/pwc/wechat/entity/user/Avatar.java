package com.pwc.wechat.entity.user;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "avatar", schema = "wechat")
public class Avatar implements Serializable{
 
   private static final long serialVersionUID = 4319725457716709490L;
	   
   @Id
   @Column(name = "user_name", unique = true, nullable = false)
   private String userName;
   
   @Column(name = "image")
   private byte[] image;
   
   public String getUserName(){
	   return userName;
   }
   
   public void setUserName(String userName){
	   this.userName = userName;
   }
   
   public byte[] getImage(){
	   return image;
   }
   
   public void setImage(byte[] image){
	   this.image = image;
   }
   
}
