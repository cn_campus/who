package com.pwc.wechat.entity.candidate;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "application", schema = "wechat")
@IdClass(ApplicationPk.class)
public class Application implements Serializable {

    private static final long serialVersionUID = 2141704120823284613L;

    @Id
    @Column(name = "apply_date")
    private String applyDate;// yyyy-MM-dd

    @Id
    @Column(name = "apply_id")
    private Integer applyId;

    @Column(name = "apply_position")
    private String applyPosition;

    @Column(name = "national_id")
    private String nationalId;

    @Column(name = "status")
    private AppStatus status;

    @Column(name = "result")
    private AppResult result;

    @Column(name = "round_1_score")
    private Integer round1Score;

    @Column(name = "round_2_score")
    private Integer round2Score;

    @Column(name = "round_3_score")
    private Integer round3Score;

    @Column(name = "opt_time")
    private String optTime;

    public Integer getApplyId() {

        return applyId;
    }

    public void setApplyId(Integer applyId) {

        this.applyId = applyId;
    }

    public String getApplyPosition() {

        return applyPosition;
    }

    public void setApplyPosition(String applyPosition) {

        this.applyPosition = applyPosition;
    }

    public String getNationalId() {

        return nationalId;
    }

    public void setNationalId(String nationalId) {

        this.nationalId = nationalId;
    }

    public AppStatus getStatus() {

        return status;
    }

    public void setStatus(AppStatus status) {

        this.status = status;
    }

    public AppResult getResult() {

        return result;
    }

    public void setResult(AppResult result) {

        this.result = result;
    }

    public Integer getRound1Score() {

        return round1Score;
    }

    public void setRound1Score(Integer round1Score) {

        this.round1Score = round1Score;
    }

    public Integer getRound2Score() {

        return round2Score;
    }

    public void setRound2Score(Integer round2Score) {

        this.round2Score = round2Score;
    }

    public Integer getRound3Score() {

        return round3Score;
    }

    public void setRound3Score(Integer round3Score) {

        this.round3Score = round3Score;
    }

    public String getApplyDate() {

        return applyDate;
    }

    public void setApplyDate(String applyDate) {

        this.applyDate = applyDate;
    }

    public String getOptTime() {

        return optTime;
    }

    public void setOptTime(String optTime) {

        this.optTime = optTime;
    }

}
