package com.pwc.wechat.entity.candidate;

public enum AppStatus {

    STATUS_NEW("new", "STATUS_Q1", "STATUS_NEW"),
    STATUS_Q1("q1", "STATUS_Q2", "STATUS_NEW"),
    STATUS_Q2("q2", "STATUS_Q3", "STATUS_Q1"),
    STATUS_Q3("q3", "STATUS_Q4", "STATUS_Q2"),
    STATUS_Q4("q4", "STATUS_Q5", "STATUS_Q3"),
    STATUS_Q5("q5", "STATUS_Q5", "STATUS_Q4");

    private String statusName;
    private String next;
    private String previous;

    private AppStatus(
            String statusName,
            String next,
            String previous) {

        this.statusName = statusName;
        this.next = next;
        this.previous = previous;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {

        this.statusName = statusName;
    }

    public String getNext() {

        return next;
    }

    public void setNext(String next) {

        this.next = next;
    }

    public String getPrevious() {

        return previous;
    }

    public void setPrevious(String previous) {

        this.previous = previous;
    }


}
