package com.pwc.wechat.dao.hr;

import com.pwc.wechat.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserCrud
        extends JpaRepository<User, String> {

}
