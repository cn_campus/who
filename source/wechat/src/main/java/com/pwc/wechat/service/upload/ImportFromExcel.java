package com.pwc.wechat.service.upload;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.util.EnglishEnums;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.internal.runners.model.EachTestNotifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pwc.wechat.config.constant.BaseConst;
import com.pwc.wechat.dao.candidate.IQuestionCrud;
import com.pwc.wechat.dao.candidate.IQuestionnaireCrud;
import com.pwc.wechat.dao.hr.IEmailCrud;
import com.pwc.wechat.entity.candidate.Application;
import com.pwc.wechat.entity.candidate.Candidate;
import com.pwc.wechat.entity.candidate.EducationExperience;
import com.pwc.wechat.entity.candidate.Questionnaire;
import com.pwc.wechat.entity.candidate.WorkExperience;
import com.pwc.wechat.entity.hr.Config;
import com.pwc.wechat.service.candidate.ICandidateHandler;
import com.pwc.wechat.service.candidate.IQuestionnaireHandler;
import com.pwc.wechat.util.CommonUtil;
import com.pwc.wechat.util.JsonUtil;

@Service("importFromExcel")
public class ImportFromExcel implements IUploadAndProcess<String> {
	
	private static final Logger logger = LogManager.getLogger(ImportFromExcel.class);
	
	@Autowired
	private ICandidateHandler iCandidateHandler;
	
	@Autowired
	private IGetUploadFile<XSSFWorkbook> iGetUploadFile;
	
	@Autowired
	private IEmailCrud iEmailCrud;
	
	@Autowired
	private IQuestionnaireCrud iQuestionnaireCrud;
	
	@Autowired
	private IQuestionnaireHandler iQuestionnaireHandler;
	
	
	@Override
	public String getUploadFile(HttpServletRequest request, String... attrs) {
		List<XSSFWorkbook> books = iGetUploadFile.getUploadFile(request, file -> {
			logger.info("Get File");
			logger.info("OriginalFilename {}", file.getOriginalFilename());
			XSSFWorkbook workbook = null;
			try {
				workbook = new XSSFWorkbook(file.getInputStream());
			} catch (IOException e) {
				logger.info("Read Excel Failed");
			}
			logger.info("Save Excel Success");
			return workbook;
		});
		List<Integer> results = books.stream().map(this::readExcelAndSave).collect(Collectors.toList());
		return results.get(0).toString();
	}

	private int readExcelAndSave(XSSFWorkbook workbook) {
		AtomicInteger failNum = new AtomicInteger(0);
		XSSFSheet sheet = workbook.getSheetAt(0);
		sheet.rowIterator().forEachRemaining(row -> {
			if (row.getRowNum() >= BaseConst.EXCEL_START_ROW) {
				if(!this.saveRow(row)){
					failNum.getAndAdd(1);
				}
			}
		});
		return failNum.get();
	}
	
	private boolean saveRow(Row row) {
		boolean flag;
		Candidate candidate = new Candidate();
		candidate.setAvailableSchedule(this.cellToStr(row.getCell(2), row.getCell(2).getCellType()));
		candidate.setName(row.getCell(3).getStringCellValue());
		candidate.setCurrentProvince(row.getCell(4).getStringCellValue());
		candidate.setGender(row.getCell(5).getStringCellValue());
		candidate.setNationality(row.getCell(6).getStringCellValue());
		candidate.setNationalId(this.cellToStr(row.getCell(7),row.getCell(7).getCellType()));
		candidate.setPassportNumber(this.cellToStr(row.getCell(8),row.getCell(8).getCellType()));
		candidate.setBirthday(this.dateToStr(row.getCell(9),row.getCell(9).getCellType()));//row.getCell(8).getStringCellValue()//this.dateToStr(row.getCell(9).getDateCellValue(),row.getCell(9).getCellType())
		candidate.setBirthProvince(row.getCell(10).getStringCellValue());
		candidate.setHomePhone(this.cellToStr(row.getCell(11), row.getCell(11).getCellType()));
		candidate.setCellPhone(this.cellToStr(row.getCell(12), row.getCell(12).getCellType()));
		candidate.setMailAddress(row.getCell(13).getStringCellValue());
		candidate.setPersonalPage(row.getCell(14).getStringCellValue());
		candidate.setSkillsDescribe(row.getCell(15).getStringCellValue());
		candidate.setGraduatePlan(row.getCell(16).getStringCellValue());
		candidate.setGraduateDate(this.dateToStr(row.getCell(17),row.getCell(17).getCellType()));//Numeric//String.valueOf(row.getCell(16).getNumericCellValue())//this.dateToStr(row.getCell(17).getDateCellValue(),row.getCell(17).getCellType())
		
		candidate.setLanguageLevel(formateLanguageLevel(row.getCell(18).getStringCellValue()));
		
		
		List<EducationExperience> educationExperiences = new ArrayList<>();
		EducationExperience eduExp = new EducationExperience();
		eduExp.setDegree(row.getCell(19).getStringCellValue());
		eduExp.setUniversity(row.getCell(20).getStringCellValue());
		eduExp.setMajor(row.getCell(21).getStringCellValue());
		eduExp.setPeriod(this.cellToStr(row.getCell(22),row.getCell(22).getCellType()));
		eduExp.setGpa(this.cellToStr(row.getCell(23),row.getCell(23).getCellType()));
		educationExperiences.add(eduExp);
		candidate.setEducationExperiences(educationExperiences);
		
		List<WorkExperience> workExperiences = new ArrayList<>();
		WorkExperience workExp = new WorkExperience();
		workExp.setCompany(row.getCell(25).getStringCellValue());
		workExp.setPosition(row.getCell(26).getStringCellValue());
		workExp.setPeriod(row.getCell(27).getStringCellValue());
		workExp.setDetails(cellToStr(row.getCell(28),row.getCell(28).getCellType()));
		workExperiences.add(workExp);
		candidate.setWorkExperiences(workExperiences);
		
		Application application = new Application();
		application.setApplyPosition(row.getCell(1).getStringCellValue());
		
		String no = this.cellToStr(row.getCell(0), row.getCell(0).getCellType());// 20150101-001
		application.setApplyDate(LocalDate.parse(no, DateTimeFormatter.BASIC_ISO_DATE).toString());// 2015-01-01
		
		
		flag = iCandidateHandler.saveCandidateFromUpload(candidate, application);
		
		if(flag){
		Config config = new Config();
		logger.info("app:{}", application);
		config.setApplyNumber(application.getApplyDate().replaceAll("-", "") + "-" + CommonUtil.format4Digits(application.getApplyId().intValue()));
		config.setRoundMoveFlag("true");
		iEmailCrud.save(config);
		}
			
		Questionnaire questionnaire = new Questionnaire();
		questionnaire.setNationalId(this.cellToStr(row.getCell(7),row.getCell(7).getCellType()));// outside answer as nationalId
		questionnaire.setContent(JsonUtil.toNormalJson(iQuestionnaireHandler.searchQuestAndAnswer("english")));
		iQuestionnaireCrud.save(questionnaire);
			
		return flag;
	}

	private String dateToStr(Cell dateDate, int type) {
		if (type == 0) {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			String dateString = formatter.format(dateDate.getDateCellValue());
			return dateString;
		} else if (type == 1) {
			return dateDate.getStringCellValue();
		} else {
			return null;
		}
	}
	
	private String cellToStr(Cell cell, int type) {
		if (type == 0) {
			BigDecimal bigDecimal = new BigDecimal(cell.getNumericCellValue());  
	        String result = bigDecimal.toString();  
			return result;
		} else if (type == 1) {
			return cell.getStringCellValue();
		} else {
			return null;
		}
	}
	
	private String formateLanguageLevel(String languageLevel) {
		String[] formatLevel=new String[8];
		String formatlanguagelevel="";
		String level[]=languageLevel.split(",");
		for (String rightlevel:level){
			switch (rightlevel) {
			case "CET-4":
				formatLevel[0]="CET-4";
				break;
			case "CET-6":
				formatLevel[1]="CET-6";
				break;
			case "TEM-4":
				formatLevel[2]="TEM-4";
				break;
			case "TEM-8":
				formatLevel[3]="TEM-8";
				break;
			case "JPT-1":
				formatLevel[4]="JPT-1";
				break;
			case "JPT-2":
				formatLevel[5]="JPT-2";
				break;
			case "JPT-3":
				formatLevel[6]="JPT-3";
				break;
			default:
				if(formatLevel[7]!=null){
				formatLevel[7]=formatLevel[7]+","+rightlevel;
				}else{
					formatLevel[7]=rightlevel;
				}
				break;
			}
		}
		for(String finalLevel:formatLevel){
			if(finalLevel!=null){
			formatlanguagelevel=formatlanguagelevel+finalLevel+",";}
			else{
				formatlanguagelevel=formatlanguagelevel+",";
			}
		}
		return formatlanguagelevel.toUpperCase();
	}

}
