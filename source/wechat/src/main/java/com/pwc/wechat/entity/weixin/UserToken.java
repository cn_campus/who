package com.pwc.wechat.entity.weixin;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserToken {

	@JsonProperty(value = "access_token")
	private String accessToken;

	@JsonProperty(value = "expires")
	private String expires;

	@JsonProperty(value = "refresh_token")
	private String refreshToken;

	@JsonProperty(value = "openid")
	private String openId;

	@JsonProperty(value = "scope")
	private String scope;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getExpires() {
		return expires;
	}

	public void setExpires(String expires) {
		this.expires = expires;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

}
