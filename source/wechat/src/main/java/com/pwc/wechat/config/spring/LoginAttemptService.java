package com.pwc.wechat.config.spring;

import com.pwc.wechat.entity.user.User;
import com.pwc.wechat.entity.user.UserRole;
import com.pwc.wechat.service.hr.user.IUserRoleHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * For recording login attempt times Max Attempt is 5 times, and the
 * corresponding param is 4 (if count = 4,then is blocked )
 *
 * @author axia021
 */
@Service
public class LoginAttemptService {

    private Map<String, Integer> hrAttemptAccountMap = new HashMap<>();

    @Autowired
    private IUserRoleHandler iUserRoleHandler;

    @PostConstruct
    public void init() {
        List<UserRole> userRoles = iUserRoleHandler.searchUserRoles();
        for (UserRole userRole : userRoles) {
            if (userRole.getHrFlag() == true || userRole.getHrmFlag() == true) {
                hrAttemptAccountMap.put(userRole.getUserName(), 0);
            }
        }
    }

    public List<String> getLockedAccountList() {

        List<String> lockedAccountList = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : hrAttemptAccountMap.entrySet()) {
            if (entry.getValue() > 3)
                lockedAccountList.add(entry.getKey());
        }
        return lockedAccountList;
    }

    public Map<String, Integer> getHrAttemptAccountMap() {

        return hrAttemptAccountMap;
    }

    public void setHrAttemptAccountMap(Map<String, Integer> hrAttemptAccountMap) {

        this.hrAttemptAccountMap = hrAttemptAccountMap;
    }

    public List<User> unlockUsers(List<User> users) {

        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getEnabled()) {
                hrAttemptAccountMap.put(users.get(i).getUserName(), 0);
                users.remove(i);
            }
        }
        return users;
    }

    public void refreshHrAttemptAccountMap() {

        this.init();
    }
}
