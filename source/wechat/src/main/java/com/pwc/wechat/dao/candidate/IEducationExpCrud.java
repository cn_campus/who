package com.pwc.wechat.dao.candidate;

import com.pwc.wechat.entity.candidate.EducationExperience;
import com.pwc.wechat.entity.candidate.EducationExperiencePk;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IEducationExpCrud
        extends JpaRepository<EducationExperience, EducationExperiencePk> {

    public List<EducationExperience> findAllByNationalId(
            String nationalId);

    public void deleteAllByNationalId(
            String nationalId);

}
