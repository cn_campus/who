package com.pwc.wechat.config.constant;

import java.io.IOException;
import java.util.Properties;
import org.springframework.core.io.ClassPathResource;
import org.jasypt.util.text.BasicTextEncryptor;

public class MySQLConst {
	
	private MySQLConst () {
		
	}	

	public static String MYSQL_URL = GetValueByKey("MYSQL_URL");
	public static String MYSQL_DRIVER = GetValueByKey("MYSQL_DRIVER");
	public static String MYSQL_USER_NAME = GetValueByKey("MYSQL_USER_NAME");
	public static String MYSQL_PASSWORD = GetValueByKey("MYSQL_PASSWORD");

	public static String GetValueByKey(String key) {
		Properties pps = new Properties();

		BasicTextEncryptor passwordEecryptor = new BasicTextEncryptor();
		passwordEecryptor.setPassword("password");
		
		try {
			ClassPathResource cr = new ClassPathResource("application.properties");
			pps.load(cr.getInputStream());
			String decryptedPassword = passwordEecryptor.decrypt(pps.getProperty(key));
			return decryptedPassword;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
