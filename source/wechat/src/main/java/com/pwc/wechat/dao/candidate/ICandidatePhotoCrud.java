package com.pwc.wechat.dao.candidate;

import com.pwc.wechat.entity.candidate.CandidatePhoto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ICandidatePhotoCrud
        extends JpaRepository<CandidatePhoto, String> {

}

