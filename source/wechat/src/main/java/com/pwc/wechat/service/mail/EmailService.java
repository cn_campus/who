package com.pwc.wechat.service.mail;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.openxmlformats.schemas.drawingml.x2006.chart.STXstring;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itextpdf.text.log.SysoCounter;
import com.mysql.fabric.xmlrpc.base.Array;
import com.pwc.wechat.dao.candidate.HybridDao;
import com.pwc.wechat.dao.hr.IEmailCrud;
import com.pwc.wechat.entity.hr.Config;
import com.pwc.wechat.entity.hr.TalentData;
import com.pwc.wechat.entity.hr.TalentDataSet;
import com.pwc.wechat.entity.hr.TalentEmail;

import com.pwc.wechat.service.mail.JavaMail.MailSenderinfo;
import com.pwc.wechat.service.mail.JavaMail.SimpleMailSender;
import java.util.Comparator;

@Service("emailService")
public class EmailService implements IEmailHandler{
	
	@Autowired
	private IEmailCrud iEmailCrud;
	
	@Autowired
	private HybridDao hybridDao;
	
	public int sendEmailWithAttacments(TalentEmail talentemail, List<Path> resource) {
		AtomicInteger failNum = new AtomicInteger(0);
		MailSenderinfo mailInfo = new MailSenderinfo();// 生成邮件对象
//		mailInfo.setMailServerHost("smtp.163.com");
//		mailInfo.setMailServerPort("25");
//		mailInfo.setValidate(true);
//		mailInfo.setUserName("ChandlerYu@163.com");
//		mailInfo.setPassword("Pwcwelcome2");
//		mailInfo.setFromAddress("ChandlerYu@163.com");
		mailInfo.setMailServerHost("smtp.gmail.com");
		mailInfo.setMailServerPort("465");
		mailInfo.setValidate(true);
		mailInfo.setUserName("daidavid30@gmail.com");
		mailInfo.setPassword("dww123321");
		mailInfo.setFromAddress("CN_Campaus@pwc.com");
		
		
		talentemail.getTalentDatas().stream().filter(one -> one.getCheck() != null && one.getCheck()).forEach(one -> {
			
		//String mailAddress = one.getEmail();
		String subject = talentemail.getSubject();
		String context = talentemail.getContext();
		context = context.replace("Dear&nbsp;@checked", "Dear "+one.getName());
		context = context.replace("Dear @checked", "Dear "+one.getName());
		
		mailInfo.setToAddress(one.getEmail());
		mailInfo.setSubject(subject);
		mailInfo.setContent(context);
		
		SimpleMailSender sms = new SimpleMailSender();// 这个类主要来发送邮件
		
		

		//return sms.sendTextMail(mailInfo);
		try {
			failNum.addAndGet(sms.sendMimeMail(mailInfo,resource));
			//sms.sendMimeMail(mailInfo,resource);
		} catch (IOException e) {
			e.printStackTrace();
			failNum.getAndAdd(1);
		}
		
	});
		return failNum.get();
		
	}

	
	@Override
	public Config getEmailTemplate(String emailStatusTemplate) {
		return iEmailCrud.findAllByEmailStatusTemplate(emailStatusTemplate).stream().findFirst().orElse(null);
	}
	@Override
	public int sendEmail(TalentEmail talentemail,List<Path> files){
		int error=0;
		error = this.sendEmailWithAttacments(talentemail, files);
		return error;
	}
	
	@Override
	public String updateEmailTemplate(Config config) {
		Config emailtemp = iEmailCrud.findAllByEmailStatusTemplate(config.getEmailStatusTemplate()).stream().findFirst().orElse(null);
		emailtemp.setEmailSubjectTemplate(config.getEmailSubject());
		emailtemp.setEmailContextTemplate(config.getEmailContextTemplate());
				
				iEmailCrud.save(emailtemp);
			
		return "success";
		
	}


	@Override
	public TalentDataSet configToSet(TalentDataSet set) {
		set.getTalentDatas().forEach(one->{
			iEmailCrud.findAllByApplyNumber(one.getDate().replaceAll("-", "")+"-"+one.getAppId()).forEach(config->{
				if((config.getRoundMoveFlag()!=null)&&config.getRoundMoveFlag().equals("true")){
					one.setRoundmoveflag(config.getRoundMoveFlag());
				}else if((config.getMoveBackFlag()!=null)&&config.getMoveBackFlag().equals("true")){
					one.setMovebackflag(config.getMoveBackFlag());
				}else if((config.getSendEmailFlag()!=null)&&config.getSendEmailFlag().equals("true")){
					one.setSendemailflag(config.getSendEmailFlag());
				}
			});
		});
		//set.getTalentDatas().sort((a,b)->a.getRoundmoveflag() == null ? 1 : b.getRoundmoveflag() == null ? -1 : a.getRoundmoveflag().compareTo(b.getRoundmoveflag()));
		return set;
	}
	
	

}
