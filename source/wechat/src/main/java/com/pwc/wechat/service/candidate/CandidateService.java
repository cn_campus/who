package com.pwc.wechat.service.candidate;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.pwc.wechat.dao.candidate.HybridDao;
import com.pwc.wechat.dao.candidate.IApplicationCrud;
import com.pwc.wechat.dao.candidate.ICandidateCrud;
import com.pwc.wechat.dao.candidate.ICandidateHistoryCrud;
import com.pwc.wechat.dao.candidate.IEducationExpCrud;
import com.pwc.wechat.dao.candidate.IWorkExpCrud;
import com.pwc.wechat.dao.hr.IEmailCrud;
import com.pwc.wechat.entity.candidate.AppResult;
import com.pwc.wechat.entity.candidate.AppStatus;
import com.pwc.wechat.entity.candidate.Application;
import com.pwc.wechat.entity.candidate.ApplicationPk;
import com.pwc.wechat.entity.candidate.Candidate;
import com.pwc.wechat.entity.candidate.CandidateAllInfo;
import com.pwc.wechat.entity.candidate.CandidateHistory;
import com.pwc.wechat.entity.candidate.EducationExperience;
import com.pwc.wechat.entity.candidate.WorkExperience;
import com.pwc.wechat.entity.hr.Config;
import com.pwc.wechat.util.CommonUtil;
import com.pwc.wechat.util.DateUtil;
import com.pwc.wechat.util.JsonUtil;

@Service("candidateService")
public class CandidateService implements ICandidateHandler, IApplicationHandler{
	
	private static final Logger logger = LogManager.getLogger(CandidateService.class);
	
	@Autowired
	private HybridDao hybridDao;
	
	@Autowired
	private ICandidateCrud iCandidateCrud;
	
	@Autowired
	private IEmailCrud iEmailCrud;
	
	@Autowired
	private IApplicationCrud iApplicationCrud;
	
	@Autowired
	private IEducationExpCrud iEducationExpCrud;
	
	@Autowired
	private IWorkExpCrud iWorkExpCrud;
	
	@Autowired
	private ICandidateHistoryCrud iCandidateHistoryCrud;

	@Override
	public boolean saveCandidateFromPage(Candidate candidate, String position) {
		if (hybridDao.applicationAcceptable(candidate.getNationalId()) && position != null) {
			logger.info("CandidateFromPage accepted {}", candidate.getNationalId());
			if (this.saveOrUpdateCandidate(candidate)) {
				Application application = new Application();
				application.setApplyPosition(position);
				application.setNationalId(candidate.getNationalId());
				return this.saveApplication(application);
			}
		}
		logger.info("CandidateFromPage Not Acceptable {}", candidate.getNationalId());
		return false;
	}
	
	@Override
	public boolean saveCandidateFromUpload(Candidate candidate, Application application) {
		if (hybridDao.uploadApplicationAcceptable(candidate.getNationalId(), application.getApplyDate()) && application.getApplyDate() != null && application.getApplyId() == null) {
			logger.info("CandidateFromUpload accepted {}", candidate.getNationalId());
			if (this.saveOrUpdateCandidate(candidate)) {
				application.setNationalId(candidate.getNationalId());
				return this.saveApplication(application);
			}
		}
		logger.info("CandidateFromUpload Not Acceptable {}", candidate.getNationalId());
		return false;
	}
	
	@Override
	public Candidate findCandidateByNationalId(String nationalId) {
		Candidate candidate = iCandidateCrud.findOne(nationalId);
		if (candidate != null) {
			candidate.setWorkExperiences(iWorkExpCrud.findAllByNationalId(nationalId));
			candidate.setEducationExperiences(iEducationExpCrud.findAllByNationalId(nationalId));
		}
		return candidate;
	}

	@Override
	public Candidate findCandidateByWechatUid(String wechatUid) {
		Candidate candidate = iCandidateCrud.findOneByWechatUid(wechatUid);
		if (candidate != null) {
			candidate.setWorkExperiences(iWorkExpCrud.findAllByNationalId(candidate.getNationalId()));
			candidate.setEducationExperiences(iEducationExpCrud.findAllByNationalId(candidate.getNationalId()));
		}
		return candidate;
	}
	
	/**
	 * insert or update application
	 * nationalId is essential
	 * update only if date and id are not null
	 * @param application
	 */
	@Override
	public boolean saveApplication(Application application) {
		String nationalId = application.getNationalId();
		Assert.notNull(nationalId, "NationalId must not be null");
		logger.info("Handle Application NI:{}", nationalId);
		if (application.getApplyDate() != null && application.getApplyId() == null) {
			// upload application
			this.insertFromUpload(application);
			logger.info("Upload Application");
			return true;
		} else if (application.getApplyDate() == null && application.getApplyId() == null) {
			// acceptable application
			this.insertNewApplication(application);
			logger.info("Accept Application");
			return true;
		}
		logger.info("Handle Application NI:{} Failed", nationalId);
		return false;
	}
	
	@Override
	public boolean updateApplication(Application application) {
		iApplicationCrud.save(application);
		return true;
	}
	
	private void insertNewApplication(Application application) {
		String today = DateUtil.getLocalDate();
		application.setApplyDate(today);
		application.setApplyId(this.generateApplyId(today));
		application.setRound1Score(0);//initial value
		application.setRound2Score(0);//initial value
		application.setRound3Score(0);//initial value
		application.setStatus(AppStatus.STATUS_NEW);//initial value
		application.setResult(AppResult.RESULT_UNKNOWN);//initial value
		
		Config config = new Config();
		config.setApplyNumber(application.getApplyDate().replaceAll("-", "")+"-"+CommonUtil.format4Digits(Integer.valueOf(application.getApplyId())));
			config.setRoundMoveFlag("true");
			iEmailCrud.save(config);
		
		
		
		
		iApplicationCrud.save(application);
	}
	
	private void insertFromUpload(Application application) {
		application.setApplyId(this.generateApplyId(application.getApplyDate()));
		application.setRound1Score(0);//initial value
		application.setRound2Score(0);//initial value
		application.setRound3Score(0);//initial value
		application.setStatus(AppStatus.STATUS_NEW);//initial value
		application.setResult(AppResult.RESULT_UNKNOWN);//initial value
		iApplicationCrud.save(application);
	}
	
	private Integer generateApplyId(String date) {
		int ran = CommonUtil.random().nextInt(10000);
		if (iApplicationCrud.findOne(new ApplicationPk(date, ran)) == null) {
			return ran;
		} else {
			return this.generateApplyId(date);
		}
	}

	/**
	 * merge the two candidate entity if exist
	 * insert into database
	 * @param inputCandidate
	 */
	@Override
	public boolean saveOrUpdateCandidate(Candidate inputCandidate) {
		String nationalId = inputCandidate.getNationalId();
		Assert.notNull(nationalId, "NationalId must not be null");
		logger.info("Handle Candidate NI:{}", nationalId);
		List<EducationExperience> eduExpList = inputCandidate.getEducationExperiences();
		if (!eduExpList.isEmpty()) {
			for (int i = 0; i < eduExpList.size(); i++) {
				eduExpList.get(i).setNationalId(nationalId);
				eduExpList.get(i).setExperienceId(i);
				eduExpList.get(i).setLastExp(false);
			}
			eduExpList.get(eduExpList.size() - 1).setLastExp(true);
		}
		List<WorkExperience> workExpList = inputCandidate.getWorkExperiences();
		if (!workExpList.isEmpty()) {
			for (int i = 0; i < workExpList.size(); i++) {
				workExpList.get(i).setNationalId(nationalId);
				workExpList.get(i).setExperienceId(i);
				workExpList.get(i).setLastExp(false);
			}
			workExpList.get(workExpList.size() - 1).setLastExp(true);
		}
		this.executeInsertOrUpdate(inputCandidate);
		logger.info("Accept Candidate");
		return true;
	}
	
	/**
	 * 
	 * insertOrUpdate candidate table, insert candidateHistory table, deleteAndInsert work&Edu two tables
	 * 
	 */
	private void executeInsertOrUpdate(Candidate inputCandidate) {
		iWorkExpCrud.deleteAllByNationalId(inputCandidate.getNationalId());
		iEducationExpCrud.deleteAllByNationalId(inputCandidate.getNationalId());
		iCandidateCrud.save(inputCandidate);//insert or update can
		inputCandidate.getWorkExperiences().forEach(workExp -> {
			iWorkExpCrud.save(workExp);//insert
		});
		inputCandidate.getEducationExperiences().forEach(eduExp -> {
			iEducationExpCrud.save(eduExp);//insert
		});
		iCandidateHistoryCrud.save(this.genHistory(inputCandidate));//insert history
	}
	
	private CandidateHistory genHistory(Candidate candidate) {
		CandidateHistory candidateHistory = new CandidateHistory();
		candidateHistory.setHashCode(candidate.hashCode());
		candidateHistory.setNationalId(candidate.getNationalId());
		candidateHistory.setContent(JsonUtil.toNormalJson(candidate));
		return candidateHistory;
	}

	@Override
	public CandidateAllInfo findCandidateAllInfoByNationalId(String nationalId) {
		CandidateAllInfo candidateAllInfo = new CandidateAllInfo();
		candidateAllInfo.setCandidate(this.findCandidateByNationalId(nationalId));
		candidateAllInfo.setApplication(hybridDao.findLatestApplication(nationalId));
		candidateAllInfo.setQuestAndAnswer(hybridDao.findLatestQuestAndAnswer(nationalId));
		return candidateAllInfo;
	}
	
}
