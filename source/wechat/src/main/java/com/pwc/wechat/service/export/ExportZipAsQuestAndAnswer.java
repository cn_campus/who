package com.pwc.wechat.service.export;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pwc.wechat.dao.candidate.HybridDao;
import com.pwc.wechat.entity.candidate.Candidate;
import com.pwc.wechat.entity.candidate.QuestAndAnswer;
import com.pwc.wechat.service.candidate.ICandidateHandler;
import com.pwc.wechat.service.export.writer.IEntityWriter;

@Service("exportZipAsQuestAndAnswer")
public class ExportZipAsQuestAndAnswer extends AbstractExportZip<QuestAndAnswer> {
	
	@Autowired
	private IEntityWriter<QuestAndAnswer> questAndAnswerToPdf;

	@Autowired
	private HybridDao hybridDao;
	
	@Autowired
	private ICandidateHandler candidateService;
	
	@Override
	protected QuestAndAnswer searchByNationalId(String nationalId) {
		return hybridDao.findLatestQuestAndAnswer(nationalId);
	}

	@Override
	protected String getFileName(QuestAndAnswer entity) {
		Candidate candidate = candidateService.findCandidateByNationalId(entity.getAnswer());//outside Answer is nationalId
		return candidate.getName() + candidate.getNationalId();
	}

	@Override
	protected IEntityWriter<QuestAndAnswer> initWriter() {
		return questAndAnswerToPdf;
	}


}
