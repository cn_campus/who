package com.pwc.wechat.dao.candidate;

@FunctionalInterface
public interface ResultMapping<T> {

    public T mapResult(
            Object[] sqlResult,
            int num);

}
