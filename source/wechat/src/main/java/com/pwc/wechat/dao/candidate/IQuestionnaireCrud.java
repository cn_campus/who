package com.pwc.wechat.dao.candidate;

import com.pwc.wechat.entity.candidate.Questionnaire;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IQuestionnaireCrud
        extends JpaRepository<Questionnaire, Integer> {

    public List<Questionnaire> findAllByNationalIdOrderByIdDesc(
            String nationalId);

}
