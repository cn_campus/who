package com.pwc.wechat.dao.candidate;

import com.pwc.wechat.entity.candidate.Candidate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ICandidateCrud
        extends JpaRepository<Candidate, String> {

    public Candidate findOneByWechatUid(
            String wechatUid);

}
