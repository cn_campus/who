package com.pwc.wechat.controller.hr;


import com.pwc.wechat.entity.user.User;
import com.pwc.wechat.service.candidate.ICandidatePhotoHandler;
import com.pwc.wechat.service.candidate.InsertCandidateTestData;
import com.pwc.wechat.service.hr.user.IAvatarHandler;
import com.pwc.wechat.service.hr.user.IUserHandler;
import com.pwc.wechat.service.upload.IUploadAndProcess;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class LoginController {

    private static final Logger logger = LogManager.getLogger(LoginController.class);

    @Autowired
    private ICandidatePhotoHandler iCandidatePhotoHandler;

    @Autowired
    private InsertCandidateTestData insertCandidateTestData;

    @Autowired
    private IUserHandler iUserHandler;

    @Autowired
    private IAvatarHandler iAvatarHandler;

    @Autowired
    private IUploadAndProcess<String> uploadAvatar;

    @RequestMapping(value = "/login", produces = "text/html; charset=utf-8")
    public ModelAndView login(HttpServletRequest request) {

        logger.info("Login now");
        return new ModelAndView("login/HR_LogIn");
    }

    @RequestMapping(value = "/", produces = "text/html; charset=utf-8")
    public ModelAndView home(HttpServletRequest request) {

        logger.info("Welcome home");
        return new ModelAndView("hr/roundMove");
    }

    @RequestMapping(value = "/favicon.ico", produces = "image/x-icon; charset=utf-8")
    public ModelAndView getFavicon(HttpServletRequest request) {

        return new ModelAndView("redirect:/resources/images/icon/favicon.ico");
    }

    @RequestMapping(value = "/insertTestData", produces = "text/html; charset=utf-8")
    public String insertTestData(@RequestParam int num) {

        logger.info("insertTestData:{}", num);
        return insertCandidateTestData.insert(num);
    }


    @RequestMapping(value = "/verifyPassword", produces = "text/html; charset=utf-8")
    public String verifyPassword(@RequestBody User user) {

        logger.info("verify last password");
        return (iUserHandler.verifyPassword(user)) ? "valid" : "invalid";
    }

    @RequestMapping(value = "/changePassword", produces = "text/html; charset=utf-8")
    public void changePassword(@RequestBody User user) {

        logger.info("change password");
        iUserHandler.changePassword(user);
    }

    @RequestMapping(value = "/uploadAvatar", produces = "text/html; charset=utf-8", method = RequestMethod.POST)
    public void uploadAvatar(@RequestParam(value = "userName") String userName, HttpServletRequest request) {

        logger.info("upload avatar");
        uploadAvatar.getUploadFile(request, userName);
    }

    @RequestMapping(value = "/getAvatar", produces = "text/html; charset=utf-8")
    public String getAvatar(@RequestParam(value = "userName") String userName, HttpServletRequest request, HttpServletResponse response) {

        logger.info("get avatar");
        return iAvatarHandler.createAvatar(userName, request, response);
    }

    @RequestMapping(value = "/getPhoto", produces = "text/html; charset=utf-8")
    public String getPhoto(@RequestParam(value = "nationalId") String nationalId, HttpServletRequest request, HttpServletResponse response) {

        logger.info("get =====photo , and nationalId is" + nationalId);
        return iCandidatePhotoHandler.createPhoto(nationalId, response);
    }
}
