package com.pwc.wechat.controller.hr;

import com.pwc.wechat.entity.hr.MultiCondition;
import com.pwc.wechat.entity.hr.TalentDataSet;
import com.pwc.wechat.service.export.IExportByNationalIds;
import com.pwc.wechat.service.hr.round.IMultiSearch;
import com.pwc.wechat.service.upload.IUploadAndProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class ExportResumeController {

    @Autowired
    private IExportByNationalIds exportZipAsCandidate;

    @Autowired
    private IExportByNationalIds exportZipAsPrettyCandidate;

    @Autowired
    private IExportByNationalIds exportZipAsQuestAndAnswer;

    @Autowired
    private IExportByNationalIds exportFileAsArray;

    @Autowired
    private IUploadAndProcess<String> importFromExcel;

    @Autowired
    private IMultiSearch<TalentDataSet, MultiCondition> searchTalentData;

    @RequestMapping(value = "/exportCandidateZip", produces = "text/html; charset=utf-8")
    public void exportCandidateZip(@RequestParam String[] nationalIds, HttpServletResponse response) throws Exception {

        exportZipAsCandidate.executeFlush(nationalIds, response);
    }

    @RequestMapping(value = "/exportPrettyCandidateZip", produces = "text/html; charset=utf-8")
    public void exportPrettyCandidateZip(@RequestParam String[] nationalIds, HttpServletResponse response) throws Exception {

        exportZipAsPrettyCandidate.executeFlush(nationalIds, response);
    }

    @RequestMapping(value = "/exportQuestionnaireZip", produces = "text/html; charset=utf-8")
    public void exportQuestionnaireZip(@RequestParam String[] nationalIds, HttpServletResponse response) throws Exception {

        exportZipAsQuestAndAnswer.executeFlush(nationalIds, response);
    }

    @RequestMapping(value = "/exportExcel", produces = "text/html; charset=utf-8")
    public void exportExcel(@RequestParam String[] nationalIds, HttpServletResponse response) throws Exception {

        exportFileAsArray.executeFlush(nationalIds, response);
    }

    @RequestMapping(value = "/uploadExcel", produces = "text/html; charset=utf-8")
    public String uploadExcel(HttpServletRequest request) {

        return importFromExcel.getUploadFile(request);
    }

    @RequestMapping(value = "/exportAllExcel", produces = "application/json; charset=utf-8")
    public String[] exportAllExcel(@RequestBody MultiCondition multiCondition, HttpServletResponse response) throws Exception {

        //return all nationalIds
        TalentDataSet set = searchTalentData.searchAllByMultiCondition(multiCondition);
        String[] nationalIds = new String[set.getTalentDatas().size()];
        for (int i = 0; i < set.getTalentDatas().size(); i++) {
            String aString = set.getTalentDatas().get(i).getNationalId();
            nationalIds[i] = set.getTalentDatas().get(i).getNationalId();
        }
        return nationalIds;
    }
}
