package com.pwc.wechat.service.candidate;

import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.config.constant.BaseConst;
import com.pwc.wechat.entity.candidate.Application;
import com.pwc.wechat.entity.candidate.Candidate;
import com.pwc.wechat.entity.candidate.CandidateAllInfo;
import com.pwc.wechat.entity.candidate.CandidatePhoto;

public interface ICandidateHandler {

	@Transactional(readOnly = false, rollbackFor = Exception.class, timeout = BaseConst.TRANSACTION_TIMEOUT)
	public boolean saveCandidateFromPage(Candidate candidate, String positon);
	
	@Transactional(readOnly = false, rollbackFor = Exception.class, timeout = BaseConst.TRANSACTION_TIMEOUT)
	public boolean saveCandidateFromUpload(Candidate candidate, Application application);
	
	@Transactional(readOnly = false, rollbackFor = Exception.class, timeout = BaseConst.TRANSACTION_TIMEOUT)
	public boolean saveOrUpdateCandidate(Candidate candidate);
	
	@Transactional(readOnly = true, rollbackFor = Exception.class)
	public Candidate findCandidateByNationalId(String nationalId);
	
	@Transactional(readOnly = true, rollbackFor = Exception.class)
	public Candidate findCandidateByWechatUid(String wechatUid);
	
	@Transactional(readOnly = true, rollbackFor = Exception.class)
	public CandidateAllInfo findCandidateAllInfoByNationalId(String nationalId);
			
}
