package com.pwc.wechat.controller.hr;

import com.pwc.wechat.config.spring.LoginAttemptService;
import com.pwc.wechat.entity.user.User;
import com.pwc.wechat.entity.user.UserRole;
import com.pwc.wechat.service.hr.user.IUserHandler;
import com.pwc.wechat.service.hr.user.IUserRoleHandler;
import com.pwc.wechat.service.upload.IUploadAndProcess;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/forHrm")
public class ForHrmController {

    @Autowired
    private IUserRoleHandler iUserRoleHandler;

    @Autowired
    private IUserHandler iUserHandler;

    @Autowired
    private IUploadAndProcess<String> candidatePhotoService;

    @Autowired
    private LoginAttemptService loginAttemptService;

    private static final Logger logger = LogManager.getLogger(ForHrmController.class);

    @RequestMapping(value = {"/newHr"}, produces = "text/html; charset=utf-8")
    public ModelAndView newHr(HttpServletRequest request) {

        logger.info("HR Manager Welcome");
        return new ModelAndView("hrm/hrmAuthorization");
    }

    @RequestMapping(value = "/addNewUser", produces = "application/json; charset=utf-8")
    public List<UserRole> addNewUser(@RequestBody User user) {

        iUserHandler.insertUser(user);
        return this.getAuthority();
    }

    @RequestMapping(value = "/updateAuthority", produces = "application/json; charset=utf-8")
    public List<UserRole> updateAuth(@RequestBody List<UserRole> userRoles) {

        userRoles.forEach((UserRole userRole) -> {
            iUserRoleHandler.updateUserRole(userRole);
        });
        return this.getAuthority();
    }

    @RequestMapping(value = "/getUser", produces = "application/json; charset=utf-8")
    public List<UserRole> getAuthority() {

        return iUserRoleHandler.searchUserRoles();
    }

    @RequestMapping(value = "/uploadPhoto", produces = "text/html; charset=utf-8")
    public String uploadPhoto(@RequestParam(value = "nationalId") String nationalId, HttpServletRequest request) {

        logger.info("upload candidate's photo and its nationalId is " + nationalId);
        if (candidatePhotoService.getUploadFile(request, nationalId).equals("success"))
            return "success";
        else
            return "failed";
    }

    @RequestMapping(value = "/getLockedAccount", produces = "application/json; charset=utf-8")
    public List<User> getLockedAccount() {

        List<User> lockedAccounts = new ArrayList<>();
        for (String username : loginAttemptService.getLockedAccountList()) {
            User user = new User();
            user.setUserName(username);
            user.setEnabled(false);
            lockedAccounts.add(user);
        }
        return lockedAccounts;
    }

    @RequestMapping(value = "/unlockUser", produces = "application/json; charset=utf-8")
    public List<User> unlockUser(@RequestBody List<User> lockedAccounts) {

        List<User> stillLockedAccount = loginAttemptService.unlockUsers(lockedAccounts);
        return stillLockedAccount;
    }
}
