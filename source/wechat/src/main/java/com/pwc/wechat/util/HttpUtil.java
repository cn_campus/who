package com.pwc.wechat.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.pwc.wechat.config.constant.BaseConst;
import com.pwc.wechat.entity.weixin.UserToken;

public class HttpUtil {
	
	private HttpUtil() {
		
	}
	
	private static final Logger logger = LogManager.getLogger(HttpUtil.class);
	
	public static String toRedirectUrl(String redirectUri, String status) throws UnsupportedEncodingException {
		String encode = URLEncoder.encode(redirectUri, "UTF-8");
		status = status == null || status.length() == 0 ? "status" : status;
		return "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + BaseConst.WEIXIN_APP_ID + "&redirect_uri=" + encode + "&response_type=code&scope=snsapi_base&state=" + status + "#wechat_redirect";
	}
	
	public static String getMessageFromRequest(HttpServletRequest request) throws IOException {
		StringBuilder message = new StringBuilder();
		Reader reader = new InputStreamReader(request.getInputStream());
		BufferedReader bufferedReader = new BufferedReader(reader);
		String line = null;
		while ((line = bufferedReader.readLine()) != null) {
			message.append(line);
		}
		return message.toString();
	}
	
	public static void writeMessagetoResponse(HttpServletResponse response, String str) throws IOException {
		BufferedWriter bufferedWriter = new BufferedWriter(response.getWriter());
		bufferedWriter.write(str);
		bufferedWriter.close();
	}
	
	public static void refreshToken() throws IOException {
		Map<String, String> params = new HashMap<>();
		params.put("grant_type", BaseConst.WEIXIN_TOKEN_GRANT_TYPE);
		params.put("appid", BaseConst.WEIXIN_APP_ID);
		params.put("secret", BaseConst.WEIXIN_APP_SECRET);
		JsonNode jsonNode = JsonUtil.readAsJsonNode(sendHttpsGet(BaseConst.WEIXIN_TOKEN_URL, params));
		BaseConst.WEIXIN_ACCESS_TOKEN = jsonNode == null ? BaseConst.WEIXIN_ACCESS_TOKEN : jsonNode.findValue("access_token").asText();
		BaseConst.WEIXIN_EXPIRES_IN = jsonNode == null ? BaseConst.WEIXIN_EXPIRES_IN : jsonNode.findValue("expires_in").asInt();
		logger.info("refresh_access_token {} {}", BaseConst.WEIXIN_ACCESS_TOKEN, BaseConst.WEIXIN_EXPIRES_IN);
	}
	
	public static String getUserOpenId(String code) throws IOException {
		Map<String, String> params = new HashMap<>();
		params.put("appid", BaseConst.WEIXIN_APP_ID);
		params.put("secret", BaseConst.WEIXIN_APP_SECRET);
		params.put("code", code);
		params.put("grant_type", BaseConst.WEIXIN_USER_TOKEN_GRANT_TYPE);
		UserToken userToken = JsonUtil.toNormalObject(sendHttpsGet(BaseConst.WEIXIN_USER_TOKEN_URL, params), UserToken.class);
		logger.info("get_user_openid {}", userToken.getOpenId());
		return userToken.getOpenId();
	}
	
	public static String sendHttpsGet(String url, Map<String, String> params) throws IOException {
		String httpsUrl = makeHttpsUrl(url, params);
		return sendHttpsGet(httpsUrl);
	}
	
	private static String makeHttpsUrl(String url, Map<String, String> params) {
		StringBuilder finalUrl = new StringBuilder();
		finalUrl.append("https://");
		finalUrl.append(url);
		boolean isfirst = true;
		for (Entry<String, String> entry : params.entrySet()) {
			finalUrl.append(isfirst ? "?" : "&");
			finalUrl.append(entry.getKey() + "=" + entry.getValue());
			isfirst = false;
		}
		return finalUrl.toString();
	}
	
	private static String sendHttpsGet(String finalUrl) throws IOException {
		URL url = new URL(finalUrl);
		HttpsURLConnection connect = (HttpsURLConnection) url.openConnection();
		connect.setReadTimeout(5000);
		connect.setRequestMethod("GET");
		connect.setDoOutput(true);
		connect.setDoInput(true);
		connect.setUseCaches(false);
		InputStreamReader reader = new InputStreamReader(connect.getInputStream());
		BufferedReader bufferedReader = new BufferedReader(reader);
		StringBuilder response = new StringBuilder();
		String line = null;
		while ((line = bufferedReader.readLine()) != null) {
			response.append(line);
		}
		connect.disconnect();
		return response.toString();
	}

}
