package com.pwc.wechat.config.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Component
public class AuthenticationSuccessHandlerImpl
        extends SimpleUrlAuthenticationSuccessHandler {

    @Autowired
    private LoginAttemptService loginAttemptService;

    @Override
    public void onAuthenticationSuccess(
            HttpServletRequest request,
            HttpServletResponse response,
            Authentication authentication)
            throws IOException,
            ServletException {

        Map<String, Integer> hrAttemptAccountMap = loginAttemptService.getHrAttemptAccountMap();
        String thisUser = request.getParameter("username");
        String thisPsw = request.getParameter("password");
        int count = -1;
        if (hrAttemptAccountMap.containsKey(thisUser)) {
            count = hrAttemptAccountMap.get(thisUser);
            if (count > 3) {
                request.getRequestDispatcher("/login?error=blocked").forward(request, response);
            } else {
                count = 0;
                hrAttemptAccountMap.put(thisUser, count);
                handle(request, response, authentication);
                clearAuthenticationAttributes(request);
            }
        } else {
            handle(request, response, authentication);
            clearAuthenticationAttributes(request);
        }
    }
}