package com.pwc.wechat.dao.candidate;

import com.pwc.wechat.entity.candidate.Candidate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CandidatePagingAndSorting
        extends PagingAndSortingRepository<Candidate, String> {

    public Page<Candidate> findAllByGenderAndWechatUid(
            String gender,
            String wechatUid,
            Pageable pageable);
}
