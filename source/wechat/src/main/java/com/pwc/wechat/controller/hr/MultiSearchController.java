package com.pwc.wechat.controller.hr;

import com.pwc.wechat.dao.hr.IEmailCrud;
import com.pwc.wechat.entity.candidate.CandidateAllInfo;
import com.pwc.wechat.entity.hr.*;
import com.pwc.wechat.service.candidate.ICandidateHandler;
import com.pwc.wechat.service.hr.round.IHrCommentHandler;
import com.pwc.wechat.service.hr.round.IMultiSearch;
import com.pwc.wechat.service.hr.round.ITalentDataHandler;
import com.pwc.wechat.service.hr.round.ITalentDataSetHandler;
import com.pwc.wechat.service.mail.IEmailHandler;
import com.pwc.wechat.service.upload.IGetUploadFile;
import com.pwc.wechat.service.upload.IUploadAndProcess;
import com.pwc.wechat.util.CommonUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/forHr")
public class MultiSearchController {

    @Autowired
    private IMultiSearch<TalentDataSet, MultiCondition> searchTalentData;

    @Autowired
    private ITalentDataHandler iTalentDataHandler;

    @Autowired
    private ITalentDataSetHandler iTalentDataSetHandler;

    @Autowired
    private IHrCommentHandler iHrCommentHandler;

    @Autowired
    private ICandidateHandler candidateService;

    @Autowired
    private IEmailHandler emailService;

    @Autowired
    private IEmailCrud iEmailCrud;

    @Autowired
    private IGetUploadFile<TalentFile> iGetUploadFile;

    private static final Logger logger = LogManager.getLogger(MultiSearchController.class);

    @RequestMapping(value = "/multiSearch", produces = "application/json; charset=utf-8")
    public TalentDataSet multiSearch(@RequestBody MultiCondition multiCondition) {

        TalentDataSet set = searchTalentData.searchByMultiCondition(multiCondition);
        set = emailService.configToSet(set);
        return set;
    }

    @RequestMapping(value = "/saveTalentData", produces = "text/html; charset=utf-8")
    public String saveTalentData(@RequestBody TalentDataSet talentDataSet) {

        return iTalentDataSetHandler.updateTalentDataSet(talentDataSet);
    }

    @RequestMapping(value = "/roundMoveTalentData", produces = "text/html; charset=utf-8")
    public String roundMoveTalentData(@RequestBody TalentDataSet talentDataSet) {

        return iTalentDataSetHandler.roundMoveTalentDataSet(talentDataSet);
    }

    @RequestMapping(value = "/moveBackTalentData", produces = "text/html; charset=utf-8")
    public String moveBackTalentData(@RequestBody TalentDataSet talentDataSet) {

        return iTalentDataSetHandler.moveBackTalentDataSet(talentDataSet);
    }

    @RequestMapping(value = "/toTrash", produces = "text/html; charset=utf-8")
    public String toTrash(@RequestBody TalentData talentData) {

        return iTalentDataHandler.moveToTrash(talentData);
    }

    @RequestMapping(value = "/batchTotrash", produces = "text/html; charset=utf-8")
    public void batchTotrash(@RequestBody TalentDataSet talentDataSet) {

        talentDataSet.getTalentDatas().forEach((TalentData talentData) -> {
            if (talentData.getCheck() != null && talentData.getCheck()) {
                iTalentDataHandler.moveToTrash(talentData);
            }
        });
    }

    @RequestMapping(value = "/recoverFromTrash", produces = "text/html; charset=utf-8")
    public String recoverFromTrash(@RequestBody TalentData talentData) {

        Config config = new Config();
        config.setApplyNumber(talentData.getDate().replaceAll("-", "") + "-" + talentData.getAppId());
        config.setRoundMoveFlag("true");
        iEmailCrud.save(config);
        return iTalentDataHandler.recoverFromTrash(talentData);
    }

    @RequestMapping(value = "/getHrComment", produces = "application/json; charset=utf-8")
    public HrComment getHrComment(@RequestBody TalentData talentData) {

        Config config = new Config();
        config.setApplyNumber(talentData.getDate().replaceAll("-", "") + "-" + talentData.getAppId());
        iEmailCrud.findAllByApplyNumber(config.getApplyNumber()).forEach(person -> {
            iTalentDataSetHandler.delmoveflag(person);
        });
        return iHrCommentHandler.searchHrComment(talentData.getNationalId());
    }

    @RequestMapping(value = "/saveHrComment", produces = "text/html; charset=utf-8")
    public String saveHrComment(@RequestBody HrComment hrComment) {

        iHrCommentHandler.updateHrComment(hrComment);
        return "success";
    }

    @RequestMapping(value = "/getCandidateAllInfo", produces = "application/json; charset=utf-8")
    public CandidateAllInfo getCandidateInfo(@RequestBody TalentData talentData) {

        if ((talentData.getRoundmoveflag() != null) && talentData.getRoundmoveflag().equals("true")) {
            Config config = new Config();
            config.setApplyNumber(talentData.getDate().replaceAll("-", "") + "-" + talentData.getAppId());
            iTalentDataSetHandler.delmoveflag(
                    iEmailCrud.findAllByApplyNumber(config.getApplyNumber()).stream().findFirst().orElse(null));

        } else if ((talentData.getMovebackflag() != null) && talentData.getMovebackflag().equals("true")) {
            Config config = new Config();
            config.setApplyNumber(talentData.getDate().replaceAll("-", "") + "-" + talentData.getAppId());
            iTalentDataSetHandler.delmoveflag(
                    iEmailCrud.findAllByApplyNumber(config.getApplyNumber()).stream().findFirst().orElse(null));
        }
        return candidateService.findCandidateAllInfoByNationalId(talentData.getNationalId());

    }

    // get tanlentEmail subject and context template
    @RequestMapping(value = "/getEmailTemplate", produces = "application/json; charset=utf-8")
    public Config getEmailTemplate(@RequestBody String emailStatusTemplate) {

        return emailService.getEmailTemplate(emailStatusTemplate);
    }

    @RequestMapping(value = "/updateEmailTemplate", produces = "text/html; charset=utf-8")
    public String updateEmailTemplate(@RequestBody Config emailconfig) {

        return emailService.updateEmailTemplate(emailconfig);
    }

    @RequestMapping(value = "/sendemail", produces = "text/html; charset=utf-8")
    public String sendemail(@RequestBody TalentEmail talentemail) throws IOException {

        File folder = new File("C:\\Email");
        if (folder.exists() && folder.isDirectory()) {

        } else {
            folder.mkdirs();
        }

        Path path = Files.createTempDirectory(Paths.get("C:/Email"), "TempAttach");
        List<Path> files = talentemail.getAttachment().stream().map(talentfile -> {
            Path filePath = path.resolve(talentfile.getFilename());
            try {
                OutputStream outputStream = Files.newOutputStream(filePath);
                outputStream.write(talentfile.getFilecontexnt());
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return filePath;
        }).collect(Collectors.toList());
        int error = emailService.sendEmail(talentemail, files);

        files.forEach(file -> {
            try {
                Files.delete(file);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        });
        Files.delete(path);
        if (error == 0) {

            return "success";
        } else {
            return "error";
        }
    }

    @RequestMapping(value = "/sendattachment", produces = "application/json; charset=utf-8")
    public List<TalentFile> sendattachment(HttpServletRequest request) {

        IUploadAndProcess<List<TalentFile>> uploadAttachment = (req, attrs) -> iGetUploadFile.getUploadFile(request,
                file -> {
                    TalentFile talentfile = new TalentFile();
                    try {
                        talentfile.setFilecontexnt(file.getBytes());
                        talentfile.setFilename(file.getOriginalFilename());
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    return talentfile;
                });
        return uploadAttachment.getUploadFile(request);
    }

    @RequestMapping(value = "/addroundmoveflag", produces = "text/html; charset=utf-8")
    public String addroundmoveflag(@RequestBody TalentDataSet talentDataSet) {

        talentDataSet.getTalentDatas().forEach(one -> {
            if (one.getCheck() != null && one.getCheck()) {
                Config config = new Config();
                config.setApplyNumber(one.getDate().replaceAll("-", "") + "-" + CommonUtil.format4Digits(Integer.valueOf(one.getAppId())));
                config.setRoundMoveFlag("true");
                iEmailCrud.save(config);
            }
        });
        return "success";
    }

    @RequestMapping(value = "/addsendEmailflag", produces = "text/html; charset=utf-8")
    public void addsendEmailflag(@RequestBody TalentDataSet talentDataSet) {

        talentDataSet.getTalentDatas().forEach((TalentData talentData) -> {
            if (talentData.getCheck() != null && talentData.getCheck()) {
                Config email = new Config();
                email.setApplyNumber(talentData.getDate().replaceAll("-", "") + "-" + talentData.getAppId());
                email.setSendEmailFlag("true");
                iTalentDataSetHandler.addsendEmailflag(email);
            }
        });
    }

    @RequestMapping(value = "/delmoveflag", produces = "text/html; charset=utf-8")
    public void delroundmoveflag(@RequestBody TalentDataSet talentDataSet) {

        talentDataSet.getTalentDatas().forEach((TalentData talentData) -> {
            if (talentData.getCheck() != null && talentData.getCheck()) {
                Config config = new Config();
                config.setApplyNumber(talentData.getDate().replaceAll("-", "") + "-" + talentData.getAppId());
                iEmailCrud.findAllByApplyNumber(config.getApplyNumber()).forEach(person -> {
                    iTalentDataSetHandler.delmoveflag(person);
                });
            }
        });
    }
}
