package com.pwc.wechat.service.hr.round;

import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.entity.hr.Config;
import com.pwc.wechat.entity.hr.TalentDataSet;

public interface ITalentDataSetHandler {
	
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public String updateTalentDataSet(TalentDataSet talentDataSet);
	
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public String roundMoveTalentDataSet(TalentDataSet talentDataSet);
	
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public String moveBackTalentDataSet(TalentDataSet talentDataSet);
	
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public String addmoveflag(Config config);
	
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void delmoveflag(Config config);
	
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void addsendEmailflag(Config config);

}
