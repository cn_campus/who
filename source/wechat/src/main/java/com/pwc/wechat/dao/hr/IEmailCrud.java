package com.pwc.wechat.dao.hr;

import com.pwc.wechat.entity.hr.Config;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface IEmailCrud
        extends JpaRepository<Config, String> {

    public List<Config> findAllByEmailStatusTemplate(
            String status);

    public List<Config> findAllByApplyNumber(
            String ApplyNumber);

}
