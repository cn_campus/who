package com.pwc.wechat.dao.hr;

import com.pwc.wechat.entity.hr.HrComment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IHrCommentCrud
        extends JpaRepository<HrComment, String> {

}
