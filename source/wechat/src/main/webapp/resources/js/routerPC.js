var app = angular.module('CandidateModulePC', ['ngAnimate', 'ui.bootstrap', 'ui.router']);
app.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise('/basic');
    $stateProvider
        .state('basic', {
            url: '/basic',
            views: {
                'sideMenu': {
                    templateUrl: '/wechat/html/candidatePC/side.html'
                },
                'content': {
                    templateUrl: '/wechat/html/candidatePC/basicInformation.html'
                }
            }
        })
        .state('education', {
            url: '/education',
            views: {
                'sideMenu': {
                    templateUrl: '/wechat/html/candidatePC/side.html'
                },
                'content': {
                    templateUrl: '/wechat/html/candidatePC/educationSkill.html'
                }
            }
        })
        .state('survey', {
            url: '/survey',            
            views: {
                'sideMenu': {
                    templateUrl: '/wechat/html/candidatePC/side.html'
                },
                'content': {
                    templateUrl: '/wechat/html/candidatePC/survey.html'
                }
            }
        })
        .state('uploadPhoto', {
            url: '/uploadPhoto',           
            views: {
                'sideMenu': {
                    templateUrl: '/wechat/html/candidatePC/side.html'
                },
                'content': {
                    templateUrl: '/wechat/html/candidatePC/uploadPhoto.html'
                }
            }
        })
        .state('provision', {
            url: '/provision',
            views: {
                'sideMenu': {
                    templateUrl: '/wechat/html/candidatePC/side.html'
                },
                'content': {
                    templateUrl: '/wechat/html/candidatePC/provision.html'
                }
            }
        })
        .state('contact', {
            url: '/contact',
            views: {
                'sideMenu': {
                    templateUrl: '/wechat/html/candidatePC/side.html'
                },
                'content': {
                    templateUrl: '/wechat/html/candidatePC/contact.html'
                }
            }
        })
});
