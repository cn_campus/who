// upload photo
app.directive('fileModel', ['$parse', function($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs, ngModel) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            element.bind('change', function() {
                scope.$apply(function() {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

app.controller('candidateController',function( $rootScope,$scope, $http,$location,$anchorScroll, $filter,  $interval, $timeout, $uibModal, $state) {
    $scope.candidate_stap = 0;
    $scope.candidate = {
        'applicationPosition': "Associate Software Engineer",
        // 'nationalId':'123456789098765432',
        'availableSchedule': 3,
        'graduatePlan': "Work",
        'gender': 'male'
    };
    $scope.status = {};
    $scope.title = {};
    $scope.question = {};
    $scope.forms = {};
    $scope.candidate.educationExperiences = [{}];
    $scope.candidate.workExperiences = [{}];
    $scope.languageLevel = [];
    $scope.university = {};
    $scope.university.province = {};
    $scope.university.city = {};
    $scope.signatureDialog = {};
    $scope.loadingToast = {};
    $scope.successToast = {};
    $scope.notice = {};
    $scope.languageLevelPreview = [];
    $scope.languages = ["English", "中文"];
    $scope.selectedLanguage = "English";
    $scope.LanguageSkillinvalid = true;
    $scope.LanguageSkillisChoose = false;
    $scope.isUniversityTouched = false;
    $scope.isMajorTouched = false;
    $scope.questionNull = true;
    $scope.isEduNull = true;
    $scope.source = config.params.source;
    $scope.title.positionOptions = ["Associate Software Engineer", "Associate Testing Engineer",
        "Associate Analyst", "Associate UI Designer",
        "PQA Intern", "Admin Intern",
        "HR Intern", "Finance Intern"
    ];
    $scope.title.availabilityOptions = [{ "value": 3, "option": "Three days" },
        { "value": 4, "option": "Four days" },
        { "value": 5, "option": "Five days" }
    ];
    $scope.title.graduatePlanOptions = ["Work", "Go abroad", "Civil servant", "Graduate Studies", "Other"];

    $scope.title.universityOption = [
        { province: '上海', cities: [{ city: '上海', universities: ['上海中医药大学', '上海交通大学', '上海商学院', '上海大学', '上海对外经贸大学', '上海工程技术大学', '上海师范大学', '上海应用技术大学', '上海政法学院', '上海海事大学', '上海海关学院', '上海海洋大学', '上海理工大学', '上海电力学院', '上海电机学院', '上海立信会计学院', '上海第二工业大学', '上海财经大学', '上海金融学院', '东华大学', '华东师范大学', '华东政法大学', '华东理工大学', '同济大学', '复旦大学', '上海纽约大学', '上海外国语大学'] }, { city: '其他' }] },
        { province: '辽宁', cities: [{ city: '大连', universities: ['东北财经大学', '大连海事大学', '大连理工大学', '大连外国语大学', '大连交通大学'] }, { city: '其他' }] }, {
            province: '江苏',
            cities: [{ city: '南京', universities: ['东南大学', '中国药科大学', '南京中医院大学', '南京信息工程大学', '南京农业大学', '南京医科大学', '南京大学', '南京审计学院', '南京工业大学', '南京师范大学', '南京林业大学', '南京航空航天大学', '南京财经大学', '南京邮电大学', '河海大学'] },
                { city: '苏州', universities: ['苏州大学', '西交利物浦大学'] }, { city: '其他' }
            ]
        }, {
            province: '浙江',
            cities: [{ city: '杭州', universities: ['中国美术学院', '中国计量学院', '杭州师范大学', '杭州电子科技大学', '浙江大学', '浙江工业大学', '浙江工商大学', '浙江理工大学'] },
                { city: '宁波', universities: ['宁波大学', '宁波诺丁汉大学'] }, { city: '其他' }
            ]
        },
        { province: '湖北', cities: [{ city: '武汉', universities: ['中南民族大学', '中南财经政法大学', '中国地质大学(武汉)', '华中农业大学', '华中师范大学', '华中科技大学', '武汉大学', '武汉工程大学', '武汉理工大学', '武汉科技大学', '湖北中医药大学', '湖北大学', '湖北工业大学'] }, { city: '其他' }] },
        { province: '其他' }
    ];

    $scope.title.major = [
        { division: '哲学', sections: ['哲学类'] },
        { division: '经济学', sections: ['经济学类'] },
        { division: '管理学', sections: ['管理科学与工程类', '工商管理类', '行政管理、公共管理类', '图书档案学类'] },
        { division: '文学', sections: ['语言文学类', '新闻传播学类', '艺术类'] },
        { division: '工学', sections: ['电气信息类', '计算机科学与技术类', '机械类', '土建类', '生物医学工程类', '仪器仪表类', '能源动力类', '水利类', '材料类', '制药工程类', '交通运输类', '船舶与海洋工程类', '测绘类', '轻工纺织食品类', '武器类', '公安技术类', '航空航天类'] },
        { division: '历史学', sections: ['历史学类'] },
        { division: '法学', sections: ['法学类'] },
        { division: '理学', sections: ['数学类', '物理学类', '化学类及化学工程类', '生物科学及生物技术类', '天文地质地理类', '力学类', '电子信息科学类', '系统科学类', '环境科学与安全类'] },
        { division: '教育学', sections: ['教育学类'] },
        { division: '医学', sections: ['医学类', '心理学类'] },
        { division: '农学', sections: ['农业类'] },
    ];
    $scope.title.degrees = ["Bachelor", "Master", "Doctor"];
    $scope.availableSchedule = $scope.title.availabilityOptions[0].option;
    getQuestions("english"); //init PC survey language
    mobileGetQuestion(); //init mobile survey with two language
    $scope.dateOptions = { //init PC calendar
        // formatYear: 'yyyy',
        maxDate: new Date(2020, 5, 22),
        startingDay: 1,
        showWeeks: false
    };
    $scope.settings = { //init mobile calendar
        theme: 'android-holo-light',
        display: 'center',
        dateFormat: 'yy-mm-dd',
        mode: 'datetimeDate'
    };

    $scope.openSuccess = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'success.html',
            controller: [
                '$scope', '$uibModalInstance',
                function($scope, $uibModalInstance) {
                    $scope.ok = function() {
                        $uibModalInstance.close();
                    };
                }
            ]
        });
    };

    $scope.openNotice = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'notice.html',
            resolve: {
                errorinfo: function() {
                    return $scope.errorinfo;
                }
            },
            controller: [
                '$scope', '$uibModalInstance', 'errorinfo',
                function($scope, $uibModalInstance, errorinfo) {
                    $scope.errorinfo = errorinfo;
                    $scope.ok = function() {
                        $uibModalInstance.close();
                    };
                }
            ]
        });
    };
    $scope.change_stap = function(state) {
        $scope.candidate_stap = state;
        document.body.scrollTop = 0;
    }
    $scope.selectGender = function(sex) {
        $scope.candidate.gender = sex;
    }
    $scope.openbirthday = function() {
        $scope.popupbirthday.opened = true;
    };
    $scope.popupbirthday = {
        opened: false
    };
    $scope.opengraduationtime = function() {
        $scope.popupgraduationtime.opened = true;
    };
    $scope.popupgraduationtime = {
        opened: false
    };
    $scope.openperiodEnd = function(index) {
        $scope.popupperiodEnd[index] = { opened: true };
    };
    $scope.popupperiodEnd = [];
    $scope.popupperiodEnd[0] = {
        opened: false
    };
    $scope.openperiodStart = function(index) {
        $scope.popupperiodStart[index] = { opened: true };
    };
    $scope.popupperiodStart = [];
    $scope.popupperiodStart[0] = {
        opened: false
    };

    $scope.openworkdateEnd = function(index) {
        $scope.popupworkdateEnd[index] = { opened: true };
    };
    $scope.popupworkdateEnd = [];
    $scope.popupworkdateEnd[0] = {
        opened: false
    };
    $scope.openworkdateStart = function(index) {
        $scope.popupworkdateStart[index] = { opened: true };
    };
    $scope.popupworkdateStart = [];
    $scope.popupworkdateStart[0] = {
        opened: false
    };


    $scope.selectavailable = function() {
        switch ($scope.candidate.availableSchedule) {
            case 3:
                $scope.availableSchedule = $scope.title.availabilityOptions[0].option;
                break;
            case 4:
                $scope.availableSchedule = $scope.title.availabilityOptions[1].option;
                break;
            case 5:
                $scope.availableSchedule = $scope.title.availabilityOptions[2].option;
                break;

        }
    }

    $scope.changeLanguageLevel = function() {
        // $scope.LanguageSkillisChoose = true;
        $scope.languageLevelPreview = [];
        var flage_skill = 0;
        for (var i = 0; i <= 6; i++) {
            if ($scope.languageLevel[i] != null) {
                flage_skill = 1;
                $scope.languageLevelPreview.push($scope.languageLevel[i]);
            };
        }
        if ($scope.languageLevel[7] != '' && $scope.languageLevel[7]) {
            flage_skill = 1;
            $scope.languageLevelPreview.push($scope.languageLevel[7]);
        }
        if (flage_skill == 1) {
            $scope.LanguageSkillinvalid = false;
        } else {
            $scope.LanguageSkillinvalid = true;
        }
    }
    $scope.addEduExp = function() {
        // $scope.eduExpError = false;
        // $scope.eduExpinvalid = false;
        $scope.eduExpShow = true;
        $scope.candidate.educationExperiences.push({});
    };
    $scope.dropEduExp = function() {
        if ($scope.candidate.educationExperiences.length == 2) {
            $scope.eduExpShow = false;
        } else if ($scope.candidate.educationExperiences.length == 1) {
            $scope.eduExpShow = false;
            return;
        }
        $scope.candidate.educationExperiences.pop();
        // if($scope.candidate.educationExperiences.length == 0){
        //  $scope.eduExpinvalid = true;
        //  $scope.eduExpError = true;
        // }
    };
    $scope.addWorkExp = function() {
        $scope.workExpShow = true;
        $scope.candidate.workExperiences.push({});
    };
    $scope.dropWorkExp = function() {
        if ($scope.candidate.workExperiences.length == 2) {
            $scope.workExpShow = false;
        }
        $scope.candidate.workExperiences.pop();
    };

    $scope.changeLanguage = function(language) {
        if (language == "English")
            getQuestions("english");
        else if (language == "中文") {
            getQuestions("chinese");
        }
    }
    $scope.selectProvince = function(province, index) {
        if (province.province == "其他") {
            $scope.candidate.educationExperiences[index].selectedOther = true;
            $scope.candidate.educationExperiences[index].city = { "city": "其他" };
        } else {
            $scope.candidate.educationExperiences[index].selectedOther = false;
            $scope.candidate.educationExperiences[index].city = "";
        }
        console.log(province, index);
    }
    $scope.selectCity = function(city, index) {
        if (city && city.city == "其他") {
            // $scope.candidate.educationExperiences[index].province = {"province":"其他"};
            $scope.candidate.educationExperiences[index].selectedOther = true;
            $scope.candidate.educationExperiences[index].city = { "city": "其他" };
        } else {
            $scope.candidate.educationExperiences[index].selectedOther = false;
        }
    }

    $scope.isTouched = function() { // verify whether university has been changed
        $scope.isUniversityTouched = true;
    }

    $scope.nextStep = function(val, step) {
        if (val) {
            switch (step) {
                case 'education':
                    $scope.forms.basicinformation.submitted = true;
                    $location.hash('basicinformation');
                    $anchorScroll();
                    break;
                case 'survey':
                    $scope.forms.educationSkill.submitted = true;
                    $location.hash("education");
                    $anchorScroll();
                    break;
                case 'uploadPhoto':
                    $scope.forms.survey.submitted = true;
                    $location.hash("survey");
                    $anchorScroll();
                    break;
            }
        } else {
            switch (step) {
                case 'education':
                    $scope.basicSuccess = true;
                    $state.go(step);
                    break;
                case 'survey':
                    $scope.eduSuccess = true;
                    $state.go(step);
                    break;
                case 'uploadPhoto':
                    $scope.surSuccess = true;
                    $state.go(step);
                    break;
            }
        }
    }

    $scope.$watch(function() {
        return $scope.candidate.educationExperiences;
    }, function(newValue, oldValue) {
        if (newValue != oldValue) {
            eduCheck();
        }
    }, true)

    function eduCheck() {
        $scope.isEduNull = false;
        for (var i = 0; i < $scope.candidate.educationExperiences.length; i++) {
            if (!$scope.candidate.educationExperiences[i].periodEnd || !$scope.candidate.educationExperiences[i].periodStart || !$scope.candidate.educationExperiences[i].university || !$scope.candidate.educationExperiences[i].major || !$scope.candidate.educationExperiences[i].degree) {
                $scope.isEduNull = true;
                return;
            }
        }
    }

    $scope.submitCandidate = function() {
        console.log(config);
        console.log($scope.candidate);

        $scope.signatureDialog.show = false;
        var flage_skill = 0;
        for (var i = 0; i < $scope.languageLevel.length; i++) { //verify whether Language Skill has been selected
            if ($scope.languageLevel[i] != null) flage_skill = 1;
        }
        // if (flage_skill == 0) {
        //     $scope.errorinfo = "You need to fill out all the information with an asterisk";
        //     // $scope.notice.show = true;
        //     $scope.openNotice();
        //     return;
        // }
        $scope.loadingToast.show = true;
        var candidateInfo = angular.copy($scope.candidate);
        if ($scope.graduatePlanother) candidateInfo.graduatePlan = $scope.graduatePlan_tmp2
        for (var i = 0; i < candidateInfo.educationExperiences.length; i++) {
            candidateInfo.educationExperiences[i].period = $filter('date')(candidateInfo.educationExperiences[i].periodStart, 'yyyy-MM-dd') + ' - ' + $filter('date')(candidateInfo.educationExperiences[i].periodEnd, 'yyyy-MM-dd');
            candidateInfo.educationExperiences[i].province = candidateInfo.educationExperiences[i].province.province;
            candidateInfo.educationExperiences[i].university = candidateInfo.educationExperiences[i].city.city + ',' + candidateInfo.educationExperiences[i].university;
            candidateInfo.educationExperiences[i].major = candidateInfo.educationExperiences[i].division.division + ',' + candidateInfo.educationExperiences[i].major;
        }
        for (var i = 0; i < candidateInfo.workExperiences.length; i++) {
            if (candidateInfo.workExperiences[i].workdateStart == null || candidateInfo.workExperiences[i].workdateEnd == null) {
                candidateInfo.workExperiences[i].period = "";
            } else {
                candidateInfo.workExperiences[i].period = $filter('date')(candidateInfo.workExperiences[i].workdateStart, 'yyyy-MM-dd') + ' - ' + $filter('date')(candidateInfo.workExperiences[i].workdateEnd, 'yyyy-MM-dd');
            }
        }
        candidateInfo.birthday = $filter('date')(candidateInfo.birthday, 'yyyy-MM-dd');
        candidateInfo.graduateDate = $filter('date')(candidateInfo.graduateTime, 'yyyy-MM-dd');
        candidateInfo.languageLevel = $scope.languageLevel.join(",");
        config.params.position = candidateInfo.applicationPosition;
        $http.post('/wechat/forCandidate/sendInfo', candidateInfo, config)
            .success(function(data) {
                var questionnaire = $scope.questionnaires;
                questionnaire.answer = candidateInfo.nationalId;
                console.log(data);
                if (data == 'false') {
                    $scope.errorinfo = "You can't submit repeatedly in 30 days";
                    // $scope.notice.show = true;
                    $scope.openNotice();
                    $scope.loadingToast.show = false;
                    return;
                } else {
                    $http.post('/wechat/forCandidate/insertQuestionnaire', questionnaire)
                        .success(function(data) {
                            $scope.loadingToast.show = false;
                            // $scope.successToast.show = true;
                            $scope.openSuccess();
                            if (config.params.source == "HRAddCandidate") {
                                var timer = $timeout(
                                    function() {
                                        window.location.href = "/wechat/"
                                    }, 1500);
                            }
                            console.log("send success...")
                            uploadCandidatePhoto();
                            initData();
                        }).error(function(data) {
                            $scope.loadingToast.show = false;
                            // $scope.notice.show = true;
                            $scope.openNotice();
                            console.log("send failed...")
                        });
                }
            }).error(function(data) {
                $scope.loadingToast.show = false;
                // $scope.notice.show = true;
                $scope.openNotice();
            });
    };

    function initData(){
        $scope.candidate = {
            'applicationPosition': "Associate Software Engineer",
            // 'nationalId':'123456789098765432',
            'availableSchedule': 3,
            'graduatePlan': "Work",
            'gender': 'male'
        };
        $scope.questionnaires= {};
        getQuestions("english"); //init PC survey language
        mobileGetQuestion();
        $scope.forms = {};
        $scope.candidate.educationExperiences = [{}];
        $scope.candidate.workExperiences = [{}];
        $scope.languageLevel = [];
        $scope.LanguageSkillinvalid = true;
        $scope.LanguageSkillisChoose = false;
        $scope.isUniversityTouched = false;
        $scope.isMajorTouched = false;
        $scope.questionNull = true;
        $scope.isEduNull = true;
        $scope.basicSuccess = false;
        $scope.eduSuccess = false;
        $scope.surSuccess = false;
    }

    $scope.check_char = function(index) { // verify whether the answer of Question 9 has 200 characters
        validateQuestion();
        if (index == 9) {
            $scope.countFlag = false;
            var cnt = 0;
            var str = document.getElementById("textarea" + index).value.split(' ');
            for (var i = 0; i < str.length; i++) {
                var chinese = 0;
                var split = '';
                if (/^[a-zA-Z0-9]+$/.test(str[i])) {
                    cnt = cnt + 1;
                } else {
                    for (var j = 0; j < str[i].length; j++) {
                        if (/[\u4e00-\u9fa5]/.test(str[i][j])) {
                            chinese = chinese + 1;
                            split = 'ch';
                        } else if (/[a-zA-Z0-9]/.test(str[i][j])) {
                            if (split != 'en') {
                                cnt = cnt + 1;
                                split = 'en';
                            }
                        } else
                            split = 'sp';
                    }
                    cnt = chinese + cnt;
                }
            }
            document.getElementById("span" + index).hidden = false;
            $scope.count = cnt;
            if ((cnt > 200) || (cnt == 200))
                $scope.countFlag = true;
            else if (cnt == 0)
                document.getElementById("span" + index).hidden = true;
        }

    };

    $scope.uploadPhoto = function(candidatePhoto) {
        $scope.candidatePhotoFile = candidatePhoto;
        $scope.change_stap(5);
    };

    function validateQuestion() { // verify whether survey has been completed
        var questionIsNull = 0;
        for (var i = 0; i < $scope.questionnaires.questAndAnswerList.length; i++) {
            if ($scope.questionnaires.questAndAnswerList[i].answer == null || $scope.questionnaires.questAndAnswerList[i].answer == undefined) {
                questionIsNull = 1;
            }
        }
        if (questionIsNull == 1) {
            $scope.questionNull = true;
        } else {
            $scope.questionNull = false;
        }
        console.log($scope.questionNull);
    }

    function uploadCandidatePhoto() {
        var file = $scope.candidatePhotoFile;
        if (file == null)
            return;
        var fd = new FormData();
        fd.append('file', file);
        $http.post('/wechat/forCandidate/uploadPhoto', fd, {
                transformRequest: angular.identity,
                headers: { 'content-type': undefined },
                params: { 'nationalId': $scope.candidate.nationalId }
            })
            .success(function(data) {
                console.log("upload candidate photo " + data);
            })
            .error(function() {
                console.log("upload candidate photo " + data);
            });
    };


    function getQuestions(Language) {
        $http.post('/wechat/forCandidate/getQuestions?language=' + Language)
            .success(function(data, status, headers, config) {
                $scope.questionnaires = data;
                // validateQuestion();
                console.log(data)
            }).error(function(data, status, headers, config) {
                console.log("get questions failed...");
            });
    }

    function mobileGetQuestion() {
        $http.get('/wechat/forCandidate/getQuestions?language=chinese')
            .success(function(data, status, headers, config) {
                $scope.question.chinese = data.questAndAnswerList;
                console.log($scope.question);
            }).error(function(data, status, headers, config) {
                console.log("get questions failed...");
            });
        $http.get('/wechat/forCandidate/getQuestions?language=english')
            .success(function(data, status, headers, config) {
                $scope.question.english = data.questAndAnswerList;
                console.log($scope.questionnaires)
            }).error(function(data, status, headers, config) {
                console.log("get questions failed...");
            });
    }
});
