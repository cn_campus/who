app
    .controller(
        'offerController',
        function($scope, $http, $filter, $rootScope) {
            $scope.showSelection = false;
            $scope.showSubnav = true;
            $scope.processName = "Talent Pool";
            $scope.selectUniversity = true;
            $scope.talentEmail = {};
            $scope.name = name.split("@", 1);
            $scope.page = {
                allcheck: false,
                maxSize: 5, // after 5 pages the pagination will be
                // ellipsis
                bigCurrentPage: 1
                    // current page
            };

            $scope.tinymceOptions = {
                inline: false,
                statusbar: false,
                menubar: true,
                height: 200,
                // plugins: [
                // 'advlist lists charmap anchor',
                // 'searchreplace visualblocks',
                // 'insertdatetime table paste code'
                // ],
                toolbar: ['undo redo | bold italic underline | bullist numlist outdent indent']
            };

            /*
             * 整个流程图如下 Status Process: {status:STATUS_NEW,
             * result:RESULT_UNKNOWN} {status:STATUS_Q1,
             * result:RESULT_UNKNOWN} {status:STATUS_Q2,
             * result:RESULT_UNKNOWN} {status:STATUS_Q3,
             * result:RESULT_UNKNOWN} {status:STATUS_Q4,
             * result:RESULT_PASS} {status:STATUS_Q5,
             * result:RESULT_PASS} Status Trash: { result:RESULT_FAIL}
             * 
             */
            $scope.status = {
                list: [{
                    name: 'STATUS_ALL',
                    clazz: 'on',
                    iclazz: 'fa fa-angle-right fa-fw',
                    title: 'Talent Pool'
                }, {
                    name: 'STATUS_NEW',
                    clazz: '',
                    iclazz: 'fa fa-angle-right fa-fw',
                    title: 'Q0-Campus Talk'
                }, {
                    name: 'STATUS_Q1',
                    clazz: '',
                    iclazz: 'fa fa-angle-right fa-fw',
                    title: 'Q1-Paper Test'
                }, {
                    name: 'STATUS_Q2',
                    clazz: '',
                    iclazz: 'fa fa-angle-right fa-fw',
                    title: 'Q2-Group Interview'
                }, {
                    name: 'STATUS_Q3',
                    clazz: '',
                    iclazz: 'fa fa-angle-right fa-fw',
                    title: 'Q3-Final Interview'
                }, {
                    name: 'STATUS_Q4',
                    clazz: '',
                    iclazz: 'fa fa-angle-right fa-fw',
                    title: 'Q4-Offer'
                }, {
                    name: 'STATUS_Q5',
                    clazz: '',
                    iclazz: 'fa fa-angle-right fa-fw',
                    title: 'Q5-Intern Orientation'
                }, {
                    name: 'STATUS_TRASH',
                    clazz: '',
                    iclazz: 'fa fa-trash fa-fw',
                    title: 'Trash'
                }]
            };
            // All Conditions
            $scope.condition = {
                id: 'scope.condition',
                title: 'STATUS_ALL', // Candidate status
                otherCondition: {
                    university: [null, null], // null is essential
                    // here
                    major: [null, null],
                    candidateName: '',
                    round1: [null, null], // null is essential here
                    round2: [null, null],
                    round3: [null, null],
                    graduateDate: [null, null],
                    talentPage: {
                        dataIndex: 0,
                        dataTotal: 194,
                        pageIndex: 1,
                        pageLines: 20,
                        pageTotal: 10
                    }
                }
            }

            $scope.university_pro = [{
                cities: '上海',
                universities: ['上海中医药大学', '上海交通大学', '上海商学院',
                    '上海大学', '上海对外经贸大学', '上海工程技术大学',
                    '上海师范大学', '上海应用技术大学', '上海政法学院',
                    '上海海事大学', '上海海关学院', '上海海洋大学', '上海理工大学',
                    '上海电力学院', '上海电机学院', '上海立信会计学院',
                    '上海第二工业大学', '上海财经大学', '上海金融学院', '东华大学',
                    '华东师范大学', '华东政法大学', '华东理工大学', '同济大学',
                    '复旦大学', '上海纽约大学', '上海外国语大学'
                ]
            }, {
                cities: '大连',
                universities: ['东北财经大学', '大连海事大学', '大连理工大学',
                    '大连外国语大学', '大连交通大学'
                ]
            }, {
                cities: '南京',
                universities: ['东南大学', '中国药科大学', '南京中医院大学',
                    '南京信息工程大学', '南京农业大学', '南京医科大学', '南京大学',
                    '南京审计学院', '南京工业大学', '南京师范大学', '南京林业大学',
                    '南京航空航天大学', '南京财经大学', '南京邮电大学', '河海大学'
                ]
            }, {
                cities: '苏州',
                universities: ['苏州大学', '西交利物浦大学']
            }, {
                cities: '杭州',
                universities: ['中国美术学院', '中国计量学院', '杭州师范大学',
                    '杭州电子科技大学', '浙江大学', '浙江工业大学', '浙江工商大学',
                    '浙江理工大学'
                ]
            }, {
                cities: '武汉',
                universities: ['中南民族大学', '中南财经政法大学',
                    '中国地质大学(武汉)', '华中农业大学', '华中师范大学',
                    '华中科技大学', '武汉大学', '武汉工程大学', '武汉理工大学',
                    '武汉科技大学', '湖北中医药大学', '湖北大学', '湖北工业大学'
                ]
            }, {
                cities: '宁波',
                universities: ['宁波大学', '宁波诺丁汉大学']
            }, {
                cities: '其他',
                universities: []
            }];

            $scope.major = [{
                division: '哲学',
                sections: ['哲学类']
            }, {
                division: '经济学',
                sections: ['经济学类']
            }, {
                division: '管理学',
                sections: ['管理科学与工程类', '工商管理类', '行政管理、公共管理类',
                    '图书档案学类'
                ]
            }, {
                division: '文学',
                sections: ['语言文学类', '新闻传播学类', '艺术类']
            }, {
                division: '工学',
                sections: ['电气信息类', '计算机科学与技术类', '机械类',
                    '土建类', '生物医学工程类', '仪器仪表类', '能源动力类',
                    '水利类', '材料类', '制药工程类', '交通运输类',
                    '船舶与海洋工程类', '测绘类', '轻工纺织食品类', '武器类',
                    '公安技术类', '航空航天类'
                ]
            }, {
                division: '历史学',
                sections: ['历史学类']
            }, {
                division: '法学',
                sections: ['法学类']
            }, {
                division: '理学',
                sections: ['数学类', '物理学类', '化学类及化学工程类',
                    '生物科学及生物技术类', '天文地质地理类', '力学类',
                    '电子信息科学类', '系统科学类', '环境科学与安全类'
                ]
            }, {
                division: '教育学',
                sections: ['教育学类']
            }, {
                division: '医学',
                sections: ['医学类', '心理学类']
            }, {
                division: '农学',
                sections: ['农业类']
            }];

            // mutiCondition
            $scope.condition.selections = [{
                id: 'selectedPosition',
                title: 'Selected position:',
                showinfo: true,
                selections: [{
                    id: 1,
                    title: 'Associate Software Engineer',
                    check: null
                }, {
                    id: 2,
                    title: 'Associate Testing Engineer',
                    check: null
                }, {
                    id: 3,
                    title: 'Associate Analyst',
                    check: null
                }, {
                    id: 4,
                    title: 'Associate UI Designer',
                    check: null
                }, {
                    id: 5,
                    title: 'PQA Intern',
                    check: null
                }, {
                    id: 6,
                    title: 'Admin Intern',
                    check: null
                }, {
                    id: 7,
                    title: 'HR Intern',
                    check: null
                }, {
                    id: 8,
                    title: 'Finance Intern',
                    check: null
                }]
            }, {
                id: 'availableDay',
                title: 'Availability:',
                showinfo: true,
                selections: [{
                    id: '3',
                    title: 'Three days',
                    check: null
                }, {
                    id: '4',
                    title: 'Four days',
                    check: null
                }, {
                    id: '5',
                    title: 'Five days',
                    check: null
                }]
            }, {
                id: 'graduatePlan',
                title: 'Graduate Plan:',
                showinfo: false,
                selections: [{
                    id: 31,
                    title: 'Work',
                    check: null
                }, {
                    id: 12,
                    title: 'Go abroad',
                    check: null
                }, {
                    id: 13,
                    title: 'Civil servant',
                    check: null
                }, {
                    id: 14,
                    title: 'Graduate Studies',
                    check: null
                }, {
                    id: 32,
                    title: 'Other',
                    check: null
                }]
            }, {
                id: 'languageProficiency',
                title: 'Language proficiency:',
                showinfo: false,
                selections: [{
                    id: 15,
                    title: 'CET-4',
                    check: null
                }, {
                    id: 16,
                    title: 'CET-6',
                    check: null
                }, {
                    id: 17,
                    title: 'TEM-4',
                    check: null
                }, {
                    id: 18,
                    title: 'TEM-6',
                    check: null
                }, {
                    id: 19,
                    title: 'TEM-8',
                    check: null
                }, {
                    id: 20,
                    title: 'JPT-1',
                    check: null
                }, {
                    id: 21,
                    title: 'JPT-2',
                    check: null
                }, {
                    id: 22,
                    title: 'JPT-3',
                    check: null
                }, {
                    id: 23,
                    title: 'Other',
                    check: null
                }]
            }, {
                id: 'gender',
                title: 'Gender:',
                showinfo: false,
                selections: [{
                    id: 26,
                    title: 'Male',
                    check: null
                }, {
                    id: 27,
                    title: 'Female',
                    check: null
                }]
            }, {
                id: 'degree',
                title: 'Degree:',
                showinfo: false,
                selections: [{
                    id: 28,
                    title: 'Bachelor',
                    check: null
                }, {
                    id: 29,
                    title: 'Master',
                    check: null
                }, {
                    id: 30,
                    title: 'Doctor',
                    check: null
                }]
            }];

            getTalent();

            // availability
            $scope.availabilityOptions = [{
                value: "3",
                option: "Three days"
            }, {
                value: "4",
                option: "Four days"
            }, {
                value: "5",
                option: "Five days"
            }];

            // status
            $scope.updatestatus = [{
                name: 'STATUS_ALL',
                title: 'Talent Pool'
            }, {
                name: 'STATUS_NEW',
                title: 'Q0-Campus Talk'
            }, {
                name: 'STATUS_Q1',
                title: 'Q1-Paper Test'
            }, {
                name: 'STATUS_Q2',
                title: 'Q2-Group Interview'
            }, {
                name: 'STATUS_Q3',
                title: 'Q3-Final Interview'
            }, {
                name: 'STATUS_Q4',
                title: 'Q4-Offer'
            }, {
                name: 'STATUS_Q5',
                title: 'Q5-Intern Orientation'
            }, ];
            // position
            $scope.positionOptions = ["Associate Software Engineer",
                "Associate Testing Engineer", "Associate Analyst",
                "Associate UI Designer", "PQA Intern",
                "Admin Intern", "HR Intern", "Finance Intern"
            ];
            // graduationplan
            $scope.graduationplanOptions = ["Work", "Go abroad",
                "Civil servant", "Graduate studies", "Other"
            ];

            // gender
            $scope.chooseGenders = ["male", "female"];

            // languageLevel
            $scope.chooseLanguageLevel = function() {
                $scope.languageLevelPreview = [];
                var flage_skill = 0;
                for (var i = 0; i <= 6; i++) {
                    if ($scope.languageLevel[i] != null) {
                        flage_skill = 1;
                        $scope.languageLevelPreview
                            .push($scope.languageLevel[i]);
                    };
                }
                if ($scope.languageLevel[7] != '' && $scope.languageLevel[7]) {
                    flage_skill = 1;
                    $scope.languageLevelPreview
                        .push($scope.languageLevel[7]);
                }
                if (flage_skill == 1) {
                    $scope.LanguageSkillinvalid = false;
                } else {
                    $scope.LanguageSkillinvalid = true;
                }
            }

            // degree
            $scope.degrees = ["Bachelor", "Master", "Doctor"];

            $scope.updateState = function(name, title) {
                $scope.condition.title = name;
                if (title == 'Trash' || title == 'Talent Pool') {
                    $scope.processName = title;
                } else {
                    $scope.processName = title.substring(3);
                }
                getTalent();
            }

            $scope.changeCandidateName = function(candidateName) {
                $scope.condition.otherCondition.candidateName = candidateName;
                getTalent();
            }

            $scope.changeGraduateDate = function(start, end) {
                start = $filter('date')(start, 'yyyy-MM-dd');
                end = $filter('date')(end, 'yyyy-MM-dd');
                console.log(start, end);
                $scope.condition.otherCondition.graduateDate = [start,
                    end
                ];
                getTalent();
            }

            $scope.changeScore1 = function(start, end) {
                $scope.condition.otherCondition.round1 = [start, end];
                getTalent();
            }
            $scope.changeScore2 = function(start, end) {
                $scope.condition.otherCondition.round2 = [start, end];
                getTalent();
            }
            $scope.changeScore3 = function(start, end) {
                $scope.condition.otherCondition.round3 = [start, end];
                getTalent();
            }

            // HRMs modify candidate's photo and upload it to database
            function uploadCandidatePhoto(candidateinfo, candidatePhoto) {
                var file = candidatePhoto;
                if (file == null)
                    return;
                var fd = new FormData();
                fd.append('file', file);
                $http.post('forHrm/uploadPhoto', fd, {
                        transformRequest: angular.identity,
                        headers: { 'content-type': undefined },
                        params: { 'nationalId': candidateinfo.candidate.nationalId }
                    })
                    .success(function(data) {
                        console.log("upload candidate photo " + data);
                        $scope.initCandidateInfoImg(candidateinfo.candidate);
                    })
                    .error(function(data) {
                        console.log("upload candidate photo " + data);
                    });
            };

            // edit each talent
            $scope.edit = function(talent) {
                $http.post('forHr/getCandidateAllInfo', talent)
                    .success(function(redata) {
                        // getTalent();
                    });
                console.log(talent);
                talent.isEdit = !talent.isEdit;
                if (talent.isEdit == true) {
                    talent.oldQ1 = talent.q1;
                    talent.oldQ2 = talent.q2;
                    talent.oldQ3 = talent.q3;
                } else {
                    talent.oldQ1 = null;
                    talent.oldQ2 = null;
                    talent.oldQ3 = null;
                }
                console.log($scope.talentDataSet);
            }

            $scope.editInfo = function(candidateinfo, eduExp) {

                $scope.editable = !$scope.editable;
                if ($scope.editable == true) {
                    $('.dropify-clear').trigger('click');
                    candidateinfo.candidate.oldName = candidateinfo.candidate.name;
                    candidateinfo.candidate.oldCellPhone = candidateinfo.candidate.cellPhone;
                    candidateinfo.candidate.oldMailAddress = candidateinfo.candidate.mailAddress;
                    candidateinfo.application.oldRound1Score = candidateinfo.application.round1Score;
                    candidateinfo.application.oldRound2Score = candidateinfo.application.round2Score;
                    candidateinfo.application.oldRound3Score = candidateinfo.application.round3Score;
                    candidateinfo.application.oldStatus = candidateinfo.application.status;
                    candidateinfo.candidate.oldAvailableSchedule = candidateinfo.candidate.availableSchedule;
                    // candidateinfo.application.oldApplyDate =
                    // candidateinfo.application.applyDate;
                    candidateinfo.application.oldApplyPosition = candidateinfo.application.applyPosition;
                    candidateinfo.candidate.oldGraduateDate = candidateinfo.candidate.graduateDate;
                    candidateinfo.candidate.oldGraduatePlan = candidateinfo.candidate.graduatePlan;
                    candidateinfo.candidate.oldGender = candidateinfo.candidate.gender;
                    // candidateinfo.candidate.oldNationalId =
                    // candidateinfo.candidate.nationalId;
                    // candidateinfo.candidate.oldPassportNumber =
                    // candidateinfo.candidate.passportNumber;
                    candidateinfo.candidate.oldBirthday = candidateinfo.candidate.birthday;
                    candidateinfo.candidate.oldBirthProvince = candidateinfo.candidate.birthProvince;
                    candidateinfo.candidate.oldHomePhone = candidateinfo.candidate.homePhone;
                    candidateinfo.candidate.oldNationality = candidateinfo.candidate.nationality;
                    candidateinfo.candidate.oldPersonalPage = candidateinfo.candidate.personalPage;
                    // candidateinfo.candidate.oldLanguageLevel =
                    // candidateinfo.candidate.languageLevel;
                    for (var i = 0; i < $scope.candidateinfo.candidate.educationExperiences.length; i++) {
                        $scope.candidateinfo.candidate.educationExperiences[i].oldDegree = $scope.candidateinfo.candidate.educationExperiences[i].degree;
                        $scope.candidateinfo.candidate.educationExperiences[i].oldUniversity = $scope.candidateinfo.candidate.educationExperiences[i].university;
                        $scope.candidateinfo.candidate.educationExperiences[i].oldMajor = $scope.candidateinfo.candidate.educationExperiences[i].major;
                        $scope.candidateinfo.candidate.educationExperiences[i].oldPeriod = $scope.candidateinfo.candidate.educationExperiences[i].period;
                        $scope.candidateinfo.candidate.educationExperiences[i].oldGpa = $scope.candidateinfo.candidate.educationExperiences[i].gpa;
                    }
                    for (var i = 0; i < $scope.candidateinfo.candidate.workExperiences.length; i++) {
                        $scope.candidateinfo.candidate.workExperiences[i].oldCompany = $scope.candidateinfo.candidate.workExperiences[i].company;
                        $scope.candidateinfo.candidate.workExperiences[i].oldPosition = $scope.candidateinfo.candidate.workExperiences[i].position;
                        $scope.candidateinfo.candidate.workExperiences[i].oldDetails = $scope.candidateinfo.candidate.workExperiences[i].details;
                    }
                    for (var i = 0; i < $scope.candidateinfo.questAndAnswer.questAndAnswerList.length; i++) {
                        $scope.candidateinfo.questAndAnswer.questAndAnswerList[i].oldAnswer = candidateinfo.questAndAnswer.questAndAnswerList[i].answer;
                    }

                } else {
                    candidateinfo.candidate.oldName = null;
                    candidateinfo.candidate.oldCellPhone = null;
                    candidateinfo.candidate.oldMailAddress = null;
                    candidateinfo.application.oldRound1Score = null;
                    candidateinfo.application.oldRound2Score = null;
                    candidateinfo.application.oldRound3Score = null;
                    candidateinfo.application.oldStatus = null;
                    candidateinfo.candidate.oldAvailableSchedule = null;
                    // candidateinfo.application.oldApplyDate = null;
                    candidateinfo.application.oldApplyPosition = null;
                    candidateinfo.candidate.oldGraduateDate = null;
                    candidateinfo.candidate.oldGraduatePlan = null;
                    candidateinfo.candidate.oldGender = null;
                    // candidateinfo.candidate.oldNationalId = null;
                    // candidateinfo.candidate.oldPassportNumber = null;
                    candidateinfo.candidate.oldBirthday = null;
                    candidateinfo.candidate.oldBirthProvince = null;
                    candidateinfo.candidate.oldHomePhone = null;
                    candidateinfo.candidate.oldNationality = null;
                    candidateinfo.candidate.oldPersonalPage = null;
                    // candidateinfo.candidate.oldLanguageLevel = null;

                }
            }

            $scope.updateInfo = function(candidateinfo, candidatePhoto) {
                if ($scope.editable == true) {
                    $scope.editable = false;
                    candidateinfo.candidate.name = candidateinfo.candidate.oldName;
                    candidateinfo.candidate.cellPhone = candidateinfo.candidate.oldCellPhone;
                    candidateinfo.candidate.mailAddress = candidateinfo.candidate.oldMailAddress;
                    candidateinfo.application.round1Score = candidateinfo.application.oldRound1Score;
                    candidateinfo.application.round2Score = candidateinfo.application.oldRound2Score;
                    candidateinfo.application.round3Score = candidateinfo.application.oldRound3Score;
                    candidateinfo.application.status = candidateinfo.application.oldStatus;
                    candidateinfo.candidate.availableSchedule = candidateinfo.candidate.oldAvailableSchedule;
                    // candidateinfo.application.applyDate =
                    // candidateinfo.application.oldApplyDate;
                    candidateinfo.application.applyPosition = candidateinfo.application.oldApplyPosition;
                    candidateinfo.candidate.graduateDate = candidateinfo.candidate.oldGraduateDate;
                    candidateinfo.candidate.graduatePlan = candidateinfo.candidate.oldGraduatePlan;
                    candidateinfo.candidate.gender = candidateinfo.candidate.oldGender;
                    // candidateinfo.candidate.nationalId =
                    // candidateinfo.candidate.oldNationalId;
                    // candidateinfo.candidate.passportNumber =
                    // candidateinfo.candidate.oldPassportNumber;
                    candidateinfo.candidate.birthday = candidateinfo.candidate.oldBirthday;
                    candidateinfo.candidate.birthProvince = candidateinfo.candidate.oldBirthProvince;
                    candidateinfo.candidate.homePhone = candidateinfo.candidate.oldHomePhone;
                    candidateinfo.candidate.nationality = candidateinfo.candidate.oldNationality;
                    candidateinfo.candidate.personalPage = candidateinfo.candidate.oldPersonalPage;
                    // candidateinfo.candidate.languageLevel = null;
                    for (var i = 0; i < $scope.candidateinfo.candidate.educationExperiences.length; i++) {
                        $scope.candidateinfo.candidate.educationExperiences[i].degree = $scope.candidateinfo.candidate.educationExperiences[i].oldDegree;
                        $scope.candidateinfo.candidate.educationExperiences[i].university = $scope.candidateinfo.candidate.educationExperiences[i].oldUniversity;
                        $scope.candidateinfo.candidate.educationExperiences[i].major = $scope.candidateinfo.candidate.educationExperiences[i].oldMajor;
                        $scope.candidateinfo.candidate.educationExperiences[i].period = $scope.candidateinfo.candidate.educationExperiences[i].oldPeriod;
                        $scope.candidateinfo.candidate.educationExperiences[i].gpa = $scope.candidateinfo.candidate.educationExperiences[i].oldGpa;
                    }
                    for (var i = 0; i < $scope.candidateinfo.candidate.workExperiences.length; i++) {
                        $scope.candidateinfo.candidate.workExperiences[i].company = $scope.candidateinfo.candidate.workExperiences[i].oldCompany;
                        $scope.candidateinfo.candidate.workExperiences[i].position = $scope.candidateinfo.candidate.workExperiences[i].oldPosition;
                        $scope.candidateinfo.candidate.workExperiences[i].details = $scope.candidateinfo.candidate.workExperiences[i].oldDetails;
                    }
                    for (var i = 0; i < $scope.candidateinfo.questAndAnswer.questAndAnswerList.length; i++) {
                        // Modify the repeated numbers on state 4 page by Jay Yuan 7/25/2016 --Start
                        if (i == 8) {
                            var fullQuestion = new String($scope.candidateinfo.questAndAnswer.questAndAnswerList[i].question);
                            console.log("fullQuestion-->" + fullQuestion);
                            var tempQuestion = fullQuestion.split(".");
                            var num = tempQuestion.length;
                            $scope.candidateinfo.questAndAnswer.questAndAnswerList[i].question = tempQuestion[num - 2] + "." + tempQuestion[num - 1];
                        } else {
                            var tempQuestion = $scope.candidateinfo.questAndAnswer.questAndAnswerList[i].question;
                            tempQuestion = tempQuestion.substring(tempQuestion.lastIndexOf(".") + 1, tempQuestion.length);
                            $scope.candidateinfo.questAndAnswer.questAndAnswerList[i].question = tempQuestion;
                        }
                        // Modify the repeated numbers on show state 4 page by Jay Yuan 7/25/2016 --End
                        $scope.candidateinfo.questAndAnswer.questAndAnswerList[i].answer = candidateinfo.questAndAnswer.questAndAnswerList[i].oldAnswer;
                    }

                }

                uploadCandidatePhoto(candidateinfo, candidatePhoto);
                // refresh photo to show the new photo on page
                // $scope.initCandidateInfoImg(candidateinfo.candidate);

                candidateinfo.candidate.languageLevel = $scope.languageLevel
                    .join(",");
                console.log("language--->" + candidateinfo.candidate.languageLevel);
                $http.post('forCandidate/updateInfo', candidateinfo)
                    .success(function(redata) {
                        // getTalent();
                    });
            }

            // select mutiCondition
            $scope.updateSelection = function(selection) {
                    selection.check = !selection.check;
                    getTalent();
                }
                // show subnav on left side
            $scope.toggle = function() {
                $scope.showSubnav = !$scope.showSubnav;
            }

            $scope.isCheck = function(talent) {
                var num = 0;
                for (var i = 0; i < $scope.talentDataSet.talentDatas.length; i++) {
                    if ($scope.talentDataSet.talentDatas[i].check == true) {
                        num++;
                    }
                }
                if (num == $scope.talentDataSet.talentDatas.length) {
                    $scope.page.allcheck = true;

                } else {
                    $scope.page.allcheck = false;

                }
            }

            $scope.selectAll = function() {
                for (var i = 0; i < $scope.talentDataSet.talentDatas.length; i++) {
                    $scope.talentDataSet.talentDatas[i].check = $scope.page.allcheck == true ? true : false;
                }
            }

            $scope.editAll = function() {
                for (var i = 0; i < $scope.talentDataSet.talentDatas.length; i++) {
                    $scope.talentDataSet.talentDatas[i].isEdit = true;
                    $scope.talentDataSet.talentDatas[i].oldQ1 = $scope.talentDataSet.talentDatas[i].q1;
                    $scope.talentDataSet.talentDatas[i].oldQ2 = $scope.talentDataSet.talentDatas[i].q2;
                    $scope.talentDataSet.talentDatas[i].oldQ3 = $scope.talentDataSet.talentDatas[i].q3;
                }
            }

            $scope.deleteSelected = function() {

                var talentDatas = $scope.talentDataSet.talentDatas;
                var selected = false;
                for (var i in talentDatas) {
                    if (talentDatas[i].check == true) {
                        selected = true;
                    }
                }
                if (selected) {
                    $http.post('forHr/batchTotrash', $scope.talentDataSet)
                        .success(function(redata) {
                            // alert(redata);
                            getTalent();
                        });

                } else {
                    change_dialog_error('At least one record is needed');
                    show_dialog_error();
                }




            }

            $scope.saveemail = function(talentEmail) {
                    var emailconfig = {};
                    console.log(talentEmail);
                    emailconfig.emailContextTemplate = talentEmail.context;
                    emailconfig.emailSubjectTemplate = talentEmail.subject;
                    emailconfig.emailStatusTemplate = $scope.condition.title;

                    $http.post('forHr/updateEmailTemplate', emailconfig)
                        .success(function(redata) {
                            if (redata == "success") {
                                change_dialog_error('Save success');
                                show_dialog_error();
                            }
                        }).error(function() {
                            change_dialog_error('Save failed');
                            show_dialog_error();
                        });
                }
                //			
            $scope.InitEmail = function() {
                var talentDatas = $scope.talentDataSet.talentDatas;
                var selected = false;
                for (var i in talentDatas) {
                    if (talentDatas[i].check == true) {
                        selected = true;
                    }
                }
                if (selected) {
                    $scope.config = {};
                    console.log($scope.condition);
                    var emailStatusTemplate = $scope.condition.title;
                    $http
                        .post('forHr/getEmailTemplate',
                            emailStatusTemplate)
                        .success(
                            function(redata) {
                                $scope.talentEmail.subject = redata.emailSubjectTemplate;
                                $scope.talentEmail.context = redata.emailContextTemplate;
                            });
                    $('#exampleModal').modal('show');
                } else {
                    change_dialog_error('At least one record is needed');
                    show_dialog_error();
                }

            }

            $scope.sendmodelemail = function(emailSubject) {

                // $scope.talentEmail.context =
                // $("#trix-input-1").val();

                $scope.talentEmail.talentDatas = $scope.talentDataSet.talentDatas;
                // $scope.talentEmail.subject = emailSubject;
                // console.log($scope.test);
                // $scope.attachments = {};
                $scope.talentEmail.attachment = new FormData(
                    $("#uploadattachment")[0]);
                console.log($scope.talentEmail.attachment);
                // alert($("#uploadattachment"));
                console.log($scope.talentEmail);
                show_dialog_loading();

                $http({
                        method: 'POST',
                        url: 'forHr/sendattachment',
                        // file:$scope.talentEmail.attachment,

                        data: $scope.talentEmail.attachment,
                        headers: {
                            'Content-Type': undefined
                        }
                    })
                    .success(
                        function(redata) {
                            $scope.talentEmail.attachment = redata;
                            $http
                                .post('forHr/sendemail',
                                    $scope.talentEmail)
                                .success(
                                    function(redata) {
                                        if (redata == "success") {
                                            $http
                                                .post(
                                                    'forHr/addsendEmailflag',
                                                    $scope.talentDataSet)
                                                .success(
                                                    function(
                                                        redata) {
                                                        getTalent();
                                                    });
                                            hide_dialog_loading();
                                            change_dialog_error('send success');
                                            // show_dialog_error();
                                            show_dialog_error();
                                        } else {
                                            hide_dialog_loading();
                                            change_dialog_error('send error');
                                            show_dialog_error();
                                        }

                                    })
                                .error(
                                    function() {
                                        hide_dialog_loading();
                                        change_dialog_error('send error');
                                        show_dialog_error();
                                    });
                            console.log(redata);
                        });
                // $('#exampleModal').modal('hide');
            }
            $scope.MarkedasRead = function() {
                var talentDatas = $scope.talentDataSet.talentDatas;
                var selected = false;
                for (var i in talentDatas) {
                    if (talentDatas[i].check == true) {
                        selected = true;
                    }
                }
                if (selected) {
                    $http.post('forHr/delmoveflag',
                        $scope.talentDataSet).success(
                        function(redata) {
                            getTalent();
                        });

                } else {
                    change_dialog_error('At least one record is needed');
                    show_dialog_error();
                }
            }
            $scope.MarkedasUnRead = function() {
                    var talentDatas = $scope.talentDataSet.talentDatas;
                    var selected = false;
                    for (var i in talentDatas) {
                        if (talentDatas[i].check == true) {
                            selected = true;
                        }
                    }
                    if (selected) {
                        $http.post('forHr/addroundmoveflag',
                            $scope.talentDataSet).success(
                            function(redata) {
                                getTalent();
                            });

                    } else {
                        change_dialog_error('At least one record is needed');
                        show_dialog_error();
                    }
                }
                //					by dww right menu

            $scope.selected = 'None';
            $scope.menuOptions = [

                ['Save', function() {
                    $scope.saveData();
                }], null, ['Round Move', function() {
                    $scope.roundMove();
                }],
                null, // Dividier

                ['Marked as', [
                    ['Read', function($itemScope) {
                        $scope.MarkedasRead();
                    }],
                    null, // Dividier
                    ['Unread', function($itemScope) {
                        $scope.MarkedasUnRead();

                    }],
                ]],
                null, // Dividier
                ['Move', [

                    ['Back', function() {
                        $scope.moveBack();
                    }],
                    null, // Dividier
                    ['Trash', function() {
                        $scope.deleteSelected();
                    }],
                ]],
                null, // Dividier
                ['Export', [
                    ['Resume', function() {
                        $scope.exportCandidateZip();
                    }],
                    null, // Dividier
                    ['Survey', function() {
                        $scope.exportQuestionnaireZip();
                    }],
                    null, // Dividier
                    ['Excel', function() {
                        $scope.exportExcel();
                    }],
                    null, // Dividier
                    ['Formal Resume', function() {
                        $scope.exportPrettyCandidateZip();
                    }]
                ]]



            ];

            //					by dww right menu

            $scope.pageChanged = function() {
                $scope.condition.otherCondition.talentPage.pageIndex = angular
                    .copy($scope.page.bigCurrentPage);
                $http.post('forHr/multiSearch', $scope.condition)
                    .success(function(redata) {
                        getTalent();
                    });
            };

            // shuffle
            $scope.shuffleData = function() {
                    $scope.talentDataSet.talentDatas = shuffle($scope.talentDataSet.talentDatas);
                }
                // export
                //					$scope.exportCandidateZip = function() {
                //						exportTo("/wechat/exportCandidateZip?nationalIds=");
                //					}
            $scope.exportPrettyCandidateZip = function() {
                exportTo("/wechat/exportPrettyCandidateZip?nationalIds=");
            }
            $scope.exportExcel = function() {
                exportTo("/wechat/exportExcel?nationalIds=");
            }
            $scope.exportQuestionnaireZip = function() {
                    exportTo("/wechat/exportQuestionnaireZip?nationalIds=");
                }
                // save
            $scope.saveData = function() {
                    for (var i = 0; i < $scope.talentDataSet.talentDatas.length; i++) {
                        if ($scope.talentDataSet.talentDatas[i].isEdit == true) {
                            $scope.talentDataSet.talentDatas[i].isEdit = false;
                            $scope.talentDataSet.talentDatas[i].q1 = $scope.talentDataSet.talentDatas[i].oldQ1;
                            $scope.talentDataSet.talentDatas[i].q2 = $scope.talentDataSet.talentDatas[i].oldQ2;
                            $scope.talentDataSet.talentDatas[i].q3 = $scope.talentDataSet.talentDatas[i].oldQ3;
                        }
                    }
                    console.log($scope.talentDataSet);
                    $http
                        .post('forHr/saveTalentData',
                            $scope.talentDataSet).success(
                            function(redata) {
                                // alert(redata);
                                getTalent();
                            });
                }
                // roundMove
            $scope.roundMove = function() {

                var talentDatas = $scope.talentDataSet.talentDatas;
                var selected = false;
                for (var i in talentDatas) {
                    if (talentDatas[i].check == true) {
                        selected = true;
                    }
                }
                if (selected) {
                    $http.post('forHr/roundMoveTalentData',
                        $scope.talentDataSet).success(function(redata) {
                        console.log(redata);
                        getTalent();
                    });

                } else {
                    change_dialog_error('At least one record is needed');
                    show_dialog_error();
                }




            }
            $scope.moveBack = function() {
                    var talentDatas = $scope.talentDataSet.talentDatas;
                    var selected = false;
                    for (var i in talentDatas) {
                        if (talentDatas[i].check == true) {
                            selected = true;
                        }
                    }
                    if (selected) {
                        $http.post('forHr/moveBackTalentData',
                            $scope.talentDataSet).success(function(redata) {
                            // alert(redata);
                            getTalent();

                        });

                    } else {
                        change_dialog_error('At least one record is needed');
                        show_dialog_error();
                    }




                }
                // delete
            $scope.deleteData = function(talent) {
                $http.post('forHr/toTrash', talent).success(
                    function(redata) {
                        // alert(redata);
                        getTalent();
                    });
            }

            // recover
            $scope.recoverData = function(talent) {
                $http.post('forHr/recoverFromTrash', talent).success(
                    function(redata) {
                        // alert(redata);
                        getTalent();
                    })
            }

            $scope.submitHrComment = function() {
                getStarLevel();
                $scope.hrComment = angular
                    .copy($scope.historyHrComment);
                $scope.hrComment.earliestEntry = $filter('date')(
                    $scope.hrComment.earliestEntry, 'yyyy-MM-dd');
                console.log($scope.hrComment);
                $http.post('forHr/saveHrComment', $scope.hrComment)
                    .success(function(data) {
                        // alert("success");
                        $('#myModal').modal('hide');
                    })
                console.log($scope.hrComment);
            }

            $scope.cancelComment = function() {
                $scope.historyHrComment = {};
            }
            $scope.changeCity = function() {
                if ($scope.university_pro.cities == null) {
                    $scope.selectUniversity = true;
                    $scope.condition.otherCondition.university = [
                        null, null
                    ];
                    getTalent();
                } else if ($scope.university_pro.cities.cities == '其他') {
                    $scope.selectUniversity = false;
                    $scope.condition.otherCondition.university = [
                        $scope.university_pro.cities.cities, null
                    ];
                } else {
                    $scope.selectUniversity = true;
                    $scope.condition.otherCondition.university = [
                        $scope.university_pro.cities.cities, null
                    ];
                }
                getTalent();
            }
            $scope.searchUniversity = function() {
                console
                    .log($scope.condition.otherCondition.universitytmp);
                if ($scope.condition.otherCondition.universitytmp == "") {
                    $scope.condition.otherCondition.university = [
                        $scope.university_pro.cities.cities, null
                    ];
                } else {
                    $scope.condition.otherCondition.university = [
                        $scope.university_pro.cities.cities,
                        $scope.condition.otherCondition.universitytmp
                    ];
                }
                getTalent();
            }
            $scope.changeClass = function() {
                if ($scope.major.division == null) {
                    $scope.condition.otherCondition.major = [null,
                        null
                    ];
                } else {
                    $scope.condition.otherCondition.major = [
                        $scope.major.division.division, null
                    ];
                }
                getTalent();
            }
            $scope.changeUniversity = function() {
                if ($scope.condition.otherCondition.universitytmp == null) {
                    $scope.condition.otherCondition.university = [
                        $scope.university_pro.cities.cities, null
                    ];
                } else {
                    $scope.condition.otherCondition.university = [
                        $scope.university_pro.cities.cities,
                        $scope.condition.otherCondition.universitytmp
                    ];
                }
                getTalent();
            }
            $scope.changeSection = function() {
                if ($scope.condition.otherCondition.majortmp == null) {
                    $scope.condition.otherCondition.major = [
                        $scope.major.division.division, null
                    ];
                } else {
                    $scope.condition.otherCondition.major = [
                        $scope.major.division.division,
                        $scope.condition.otherCondition.majortmp
                    ];
                }
                getTalent();
            }
            $scope.commentData = function(talent) {
                angular.element('.stars li a').removeClass('active');
                $http
                    .post('forHr/getHrComment', talent)
                    .success(
                        function(data) {
                            console.log(data);
                            data.offer = data.offer == null ? null : data.offer ? 'true' : 'false';
                            data.postgraduate = data.postgraduate == null ? null : data.postgraduate ? 'true' : 'false';
                            $scope.historyHrComment = data;
                            setStarLevel(
                                $scope.historyHrComment.appearance,
                                "Appearance");
                            setStarLevel(
                                $scope.historyHrComment.demeanour,
                                "Demeanour");
                            setStarLevel(
                                $scope.historyHrComment.english,
                                "English");
                        })


                getTalent();

            }

            $scope.importExcel = function() {
                checkFileFormat();
            }
            $scope.ToggleSelection = function() {
                $scope.showSelection = !$scope.showSelection;
                $scope.condition.selections[2].showinfo = !$scope.condition.selections[2].showinfo;
                $scope.condition.selections[3].showinfo = !$scope.condition.selections[3].showinfo;
                $scope.condition.selections[4].showinfo = !$scope.condition.selections[4].showinfo;
                $scope.condition.selections[5].showinfo = !$scope.condition.selections[5].showinfo;
            }
            $scope.showcandidateInfo = function(talent) {
                console.log("talent---->" + talent);
                $scope.initCandidateInfoImg(talent);
                $scope.infoState = 1;
                $scope.languageLevelPreview = [];
                $http
                    .post('forHr/getCandidateAllInfo', talent)
                    .success(
                        function(data) {
                            console.log(data);
                            $scope.candidateinfo = data;
                            $scope.languageLevel = $scope.candidateinfo.candidate.languageLevel
                                .split(",");
                            for (var i = 0; i <= $scope.languageLevel.length - 1; i++) {
                                if ($scope.languageLevel[i] != "") {
                                    $scope.languageLevelPreview
                                        .push($scope.languageLevel[i]);
                                };
                            }
                            $scope.candidateinfo.candidate.languageLevel = $scope.languageLevelPreview;
                            getTalent();
                        });

            }

            $scope.initCandidateInfoImg = function(talent) {
                $http.get('getPhoto', {
                    params: { 'nationalId': talent.nationalId }
                }).success(function(data) {
                    if (data == 'photo_not_exists')
                        angular.element('#candidateInfoImg').attr('src', '/wechat/resources/images/default.jpg');
                    else
                        angular.element('#candidateInfoImg').attr('src', 'getPhoto?nationalId=' + talent.nationalId);
                }).error(function(data) {
                    console.log("Init Avatar failed");
                });
                console.log(talent);
            }

            $scope.changeShow = function() {
                $scope.editable = false;
            }
            $scope.changeState_Info = function(state) {
                $scope.infoState = state;
            }

            function getStarLevel() {
                $scope.historyHrComment.appearance = angular.element(
                    '#Appearance li .active').html();
                $scope.historyHrComment.demeanour = angular.element(
                    '#Demeanour li .active').html();
                $scope.historyHrComment.english = angular.element(
                    '#English li .active').html();
            }

            function setStarLevel(level, name) {
                switch (level) {
                    case 0:
                        break;
                    case 1:
                        $('#' + name).find(".one").trigger("click");
                        break;
                    case 2:
                        $('#' + name).find(".two").trigger("click");
                        break;
                    case 3:
                        $('#' + name).find(".thr").trigger("click");
                        break;
                    case 4:
                        $('#' + name).find(".fou").trigger("click");
                        break;
                    case 5:
                        $('#' + name).find(".fiv").trigger("click");
                        break;
                }
            }
            // get talent information
            function getTalent() {
                var sendcondition = $scope.condition;
                console.log($scope.condition);
                $http
                    .post('forHr/multiSearch', sendcondition)
                    .success(
                        function(redata) {
                            console.log(redata);
                            $scope.talentDataSet = redata;
                            // $scope.talentEmail = redata;
                            $scope.bigTotalItems = $scope.talentDataSet.talentPage.dataTotal; // init
                            // pages
                            // when
                            // refresh
                            // talentData
                            // $scope.all = false;
                            $scope.page.allcheck = false;
                            allExportBystatusfunction(sendcondition);
                        });
            }
            //					by dww export by status

            allExportBystatusfunction = function(sendcondition) {
                    $http.post('/wechat/exportAllExcel', sendcondition).success(function(redata) {
                        // alert("success");
                        $scope.xxx = redata;
                        console.log($scope.xxx);
                    })
                }
                // exportTo function
                // get select nationalIds
            function exportTo(address) {
                var talentDatas = $scope.talentDataSet.talentDatas;
                var nationalIds = new Array(talentDatas.length);
                var selected = false;
                for (var i in talentDatas) {
                    if (talentDatas[i].check == true) {
                        selected = true;
                        nationalIds[i] = talentDatas[i].nationalId;
                    }
                }
                if (selected) {
                    window.location.href = address + nationalIds;

                } else {
                    window.location.href = address + $scope.xxx;
                }
            }

            // shuffle
            function shuffle(talentData) { // v1.0
                for (var j, x, i = talentData.length; i; j = parseInt(Math
                        .random() * i), x = talentData[--i], talentData[i] = talentData[j], talentData[j] = x)
                ;
                return talentData;
            };

            // doUpload
            function doUpload() {
                var formData = new FormData($("#uploadForm")[0]);
                $
                    .ajax({
                        url: '/wechat/uploadExcel',
                        type: 'POST',
                        data: formData,
                        async: false,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(failNum) {
                            getTalent();
                            $('#importModal').modal('hide');
                            if (failNum == 0) {
                                change_dialog_error('Success');
                                show_dialog_error();
                            } else {
                                change_dialog_error('Success&nbsp&nbsp' + failNum + '&nbsp record ignored');
                                show_dialog_error();
                            }
                        },
                        error: function(returndata) {
                            console.log(returndata);
                            change_dialog_error('Fail to import Excel');
                            show_dialog_error();
                        }
                    });
            }
            // checkFileFormat
            function checkFileFormat() {
                var file = document.getElementById("importExcel");
                var filename = file.value;
                var filetype = filename
                    .substring(filename.indexOf("."));;
                if (filetype == "") {
                    change_dialog_error('can\'t find upload file');
                    show_dialog_error();
                } else if (filetype != ".xlsx") {
                    change_dialog_error('upload file must be \'xlsx\'');
                    show_dialog_error();
                } else {
                    doUpload();
                }
            }

        });

app.directive('navFocus', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.on('click', 'a', function() {
                var _this = $(this);
                if (!_this.next().hasClass('nav-second-level')) {
                    $('.sidebar-collapse').find('li').removeClass('on');
                    _this.parent('li').addClass('on');
                }
            })
        }
    };
});
