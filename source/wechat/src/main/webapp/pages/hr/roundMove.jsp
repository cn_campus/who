<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html>
<html ng-app="offerModule">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="/wechat/resources/images/icon/favicon.ico">
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<link rel='stylesheet' type="text/css" href="<c:url value="/resources/css/font.css"/>"/>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/font-awesome.min.css"/>"/>
<link rel='stylesheet' type="text/css" href="<c:url value="/resources/css/bootstrap.min.css"/>"/>
<link rel='stylesheet' type="text/css" href="<c:url value="/resources/css/bootstrap-datetimepicker.min.css"/>"/>
<link rel='stylesheet' type="text/css" href="/wechat/resources/css/candidate.css"/>
<link rel='stylesheet' type="text/css" href="/wechat/resources/css/weui.min.css" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/roundmove.css"/>"/>
<link rel='stylesheet' type="text/css"  href="<c:url value="/resources/css/report.css"/>" />
<link rel="stylesheet" type="text/css" href="<c:url value ="/resources/css/style_email.css"/>"/>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/importExcelDropify.min.css"/>"/>
<link rel='stylesheet' type="text/css" href="/wechat/resources/css/stylesheet.css" />
<link rel='stylesheet' type="text/css" href="/wechat/resources/css/github-light.css" />

<!-- <link href="/wechat/resources/css/zzsc.css" type="text/css" rel="stylesheet" /> -->
<!-- <link rel="stylesheet" href="/wechat/resources/css/ng-right-click.css"> -->

<script>
   	var name = "<sec:authentication property="name" htmlEscape="false"/>"
</script>
<title>list</title>
</head>
<body ng-controller="offerController">

	<!-- top-nav -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle pull-left" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand">
                <img src="/wechat/resources/images/logo.png">
                <span class="">Talent Pool</span>
            </a>
        </div>
        <ul class="nav navbar-right navbar-signout">
            <li><a href="/wechat/logout">Sign out</a></li>
        </ul>
    </div>
    <!-- side-nav -->
    <div class="side-content">
        <nav nav-focus id="menu" class="navbar-default navbar-fixed-side side_menu" role="navigation">
            <div class="sidebar-collapse collapse in">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-user">
                        <div class="user-img">
                            <span class="img-circle">
                                    <img id="avatar"  class="dropdown img-circle img-responsive" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">   
                                    <ul class="dropdown-menu changehrinfo" aria-labelledby="dLabel">
                                        <li>
                                        <a data-toggle="modal" style="color:black" href="#" data-target="#Modal_newpwd" onclick="changepasswordInit()">Change Password</a>
                                        </li>
                                        <li>
                                        <a style="color:black" href="#" data-toggle="modal" data-target="#Modal_avatar">Upload Avatar</a>
                                        </li>
                                    </ul> 
                                </span>
                        </div>
                        <div class="user-info">
                            <div class="user-name" ng-bind="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></div>
                            <sec:authorize access="hasRole('ROLE_HRM')">
                                <a class="manager-hyperlink" ui-sref="newhr">Manager</a>
                            </sec:authorize>
                        </div>
                    </li>
                    <li class="active">
                        <a href="javascript:;" ng-click="toggle()" data-target="#process" data-toggle="collapse">
                            <i class="fa fa-bars fa-fw"></i> Recruitment Process
                            <i class="fa fa-angle-right angle-right" ng-if="!showSubnav"></i>
                            <i class="fa fa-angle-down angle-down" ng-if="showSubnav"></i>
                        </a>
                        <!-- Q0 -- Q5 -->
                        <ul id="process" class="nav nav-second-level collapse in">
                            <li ng-repeat="state in status.list" class="{{state.clazz}}" ng-click="updateState(state.name,state.title)">
                                <a ui-sref="process">
                                    <i class="{{state.iclazz}}"></i> {{state.title}}
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li ui-sref-active="on" id="sheets">
                        <a ui-sref="sheets">
                            <i class="fa fa-file-text fa-fw"></i> Sheets
                        </a>
                    </li>
                    <li ui-sref-active="on" id="candidate">
                        <a ui-sref="addcandidate.basic">
                            <i class="fa fa-user-plus fa-fw"></i> Add Candidates
                        </a>
                    </li>
                    <li ui-sref-active="on" id="help">
                        <a ui-sref="help">
                            <i class="fa fa-question-circle fa-fw"></i> Help
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
   <!--  Right Content -->
    <div class="content" ui-view></div>
    <!-- Candidte Info -->
    <div class="modal fade" id="candidateinfo">
        <div class="modal-dialog candidateinfo_modal">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="modal_close_button" type="button" data-dismiss="modal" aria-label="Close" data-dismiss="modal" aria-hidden="true" ng-click="changeShow()"></button>
                    <div class="modal_title"><span>Candidate Information</span></div>
                </div>
                <!-- state1 -->
                <div class="modal-body" ng-show="infoState==1">
                    <div class="cainfo_main">
                        <div class="title">
                            <div>
                                <div class="selection_box left">
                                    <span class="box on" ng-click="changeState_Info(1)">1</span>
                                    <span class="box" ng-click="changeState_Info(2)">2</span>
                                    <span class="box" ng-click="changeState_Info(3)">3</span>
                                    <span class="box" ng-click="changeState_Info(4)">4</span>
                                </div>
                                <sec:authorize access="hasRole('ROLE_HRM')">
                                    <div class="selection_box right">
                                        <button class="btn" ng-click="editInfo(candidateinfo)">Edit</button>
                                        <button class="btn" ng-click="updateInfo(candidateinfo, candidatePhoto)">Save</button>
                                    </div>
                                </sec:authorize>
                            </div>
                            <span class="title_main">Basic Information</span>
                        </div>
                        <div class="cainfo_head">
                            <div style="float:left;">
                                <div class="title">Name:
                                    <span ng-bind="candidateinfo.candidate.name" ng-show="!editable"></span>
                                    <input type="text" ng-model="candidateinfo.candidate.oldName" ng-show="editable">
                                </div>
                                <div class="tel_info">
                                    <span><i class="fa fa-phone" aria-hidden="true"></i> <span ng-bind="candidateinfo.candidate.cellPhone" ng-show="!editable"></span>
                                    <input type="text" ng-model="candidateinfo.candidate.oldCellPhone" ng-show="editable">
                                    </span>
                                    <span><i class="fa fa-envelope-o" aria-hidden="true"></i> <span ng-bind="candidateinfo.candidate.mailAddress" ng-show="!editable"></span>
                                    <input type="text" ng-model="candidateinfo.candidate.oldMailAddress" ng-show="editable">
                                    </span>
                                </div>
                            </div>
                            <div class="edit-userimg">
                                <div class="show-userimg" ng-show="!editable">
                                    <img id="candidateInfoImg" class=" img-responsive"  aria-haspopup="true" aria-expanded="false">
                                </div>
                                <div ng-show="editable">
                                    <input type="file" accept="image/*" id="input-file-to-destroy" file-model="candidatePhoto" data-default-file="" class="dropify" data-max-file-size="4M"/>
                                </div>                                                          	                           	
                            </div>
                        </div>
                        <div class="cainfo_body">
                            <table>
                                <tr>
                                    <td>
                                        <div>Q1</div>
                                        <div ng-bind="candidateinfo.application.round1Score" style="font-weight:bolder" ng-show="!editable"></div>
                                        <!-- <input type="text" ng-model="candidateinfo.application.olRround1Score" ng-show="editable">  -->
                                    </td>
                                    <td>
                                        <div>Q2</div>
                                        <div ng-bind="candidateinfo.application.round2Score" style="font-weight:bolder" ng-show="!editable"></div>
                                        <!-- <input type="text" ng-model="candidateinfo.application.oldRound2Score" ng-show="editable">  -->
                                    </td>
                                    <td>
                                        <div>Q3</div>
                                        <div ng-bind="candidateinfo.application.round3Score" style="font-weight:bolder" ng-show="!editable"></div>
                                        <!-- <input type="text" ng-model="candidateinfo.application.oldRound3Score" ng-show="editable">  -->
                                    </td>
                                    <td>
                                        <div>Status</div>
                                        <div ng-bind="candidateinfo.application.status" style="font-weight:bolder" ng-show="!editable"></div>
                                        <select ng-model="candidateinfo.application.oldStatus" ng-options="updatestatu.name as updatestatu.title for  updatestatu in updatestatus" ng-show="editable"></select>
                                        <!-- <input type="text" ng-model="candidateinfo.application.oldStatus" ng-show="editable"> -->
                                    </td>
                                    <td>
                                        <div>Availability</div>
                                        <div style="font-weight:bolder" ng-bind="candidateinfo.candidate.availableSchedule" ng-show="!editable"></div>
                                        <select ng-model="candidateinfo.candidate.oldAvailableSchedule" ng-options="availabilityOption.value as availabilityOption.option for availabilityOption in availabilityOptions" ng-show="editable"></select>
                                        <!-- <input type="text" ng-model="candidateinfo.candidate.oldAvailableSchedule" ng-show="editable"> -->
                                    </td>
                                    <td>
                                        <div>Date of Application</div>
                                        <div ng-bind="candidateinfo.application.applyDate" style="font-weight:bolder"></div>
                                        <!-- <input type="text" class="form_datetime" ng-model="candidateinfo.application.oldApplyDate" ng-show="editable">  -->
                                    </td>
                                </tr>
                            </table>
                            <div class="info_span">
                                <span class="info_title col-md-6">Position Applied For：</span>
                                <span class="info_content" ng-bind="candidateinfo.application.applyPosition" ng-show="!editable"></span>
                                <!-- <input type="text" ng-model="candidateinfo.application.oldApplyPosition" ng-show="editable">  -->
                                <select ng-model="candidateinfo.application.oldApplyPosition" ng-options="positionOption for positionOption in positionOptions" ng-show="editable">
                                </select>
                            </div>
                            <div class="info_span">
                                <span class="info_title col-md-6">Graduate date：</span>
                                <span class="info_content" ng-bind="candidateinfo.candidate.graduateDate" ng-show="!editable"></span>
                                <input type="text" class="form_datetime" ng-model="candidateinfo.candidate.oldGraduateDate" ng-show="editable">
                            </div>
                            <div class="info_span">
                                <span class="info_title col-md-6">Plan after graduation：</span>
                                <span class="info_content" ng-bind="candidateinfo.candidate.graduatePlan" ng-show="!editable"></span>
                                <!-- <input type="text" ng-model="candidateinfo.candidate.oldGraduatePlan" ng-show="editable">  -->
                                <select ng-model="candidateinfo.candidate.oldGraduatePlan" ng-options="graduationplanOption for graduationplanOption in graduationplanOptions" ng-show="editable">
                                </select>
                            </div>
                            <div class="info_span">
                                <span class="info_title col-md-6">Paper test score(Q1):</span>
                                <span class="info_content" ng-bind="candidateinfo.application.round1Score" ng-show="!editable"></span>
                                <input type="text" ng-model="candidateinfo.application.oldRound1Score" ng-show="editable">
                            </div>
                            <div class="info_span">
                                <span class="info_title col-md-6">Group interview score(Q2):</span>
                                <span class="info_content" ng-bind="candidateinfo.application.round2Score" ng-show="!editable"></span>
                                <input type="text" ng-model="candidateinfo.application.oldRound2Score" ng-show="editable">
                            </div>
                            <div class="info_span">
                                <span class="info_title col-md-6">Final interview score(Q3):</span>
                                <span class="info_content" ng-bind="candidateinfo.application.round3Score" ng-show="!editable"></span>
                                <input type="text" ng-model="candidateinfo.application.oldRound3Score" ng-show="editable">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- state2 -->
                <div class="modal-body" ng-show="infoState==2">
                    <div class="cainfo_main">
                        <div class="title">
                            <div>
                                <div class="selection_box left">
                                    <span class="box" ng-click="changeState_Info(1)">1</span>
                                    <span class="box on" ng-click="changeState_Info(2)">2</span>
                                    <span class="box" ng-click="changeState_Info(3)">3</span>
                                    <span class="box" ng-click="changeState_Info(4)">4</span>
                                </div>
                                <sec:authorize access="hasRole('ROLE_HRM')">
                                    <div class="selection_box right">
                                        <button class="btn" ng-click="editInfo(candidateinfo)">Edit</button>
                                        <button class="btn" ng-click="updateInfo(candidateinfo, candidatePhoto)">Save</button>
                                    </div>
                                </sec:authorize>
                            </div>
                            <span class="title_main">Basic Information</span>
                        </div>
                        <div class="cainfo_head">
                            <div class="title">Name: <span ng-bind="candidateinfo.candidate.name" ng-show="!editable"></span>
                                <input type="text" ng-model="candidateinfo.candidate.oldName" ng-show="editable">
                            </div>
                        </div>
                        <div class="cainfo_body">
                            <div class="info_span">
                                <span class="info_title col-md-6">Gender：</span>
                                <span class="info_content" ng-bind="candidateinfo.candidate.gender" ng-show="!editable"></span>
                                <!-- <input type="text" ng-model="candidateinfo.candidate.oldGender" ng-show="editable">  -->
                                <select ng-model="candidateinfo.candidate.oldGender" ng-options="chooseGender for chooseGender in chooseGenders" ng-show="editable">
                                </select>
                            </div>
                            <div class="info_span">
                                <span class="info_title col-md-6">ID Number：</span>
                                <span class="info_content" ng-bind="candidateinfo.candidate.nationalId"></span>
                                <!-- <input type="text" ng-model="candidateinfo.candidate.oldNationalId" ng-show="editable">  -->
                            </div>
                            <div class="info_span">
                                <span class="info_title col-md-6">Passport:</span>
                                <span class="info_content" ng-bind="candidateinfo.candidate.passportNumber"></span>
                                <!-- <input type="text" ng-model="candidateinfo.candidate.oldPassportNumber" ng-show="editable"> -->
                            </div>
                            <div class="info_span">
                                <span class="info_title col-md-6">Date of Birth:</span>
                                <span class="info_content" ng-bind="candidateinfo.candidate.birthday" ng-show="!editable"></span>
                                <input type="text" class="form_datetime" ng-model="candidateinfo.candidate.oldBirthday" ng-show="editable">
                            </div>
                            <div class="info_span">
                                <span class="info_title col-md-6">Birthplace:</span>
                                <span class="info_content" ng-bind="candidateinfo.candidate.birthProvince" ng-show="!editable"></span>
                                <input type="text" ng-model="candidateinfo.candidate.oldBirthProvince" ng-show="editable">
                            </div>
                            <!--                <div class="info">
                        <span class="info_title col-md-6">Address：</span>
                        <span class="info_content">ABC</span>
                    </div> -->
                            <div class="info_span">
                                <span class="info_title col-md-6">Home Phone:</span>
                                <span class="info_content" ng-bind="candidateinfo.candidate.homePhone" ng-show="!editable"></span>
                                <input type="text" ng-model="candidateinfo.candidate.oldHomePhone" ng-show="editable">
                            </div>
                            <div class="info_span">
                                <span class="info_title col-md-6">Nationality:</span>
                                <span class="info_content" ng-bind="candidateinfo.candidate.nationality" ng-show="!editable"></span>
                                <input type="text" ng-model="candidateinfo.candidate.oldNationality" ng-show="editable">
                            </div>
                            <div class="info_span">
                                <span class="info_title col-md-6">Personal page：</span>
                                <span class="info_content" ng-bind="candidateinfo.candidate.personalPage" ng-show="!editable"></span>
                                <input type="text" ng-model="candidateinfo.candidate.oldPersonalPage" ng-show="editable">
                            </div>
                            <div class="info_span">
                                <span class="info_title col-md-6">Language level：</span>
                                <span class="info_content" ng-bind="candidateinfo.candidate.languageLevel" ng-show="!editable"></span>
                                <!-- <input type="text" ng-model="candidateinfo.candidate.oldLanguageLevel" ng-show="editable">  -->
                                <div ng-show="editable">
                                    <label>
                                        <input ng-model="languageLevel[0]" ng-change="chooseLanguageLevel()" type="checkbox" name="optionsRadios" ng-true-value="'CET-4'" ng-false-value="" id="optionsRadios1" value="option1">&nbsp;CET-4</label>
                                    <label>
                                        <input ng-model="languageLevel[1]" ng-change="chooseLanguageLevel()" type="checkbox" name="optionsRadios" ng-true-value="'CET-6'" ng-false-value="" id="optionsRadios1" value="option1">&nbsp;CET-6</label>
                                    <label>
                                        <input ng-model="languageLevel[2]" ng-change="chooseLanguageLevel()" type="checkbox" name="optionsRadios" ng-true-value="'TEM-4'" ng-false-value="" id="optionsRadios1" value="option1">&nbsp;TEM-4</label>
                                    <label>
                                        <input ng-model="languageLevel[3]" ng-change="chooseLanguageLevel()" type="checkbox" name="optionsRadios" ng-true-value="'TEM-8'" ng-false-value="" id="optionsRadios1" value="option1">&nbsp;TEM-8</label>
                                    <label>
                                        <input ng-model="languageLevel[4]" ng-change="chooseLanguageLevel()" type="checkbox" name="optionsRadios" ng-true-value="'JPT-1'" ng-false-value="" id="optionsRadios1" value="option1">&nbsp;JPT-1</label>
                                    <label>
                                        <input ng-model="languageLevel[5]" ng-change="chooseLanguageLevel()" type="checkbox" name="optionsRadios" ng-true-value="'JPT-2'" ng-false-value="" id="optionsRadios1" value="option1">&nbsp;JPT-2</label>
                                    <label>
                                        <input ng-model="languageLevel[6]" ng-change="chooseLanguageLevel()" type="checkbox" name="optionsRadios" ng-true-value="'JPT-3'" ng-false-value="" id="optionsRadios1" value="option1">&nbsp;JPT-3</label>
                                    <label>
                                        <input ng-model="languageLevel[7]" ng-change="chooseLanguageLevel()" type="text" name="optionsRadios" value="" placeholder="Others">
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- state3 -->
                <div class="modal-body" ng-show="infoState==3">
                    <div class="cainfo_main">
                        <div class="title">
                            <div>
                                <div class="selection_box left">
                                    <span class="box" ng-click="changeState_Info(1)">1</span>
                                    <span class="box" ng-click="changeState_Info(2)">2</span>
                                    <span class="box on" ng-click="changeState_Info(3)">3</span>
                                    <span class="box" ng-click="changeState_Info(4)">4</span>
                                </div>
                                <sec:authorize access="hasRole('ROLE_HRM')">
                                    <div class="selection_box right">
                                        <button class="btn" ng-click="editInfo(candidateinfo)">Edit</button>
                                        <button class="btn" ng-click="updateInfo(candidateinfo, candidatePhoto)">Save</button>
                                    </div>
                                </sec:authorize>
                            </div>
                            <span class="title_main">Basic Information</span>
                        </div>
                        <div class="cainfo_head">
                            <div class="title">Name: <span ng-bind="candidateinfo.candidate.name" ng-show="!editable"></span>
                                <input type="text" ng-model="candidateinfo.candidate.oldName" ng-show="editable">
                            </div>
                        </div>
                        <div class="cainfo_body">
                            <div class="info">
                                <span class="info_title col-md-12">Education Experience：</span>
                                <div class="eduexp_info" ng-repeat="eduExp in candidateinfo.candidate.educationExperiences">
                                    <span class="info_title col-md-6">Experience{{$index+1}}:</span>
                                    <table>
                                        <tr>
                                            <td>Degree:</td>
                                            <td><span ng-bind="eduExp.degree" ng-show="!editable"></span>
                                                <!-- <input type="text" ng-model="eduExp.oldDegree" ng-show="editable"> -->
                                                <select ng-model="eduExp.oldDegree" ng-options="degree for degree in degrees" ng-show="editable"></select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>University:</td>
                                            <td><span ng-bind="eduExp.university" ng-show="!editable"></span>
                                                <input type="text" ng-model="eduExp.oldUniversity" placeholder="formate:city,university" ng-show="editable">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Major:</td>
                                            <td><span ng-bind="eduExp.major" ng-show="!editable"></span>
                                                <input type="text" ng-model="eduExp.oldMajor" placeholder="formate:division,section" ng-show="editable">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Period:</td>
                                            <td><span ng-bind="eduExp.period" ng-show="!editable"></span>
                                                <input type="text" ng-model="eduExp.oldPeriod" ng-show="editable">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Gpa:</td>
                                            <td><span ng-bind="eduExp.gpa" ng-show="!editable"></span>
                                                <input type="text" ng-model="eduExp.oldGpa" ng-show="editable">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div ng-show="editable">
                                    <table>
                                        <tr>
                                            <td ng-click="addEdu()"><img src="/wechat/resources/images/icon/add.png"></td>
                                            <td>Add or Drop</td>
                                            <td ng-click="dropEdu()"><img src="/wechat/resources/images/icon/close.png"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="info" style="border-top: 1px solid #f2f2f2;">
                                <span class="info_title col-md-12">Work Experience:</span>
                                <div class="workexp_info" ng-repeat="workExp in candidateinfo.candidate.workExperiences">
                                    <span class="info_title col-md-6">Experience{{$index+1}}:</span>
                                    <table>
                                        <tr>
                                            <td>company:</td>
                                            <td><span ng-bind="workExp.company" ng-show="!editable"></span>
                                                <input type="text" ng-model="workExp.oldCompany" ng-show="editable">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>position:</td>
                                            <td><span ng-bind="workExp.position" ng-show="!editable"></span>
                                                <input type="text" ng-model="workExp.oldPosition" ng-show="editable">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>details:</td>
                                            <td><span ng-bind="workExp.details" ng-show="!editable"></span>
                                                <textarea type="text" ng-model="workExp.oldDetails" ng-show="editable" rows="" cols=""></textarea>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div ng-show="editable">
                                    <table>
                                        <tr>
                                            <td ng-click="addWork()"><img src="/wechat/resources/images/icon/add.png"></td>
                                            <td>Add or Drop</td>
                                            <td ng-click="dropWork()"><img src="/wechat/resources/images/icon/close.png"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end state3 -->
                <!-- state4 -->
                <div class="modal-body" ng-show="infoState==4">
                    <div class="cainfo_main">
                        <div class="title">
                            <div>
                                <div class="selection_box left">
                                    <span class="box" ng-click="changeState_Info(1)">1</span>
                                    <span class="box" ng-click="changeState_Info(2)">2</span>
                                    <span class="box" ng-click="changeState_Info(3)">3</span>
                                    <span class="box on" ng-click="changeState_Info(4)">4</span>
                                </div>
                                <sec:authorize access="hasRole('ROLE_HRM')">
                                    <div class="selection_box right">
                                        <button class="btn" ng-click="editInfo(candidateinfo)">Edit</button>
                                        <button class="btn" ng-click="updateInfo(candidateinfo, candidatePhoto)">Save</button>
                                    </div>
                                </sec:authorize>
                            </div>
                            <span class="title_main">Basic Information</span>
                        </div>
                        <div class="cainfo_head">
                            <div class="title">Name: <span ng-bind="candidateinfo.candidate.name" ng-show="!editable"></span>
                                <input type="text" ng-model="candidateinfo.candidate.oldName" ng-show="editable">
                            </div>
                        </div>
                        <div class="cainfo_body">
                            <div class="info">
                                <span class="info_title">Questionnaire：</span>
                            </div>
                            <div class="info" ng-repeat="questionlist in candidateinfo.questAndAnswer.questAndAnswerList">
                                <div class="info_title">
                                    Q:{{questionlist.question}}
                                </div>
                                <div>
                                    A:<span ng-bind="questionlist.answer" ng-show="!editable"></span>
                                    <textarea rows="5" cols="" type="text" ng-model="questionlist.oldAnswer" ng-show="editable"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- endstate4 -->
            </div>
        </div>
    </div>
    <!-- /.modal -->
    <!-- /.container -->
    <!-- Change HR Password -->
    <div ng-controller="hrInfoController">
        <div class="modal fade" id="Modal_newpwd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title" id="myModalLabel">
                            <font color="#FF8000">Hello </font>
                            <span style="color:#FF8000" ng-bind="user.userName"></span>
                        </h3>
                    </div>
                    <form name="newHrForm" method="post">
                        <div class="modal-body">
                            <ul class="changePwd_ul">
                                <li class="changePwd_head">
                                    <div class="title">Password rules: 8 to 16 characters must contain at least one of each of the following characters</div>
                                    <div class="title">(1) Upper and lower case letter</div>
                                    <div class="title">(2) Number and special characters e.g: !,@,$,~,%</div>
                                </li>
                                <li class="changePwd_content">
                                    <div class="col-md-4"><span for="initialPassword">*Last password:</span></div>
                                    <div class="col-md-4">
                                        <input ng-model="user.password" ng-change="verifyPassword()" ng-bind="verifyPw" ng-style="newpwdstyle" type="password" autofocus="autofocus" autocomplete="off" />
                                    </div>
                                    <div class="col-md-4"><span ng-show="showValid" ng-bind="validLabel">correct</span></div>
                                </li>
                                <li class="changePwd_content">
                                    <div class="col-md-4"><span for="initialPassword">*New password:</span></div>
                                    <div class="col-md-4">
                                        <input ng-model="user.passwordNew" ng-bind="checkPw" type="password" ng-style="pwstyle" ng-change="checkPassword()" autofocus="autofocus"  autocomplete="off" />
                                    </div>
                                    <div class="col-md-4"><span for="initialPassword" ng-show="showpw" ng-bind="pwlabel"></span></div>
                                </li>
                                <li class="changePwd_content">
                                    <div class="col-md-4"><span for="confirmPassword">*Confirm password:</span></div>
                                    <div class="col-md-4">
                                        <input ng-model="confirmpw" ng-bind="checkConfirm" ng-style="confirmstyle" type="password" ng-change="confirmPw()" autofocus="autofocus" autocomplete="off" />
                                    </div>
                                    <div class="col-md-4"><span for="confirmPassword" ng-show="showconfirm">please confirm password</span></div>
                                </li>
                            </ul>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
                        </button>
                        <button type="button" class="btn btn-warning" data-toggle="modal" ng-click="changePassword()" ng-disabled="!(checkPw&&checkConfirm&&verifyPw)" data-target="#Modal_save_success" data-dismiss="modal">Complete</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal -->
        </div>
        <div class="modal fade" id="Modal_save_success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title" id="myModalLabel">
                            <font color="#FF8000">New Password</font>
                        </h3>
                    </div>
                    <div>
                        <br>
                        <h4 class="modal-title" id="myModalLabel" align="center">
                            <font>Saved!</font>
                        </h4>
                        <br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="Modal_avatar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title" id="myModalLabel">
                            <font color="#FF8000">Upload Avatar</font>
                        </h3>
                    </div>
                    <div class="modal-body">
                        <div style="color:red;text-align: center">The picture must be less than 1M!</div>
                        <br>
                        <form id="avatarForm" enctype="multipart/form-data">
                            <input type="file" accept="image/*" file-model="myFile" class="dropify" data-default-file="" data-max-file-size="1M" />
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal" ng-click="uploadAvatar()">Upload</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

 
    
<!-- /changehrinfo -->

	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!--<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->

</body>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-1.11.3.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
<script type="text/javascript" src="/wechat/resources/js/tinymce-dist/tinymce.js"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular.min.js"/>"></script>
<script type="text/javascript" src="resources/js/jquery.nicescroll.js"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-animate.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/ui-bootstrap-tpls-1.3.3.min.js"/>"></script>
<script type="text/javascript" src="resources/js/angular-ui-router.js"></script>
<script type="text/javascript" src="<c:url value="/resources/js/prettify.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/multiselect.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/multiselect.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.jqprint.js"/>"></script>
<script type="text/javascript" src="/wechat/resources/js/jquery-migrate.js"></script>
<script type="text/javascript" src="resources/js/tinymce.js"></script>
<script type="text/javascript" src="<%=path%>/resources/js/router.js"></script>
<script type="text/javascript" src="<c:url value="/resources/js/dropify.min.js"/>"></script>
<script type="text/javascript" src="<%=path%>/resources/js/recruitmentProcess.js"></script>
<script type="text/javascript" src="<%=path%>/resources/js/report.js"></script>
<script type="text/javascript" src="/wechat/resources/js/candidate.js" charset="utf-8"></script>
<script type="text/javascript"	src="<c:url value="/resources/js/authority.js"/>"></script> 
<script type="text/javascript" src="/wechat/resources/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="/wechat/resources/js/errorinfo.js"></script>
<script type="text/javascript" src="/wechat/resources/js/ui-bootstrap-tpls-1.3.3.min.js"></script>
<script type="text/javascript" src="/wechat/resources/js/hrInfo.js"></script>
<script type="text/javascript" src="/wechat/resources/js/elastic.js"></script>
<script type="text/javascript" src="/wechat/resources/js/democtrl.js"></script>
<!--  <script type="text/javascript" src="/wechat/resources/js/zzsc.js"></script> -->
<script type="text/javascript" src="/wechat/resources/js/contextMenu.js"></script>



<script type="text/javascript">
        // side-menu scroller
		$(function () {
            $( '#menu' ).niceScroll({
            cursorcolor: '#ccc',
            railalign: 'right', 
            cursorborder: "none", 
            horizrailenabled: false, 
            zindex: 2001, 
            cursoropacitymax: 1,
            // autohidemode:'false',
            spacebarenabled: false 
        });

            // $('.dropify').dropify({
            //     messages: {
            //         default: 'Choose a photo',
            //         replace: 'Choose a photo',
            //         remove: 'Removed',
            //         error: 'Sorry, this photo is too large'
            //     }
            // });

            var drDestroy = $('#input-file-to-destroy').dropify();
                drDestroy = drDestroy.data('dropify')
                $('#toggleDropify').on('click', function(e){
                    e.preventDefault();
                    if (drDestroy.isDropified()) {
                        drDestroy.destroy();
                    } else {
                        drDestroy.init();
                    }
                })

    		$(window).bind("resize", function() {
    		     if ($(this).width() < 768) {
    		        $('div.sidebar-collapse').addClass('collapse')
    		    } else {
    		        $('div.sidebar-collapse').removeClass('collapse')
    		    }
    		});
    		$('.navbar-toggle').on('click', function () {
    		    if ($('#menu.hidden-xs').length){
    		        $('#menu').removeClass('hidden-xs');
    		    }
    		    else{
    		        $('#menu').addClass('hidden-xs');
    		    }
    		});
  		});
  		var config = {
            params : {
                source : 'HRAddCandidate'
            }
        };
</script>

<script type="text/javascript"> 
var _LT_ajax_proxyXmlUrl="http://srvfree.api.51ditu.com/apisrv/p4x?";function LTNS(){LTNS.info={time:'Thu Jul 19 10:00:08 UTC+0800 2012',version:'1',ov:'1.3 Ver 20070705'};var w=function(uq){var iq=0,oq=0;var pq=uq.length;var aq=new String();var sq=-1;var dq=0;for(var fq=0;fq<pq;fq++){var gq=uq.charCodeAt(fq);gq=(gq==95)?63:((gq==44)?62:((gq>=97)?(gq-61):((gq>=65)?(gq-55):(gq-48))));oq=(oq<<6)+gq;iq+=6;while(iq>=8){var hq=oq>>(iq-8);if(dq>0){sq=(sq<<6)+(hq&(0x3f));dq--;if(dq==0){aq+=String.fromCharCode(sq);};}else{if(hq>=224){sq=hq&(0xf);dq=2;}else if(hq>=128){sq=hq&(0x1f);dq=1;}else{aq+=String.fromCharCode(hq);};};oq=oq-(hq<<(iq-8));iq-=8;};};return aq;};var q=["post","&charset=","url=","lt","get","undefined","utf-8","","string","error","loaded","complete","interactive","unload","shape",'function',"on",w("SsDoQN1q")];var i=window;var o=document;function yq(uq,iq){for(var oq in iq){uq[oq]=iq[oq];};}function a(){};function s(yq,uq){return function(){return uq.apply(yq,arguments)};};function d(yq){return(yq.tagName||yq==i||yq==o);};function f(yq){return(yq&&yq.ownerDocument&&yq.ownerDocument.parentWindow)?yq.ownerDocument.parentWindow:i;};function g(yq){if(!yq){yq=[];};if(!yq[0]){yq[0]=f().event;};if(yq[0]&&!yq[0].target&&yq[0].srcElement){yq[0].target=yq[0].srcElement};return yq;};function h(yq,uq){return function(){uq.apply(yq,g(arguments));};};function j(yq){yq=g(yq);if(!yq){return;};if(yq.stopPropagation){yq.preventDefault();yq.stopPropagation();}else if(o.all){yq.cancelBubble=true;yq.returnValue=false;};};function k(yq){yq=g(yq);if(!yq){return;};if(o.all){yq.cancelBubble=true;yq.returnValue=true;}else if(yq.stopPropagation){yq.stopPropagation();};};function l(yq,event,uq,iq,oq){return c(yq,event,d(yq)?h(uq,iq):s(uq,iq),oq);};function z(yq,uq){if(!yq){return;};if(yq.parentNode&&!uq){yq.parentNode.removeChild(yq);};b(yq);var iq;while(iq=yq.firstChild){z(iq);};};function x(yq,uq){return function(){var e=this;yq.apply(e,arguments);v(uq);}};function c(yq,event,uq,iq){var oq=[yq,event];if(iq){uq=x(uq,oq)};var pq=d(yq);if(pq){uq=s(yq,uq);if(yq.addEventListener){yq.addEventListener(event,uq,false);}else if(yq.attachEvent){yq.attachEvent(q[16]+event,uq);}else{var aq=yq[q[16]+event];if(typeof(aq)==q[15]){yq[q[16]+event]=function(){aq();uq();};}else{yq[q[16]+event]=uq;};};};oq.push(uq);if(yq._LT_E_&&pq!=q[14]){yq._LT_E_.push(oq);}else{yq._LT_E_=(pq==q[14])?[]:[oq];};if(!a.allEvents){a.allEvents=[];};if(event!=q[13]){a.allEvents.push(oq);};return oq;};function v(yq){if(!yq||yq.length==0){return;};if(arguments.length>1){var uq=arguments[0]._LT_E_;for(var iq=0;iq<uq.length;iq++){if(uq[iq][1]==arguments[1]&&uq[iq][2]==arguments[2]){return v(uq[iq]);}};};try{if(d(yq[0])){if(yq[0].removeEventListener){yq[0].removeEventListener(yq[1],yq[2],false);}else if(yq[0].detachEvent){yq[0].detachEvent(q[16]+yq[1],yq[2]);}else{yq[0][q[16]+yq[1]]=null;};};var oq=yq[0]._LT_E_;for(var iq=oq.length-1;iq>=0;iq--){if(oq[iq]==yq){oq.splice(iq,1);break;};};}catch(pq){};oq=a.allEvents;for(var iq=oq.length-1;iq>=0;iq--){if(oq[iq]==yq){oq.splice(iq,1);break;};};while(yq.length>0){yq.pop()};delete yq;};function b(yq,event){if(!yq||!yq._LT_E_){return;};var uq,iq=yq._LT_E_;for(var oq=iq.length-1;oq>=0;oq--){uq=iq[oq];if(!event||uq[1]==event){v(uq);};};};function n(){var yq=a.allEvents;if(yq){for(var uq=yq.length-1;uq>=0;uq--){v(yq[uq]);};};a.allEvents=null;};function m(yq,event,uq){if(d(yq)){try{if(yq.fireEvent){yq.fireEvent(q[16]+event);};if(yq.dispatchEvent){var iq="mouseover,mouseout,mousemove,mousedown,mouseup,click,dbclick";var oq=o.createEvent("Event");oq.initEvent(event,false,true);yq.dispatchEvent(oq);};}catch(pq){alert('LTEvent.trigger error.');};}else{if(!uq){uq=[];};var aq=yq._LT_E_;if(aq&&aq.length>0){for(var sq=aq.length-1;sq>=0;sq--){var dq=aq[sq];if(dq&&dq[2]){if(dq[1]=="*"){dq[2].apply(yq,[event,uq]);};if(dq[1]==event){dq[2].apply(yq,uq);};};};};};};function _(){return o.all?(o.readyState!="loading"&&o.readyState!=q[12]):(a.readyState==q[11])};function Q(){if(!a.unLoadListener){a.unLoadListener=c(i,q[13],n);};if(!o.all&&!a.readyState){a.readyState=q[12];c(o,"DOMContentLoaded",function(){a.readyState=q[11];},true);};};yq(a,{getCallback:s,isHtmlControl:d,getObjWin:f,getWindowEvent:g,createAdapter:h,cancelBubble:j,returnTrue:k,bind:l,deposeNode:z,runOnceHandle:x,addListener:c,removeListener:v,clearListeners:b,clearAllListeners:n,trigger:m,isDocumentLoaded:_,load:Q});function W(yq,uq){var e=this;e[0]=yq?parseInt(yq):0;e[1]=uq?parseInt(uq):0;e.Longitude=e[0];e.Latitude=e[1];};yq(W.prototype,{getLongitude:function(){var e=this;return e[0];},getLatitude:function(){var e=this;return e[1];},setLongitude:function(yq){var e=this;e[0]=parseInt(yq);},setLatitude:function(yq){var e=this;e[1]=parseInt(yq);}});function E(yq){var e=this;e.win=yq?yq:i;};yq(E.prototype,{load:function(yq,uq,iq,oq){var e=this;if(!e.onLoadStart(yq)){return;};e.objName=oq?oq:"_OLR";var pq=e.win;pq[e.objName]=null;var uq=uq?uq:q[6];if(!e.jsFile){e.jsFile=pq.document.createElement(q[17]);e.jsFile.type=w("T6LuT2zgONPXSsDoQN1q");e.jsFile.defer=true;pq.document.body.insertBefore(e.jsFile,pq.document.body.firstChild);l(e.jsFile,"readystatechange",e,e.onReadyStateChange);l(e.jsFile,"load",e,e.onLoad);};e.jsFile.charset=uq;e.jsFile.src=yq;e.running=true;e.crypt=iq;},onLoadStart:function(yq){var e=this;m(e,"loadstart",[yq]);return true;},onLoad:function(yq){var e=this;var uq=e.win;if(uq[e.objName]){var iq=uq[e.objName];uq[e.objName]=null;m(e,q[10],[e.parseObject(iq)]);}else{m(e,q[9],[]);};if(!o.all&&e.jsFile&&e.jsFile.parentNode==uq.document.body){e.jsFile.removeAttribute("src");uq.document.body.removeChild(e.jsFile);delete e.jsFile;};e.running=false;},parseObject:function(yq){var e=this;if(e.crypt||yq.e){U(yq);};return yq;},onReadyStateChange:function(yq){var e=this;if(!e.jsFile||(e.jsFile.readyState!=q[10])){return;};e.onLoad();}});function R(yq,uq,iq,oq){var oq=oq?oq:i;var pq={url:yq,handle:uq,charset:iq,win:oq,classNum:1};var aq=I(oq);l(aq,q[10],pq,T);l(aq,q[9],pq,T);aq.load(yq,iq);};function T(yq){var e=this;e.classNum--;if(yq&&yq._classUrls){var uq;while(uq=yq._classUrls.pop()){e.classNum++;R(uq,Y(e),e.charset,e.win);};};if(e.classNum==0){e.handle.apply(e);};};function Y(yq){return function(){yq.classNum--;if(yq.classNum==0){yq.handle.apply(yq);};};};function U(yq){var uq;if(yq.t){yq.t=A(yq.t);};for(uq in yq.a){if(typeof yq.a[uq]==q[8]){yq.a[uq]=A(yq.a[uq]);};};for(uq in yq.c){if(typeof yq.c[uq]!=q[15]){U(yq.c[uq]);};};};function I(yq){var yq=yq?yq:i;var uq;if(!yq._LT_OLRS){yq._LT_OLRS=[];};for(var iq=0;iq<yq._LT_OLRS.length;iq++){if(yq._LT_OLRS[iq].running==false){uq=yq._LT_OLRS[iq];b(uq);break;};};if(!uq){uq=new E(yq);yq._LT_OLRS.push(uq);return uq;};uq.running=true;return uq;};function O(yq,uq){for(var iq=0;iq<yq.c.length;iq++){if(yq.c[iq].n==uq){return yq.c[iq];};};};function P(yq){var uq=0,iq=0;var oq=yq.length;var pq=[];for(var aq=0;aq<oq;aq++){var sq=yq.charCodeAt(aq);if(sq>=2048){iq=(iq<<24)+(((sq>>12)|0xe0)<<16)+((((sq&0xfff)>>6)|0x80)<<8)+((sq&0x3f)|0x80);uq+=24;}else if(sq>=128){iq=(iq<<16)+(((sq>>6)|0xc0)<<8)+((sq&0x3f)|0x80);uq+=16;}else{uq+=8;iq=(iq<<8)+sq;};while(uq>=6){var dq=iq>>(uq-6);iq=iq-(dq<<(uq-6));uq-=6;var sq=(dq<=9)?(dq+48):((dq<=35)?(dq+55):((dq<=61)?(dq+61):((dq==62)?44:95)));pq.push(String.fromCharCode(sq));};};if(uq>0){var dq=iq<<(6-uq);pq.push(String.fromCharCode((dq<=9)?(dq+48):((dq<=35)?(dq+55):((dq<=61)?(dq+61):((dq==62)?44:95)))));};return pq.join(q[7]);};function A(yq){var uq=0,iq=0;var oq=yq.length;var pq=[];var aq=-1;var sq=0;for(var dq=0;dq<oq;dq++){var fq=yq.charCodeAt(dq);fq=(fq==95)?63:((fq==44)?62:((fq>=97)?(fq-61):((fq>=65)?(fq-55):(fq-48))));iq=(iq<<6)+fq;uq+=6;while(uq>=8){var gq=iq>>(uq-8);if(sq>0){aq=(aq<<6)+(gq&(0x3f));sq--;if(sq==0){pq.push(String.fromCharCode(aq));};}else{if(gq>=224){aq=gq&(0xf);sq=2;}else if(gq>=128){aq=gq&(0x1f);sq=1;}else{pq.push(String.fromCharCode(gq));};};iq=iq-(gq<<(uq-8));uq-=8;};};return pq.join(q[7]);};yq(E,{loadClass:R,onClassLoaded:T,onSubClassLoaded:Y,doDecrypt:U,getObject:I,getChild:O,encrypt:P,decrypt:A});function S(){};function D(){if(i.XMLHttpRequest){return new XMLHttpRequest();}else if(typeof(ActiveXObject)!=q[5]){return new ActiveXObject("Microsoft.XMLHTTP");};};function F(yq,uq){var iq=D();iq.open(q[4],yq,true);iq.onreadystatechange=function(){if(iq.readyState!=4){return;};var oq=iq.responseXML.xml?iq.responseXML:L(iq.responseText);uq(oq);};iq.send(null);};function G(yq,uq,iq){iq=iq?iq:q[6];var oq=I();c(oq,q[10],function(pq){if(pq.n==q[9]&&pq.a.src==q[3]){return;};var aq=X(pq);uq.apply(null,[aq]);});oq.load(i._LT_ajax_proxyXmlUrl+q[2]+encodeURIComponent(encodeURIComponent(yq))+q[1]+iq);};function H(yq,uq){var iq=D();iq.open(q[4],yq,true);iq.onreadystatechange=function(){if(iq.readyState!=4){return;};uq(iq.responseText);};iq.send(null);};function J(yq,uq,iq){var oq=iq?iq.toLowerCase()==q[0]?q[0]:pq:q[4];var aq=D();aq.open(oq,yq,true);aq.onreadystatechange=function(){if(aq.readyState!=4){return;};uq(eval('('+aq.responseText+')'));};aq.send(null);};function K(yq,uq,iq){iq=iq?iq:q[6];var oq=I();c(oq,q[10],function(pq){if(pq.n==q[9]&&pq.a.src==q[3]){return;};uq.apply(null,[A(pq)]);});oq.load(i._LT_ajax_proxyTextUrl+q[2]+encodeURIComponent(encodeURIComponent(yq))+q[1]+iq);};function L(yq){var uq;if(typeof(ActiveXObject)!=q[5]&&typeof(GetObject)!=q[5]){try{uq=new ActiveXObject("Msxml2.DOMDocument");}catch(iq){uq=new ActiveXObject("Msxml.DOMDocument");};if(yq){uq.loadXML(yq);};}else{if(yq){if(typeof DOMParser!=q[5]){uq=new DOMParser().parseFromString(yq,"text/xml")};}else{if(o.implementation&&o.implementation.createDocument){uq=o.implementation.createDocument(q[7],q[7],null);};};};return uq;};function Z(yq,uq){if(!uq){yq.i={};uq=yq;};if(yq.a.id){uq.i[yq.a.id]=yq;};for(var iq=0;iq<yq.c.length;iq++){Z(yq.c[iq],uq);};};function X(yq){var uq=L();if(yq){uq.appendChild(C(yq,uq));};return uq;};function C(yq,uq){var iq=uq.createElement(yq.n);for(var oq in yq.a){iq.setAttribute(oq,yq.a[oq]);};for(var pq=0;pq<yq.c.length;pq++){iq.appendChild(C(yq.c[pq],uq));};if(yq.t){iq.appendChild(uq.createTextNode(yq.t));};return iq;};function V(yq,uq){if(typeof(yq)==q[8]){yq=L(yq);};if(yq.documentElement){yq=yq.documentElement;};var iq={n:yq.nodeName,a:{},c:[]};if(!uq){iq.i={};uq=iq;};if(yq.attributes){for(var oq=0;oq<yq.attributes.length;oq++){var pq=yq.attributes[oq].nodeName,aq=yq.attributes[oq].nodeValue;iq.a[pq]=aq;if(pq=="id"){uq.i[aq]=iq;};};};for(var oq=0;oq<yq.childNodes.length;oq++){var sq=yq.childNodes[oq].nodeType;if(sq>=3&&sq<=6){var dq=yq.childNodes[oq].nodeValue;if(!iq.t&&!new RegExp("^[\\s]+$").test(dq)){iq.t=dq;};};if(sq==1){uq=uq?uq:iq;iq.c.push(V(yq.childNodes[oq],uq));};};return iq;};function B(yq,uq){var iq,oq=false;if(typeof yq.xml!=q[5]){try{iq=yq.selectNodes(uq);}catch(pq){oq=true;};}else{oq=true;};if(!oq){return iq;};var aq=yq.ownerDocument?yq.ownerDocument:yq;var sq=aq.createNSResolver(aq.documentElement);var dq=aq.evaluate(uq,yq,sq,XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,null);iq=[];for(var fq=0;fq<dq.snapshotLength;fq++){iq.push(dq.snapshotItem(fq));};return iq;};function N(yq,uq){var iq,oq=false;try{iq=yq.selectSingleNode(uq);}catch(pq){oq=true;};if(!oq){return iq;};return B(yq,uq)[0];};function M(yq){return i.ActiveXObject?yq.text:(yq.childNodes[0]?yq.childNodes[0].nodeValue:q[7]);};function qq(yq,uq){if(!yq.c||!yq.c.length){return;};for(var iq=0;iq<yq.c.length;iq++){if(yq.c[iq].n==uq){return yq.c[iq];};};};yq(S,{createHttpRequest:D,loadXml:F,loadRemoteXml:G,loadText:H,loadJson:J,loadRemoteText:K,createDocument:L,createJsonId:Z,toXml:X,toXmlNode:C,toJson:V,selectNodes:B,selectSingleNode:N,getNodeValue:M,getJsonChild:qq});function wq(yq,uq){var e=this;e[0]=yq?parseInt(yq):0;e[1]=uq?parseInt(uq):0;e.EARTH_RADIUS=6378137;e.PI=Math.PI;e.js_point={x:e[0],y:e[1]};e.point=e.WGS84ToMercator(e.js_point);e[0]=e.point.x;e[1]=e.point.y;e.Longitude=e[0];e.Latitude=e[1];};yq(wq.prototype,{getLongitude:function(){var e=this;return e[0];},getLatitude:function(){var e=this;return e[1];},setLongitude:function(yq){var e=this;e[0]=parseInt(yq);},setLatitude:function(yq){var e=this;e[1]=parseInt(yq);},WGS84ToMercator:function(yq){var e=this;var uq=yq;var iq=yq.x/100000.0;var oq=yq.y/100000.0;var pq=e.PI*e.EARTH_RADIUS;var aq=iq*pq/180.0;var sq=Math.log(Math.tan((90+oq)*e.PI/360.0))/(e.PI/180.0);sq=sq*pq/180.0;uq.x=Math.round(aq);uq.y=Math.round(sq);return uq;}});function eq(yq,uq){var e=this;e[0]=yq?parseInt(yq):0;e[1]=uq?parseInt(uq):0;e.EARTH_RADIUS=6378137;e.PI=Math.PI;e.js_point={x:e[0],y:e[1]};e.point=e.MercatorToWGS84(e.js_point);e[0]=e.point.x;e[1]=e.point.y;e.Longitude=e[0];e.Latitude=e[1];};yq(eq.prototype,{getLongitude:function(){var e=this;return e[0];},getLatitude:function(){var e=this;return e[1];},setLongitude:function(yq){var e=this;e[0]=parseInt(yq);},setLatitude:function(yq){var e=this;e[1]=parseInt(yq);},MercatorToWGS84:function(yq){var e=this;var uq=yq;var iq=e.PI*e.EARTH_RADIUS/180.0;var oq=yq.x/iq;var pq=yq.y/iq;pq=180.0/e.PI*(2*Math.atan(Math.exp(pq*e.PI/180.0))-e.PI/2.0);uq.x=Math.round(oq*100000.0);uq.y=Math.round(pq*100000.0);return uq;}});function rq(){var e=this;e.length=0;e.prefix="shi_2011";};yq(rq.prototype,{put:function(yq,uq){var e=this;e[e.prefix+yq]=uq;e.length++;},get:function(yq){var e=this;return typeof e[e.prefix+yq]==q[5]?null:e[e.prefix+yq];},keySet:function(){var e=this;var yq=new Array();var uq=0;for(var iq in e){if(iq.substring(0,e.prefix.length)==e.prefix){yq[uq++]=iq.substring(e.prefix.length);};};return yq.length==0?null:yq;},values:function(){var e=this;var yq=new Array();var uq=0;for(var iq in e){if(iq.substring(0,e.prefix.length)==e.prefix){yq[uq++]=e[iq];};};return yq.length==0?null:yq;},size:function(){var e=this;return e.length;},remove:function(yq){var e=this;delete e[e.prefix+yq];e.length--;},update:function(yq,uq){var e=this;e[e.prefix+yq]=uq;},clear:function(){var e=this;for(var yq in e){if(yq.substring(0,e.prefix.length)==e.prefix){delete e[yq];};};e.length=0;},isEmpty:function(){var e=this;return e.length==0;},containsKey:function(yq){var e=this;for(var uq in e){if(uq==e.prefix+yq){return true;};};return false;},containsValue:function(yq){var e=this;for(var uq in e){if(e[uq]==yq){return true;};};return false;},putAll:function(yq){var e=this;if(yq==null)return;if(yq.constructor!=rq){return;};var uq=yq.keySet();var iq=yq.values();for(var oq in uq){e.put(uq[oq],iq[oq]);};}});function tq(){var e=this;e.base64EncodeChars="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";e.base64DecodeChars=new Array(-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,62,-1,-1,-1,63,52,53,54,55,56,57,58,59,60,61,-1,-1,-1,-1,-1,-1,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,-1,-1,-1,-1,-1,-1,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,-1,-1,-1,-1,-1);};yq(tq.prototype,{base64encode:function(yq){var uq,iq,oq;var pq,aq,sq;oq=yq.length;iq=0;uq=q[7];while(iq<oq){pq=yq.charCodeAt(iq++)&0xff;if(iq==oq){uq+=tq.base64EncodeChars.charAt(pq>>2);uq+=tq.base64EncodeChars.charAt((pq&0x3)<<4);uq+="==";break;};aq=yq.charCodeAt(iq++);if(iq==oq){uq+=tq.base64EncodeChars.charAt(pq>>2);uq+=tq.base64EncodeChars.charAt(((pq&0x3)<<4)|((aq&0xF0)>>4));uq+=tq.base64EncodeChars.charAt((aq&0xF)<<2);uq+="=";break;};sq=yq.charCodeAt(iq++);uq+=tq.base64EncodeChars.charAt(pq>>2);uq+=tq.base64EncodeChars.charAt(((pq&0x3)<<4)|((aq&0xF0)>>4));uq+=tq.base64EncodeChars.charAt(((aq&0xF)<<2)|((sq&0xC0)>>6));uq+=tq.base64EncodeChars.charAt(sq&0x3F);};return uq;},base64decode:function(yq){var e=this;var uq,iq,oq,pq;var aq,sq,dq;sq=yq.length;aq=0;dq=q[7];while(aq<sq){do{uq=e.base64DecodeChars[yq.charCodeAt(aq++)&0xff];}while(aq<sq&&uq==-1);if(uq==-1)break;do{iq=e.base64DecodeChars[yq.charCodeAt(aq++)&0xff];}while(aq<sq&&iq==-1);if(iq==-1)break;dq+=String.fromCharCode((uq<<2)|((iq&0x30)>>4));do{oq=yq.charCodeAt(aq++)&0xff;if(oq==61)return dq;oq=e.base64DecodeChars[oq];}while(aq<sq&&oq==-1);if(oq==-1)break;dq+=String.fromCharCode(((iq&0XF)<<4)|((oq&0x3C)>>2));do{pq=yq.charCodeAt(aq++)&0xff;if(pq==61)return dq;pq=e.base64DecodeChars[pq];}while(aq<sq&&pq==-1);if(pq==-1)break;dq+=String.fromCharCode(((oq&0x03)<<6)|pq);};return dq;},utf16to8:function(yq){var uq,iq,oq,pq;uq=q[7];oq=yq.length;for(iq=0;iq<oq;iq++){pq=yq.charCodeAt(iq);if((pq>=0x0001)&&(pq<=0x007F)){uq+=yq.charAt(iq);}else if(pq>0x07FF){uq+=String.fromCharCode(0xE0|((pq>>12)&0x0F));uq+=String.fromCharCode(0x80|((pq>>6)&0x3F));uq+=String.fromCharCode(0x80|((pq>>0)&0x3F));}else{uq+=String.fromCharCode(0xC0|((pq>>6)&0x1F));uq+=String.fromCharCode(0x80|((pq>>0)&0x3F));};};return uq;},utf8to16:function(yq){var uq,iq,oq,pq;var aq,sq;uq=q[7];oq=yq.length;iq=0;while(iq<oq){pq=yq.charCodeAt(iq++);switch(pq>>4){case 0:case 1:case 2:case 3:case 4:case 5:case 6:case 7:uq+=yq.charAt(iq-1);break;case 12:case 13:aq=yq.charCodeAt(iq++);uq+=String.fromCharCode(((pq&0x1F)<<6)|(aq&0x3F));break;case 14:aq=yq.charCodeAt(iq++);sq=yq.charCodeAt(iq++);uq+=String.fromCharCode(((pq&0x0F)<<12)|((aq&0x3F)<<6)|((sq&0x3F)<<0));break;};};return uq;}});var p=function(a){var s=o.getElementsByTagName(q[17]);var d=new RegExp(a,"i");for(var f=0;f<s.length;f++){var g=s[f];if(g.src&&d.test(g.src)){break;};};return!o.all||f<s.length;};if(!p(w("NbnpAYXeT7HmA3ywSoa_EYylAJyeFpfRN7TTArmkAIerCMHfT7LSBcDlRIzgSozXQc5uN2vgSrnpAYG")))return false;yq(i,{LTEvent:a,LTPoint:W,LTObjectLoader:E,LTAjax:S,LTPointMercator:wq,LTPointWGS84:eq,LTHashMap:rq,LTBase64:tq});Q();};LTNS();

    </script> 
</html> 