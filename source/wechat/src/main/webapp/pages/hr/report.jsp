<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html >
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
	<meta name="description" content="">
	<meta name="author" content="">
	<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
	<link rel="icon" href="/wechat/resources/images/icon/favicon.ico">
	<title></title>
</head>
<body >
		<!-- Initialize the plugin: -->
		<div class="muti-search">
		    <h3>Print Sheet</h3>
		    <div class="search-condition">
		        <h5>Please select the conditions</h5>
		        <!-- Select Table: -->
		        <ul>
		            <li class="title">Select Table:</li>
		            <li ng-repeat="reportConfig in condition.reportConfigs" ng-class="{'selected' : reportConfig.check}" ng-click="selectTable(reportConfig)" ng-bind="reportConfig.title"></li>
		        </ul>
		        <!-- Select Status: -->
		        <ul>
		            <li class="title" ng-bind="condition.title"></li>
		            <li ng-repeat="select in condition.selections" ng-class="{'selected' : select.check}" ng-click="selectStatus(select)" ng-bind="select.title"></li>
		        </ul>
		        <!-- Input Score: -->
		        <div class="round-score">
		            <div class="round">
		                <span class="title">Round 1 score:</span>
		                <input type="text" ng-model="condition.otherCondition.round1[0]" autocomplete="false" ng-change="changeScore()"> —
		                <input type="text" ng-model="condition.otherCondition.round1[1]" autocomplete="false" ng-change="changeScore()">
		            </div>
		            <div class="round">
		                <span class="title">Round 2 score:</span>
		                <input type="text" ng-model="condition.otherCondition.round2[0]" autocomplete="false" ng-change="changeScore()"> —
		                <input type="text" ng-model="condition.otherCondition.round2[1]" autocomplete="false" ng-change="changeScore()">
		            </div>
		            <div class="round">
		                <span class="title">Round 3 score:</span>
		                <input type="text" ng-model="condition.otherCondition.round3[0]" autocomplete="false" ng-change="changeScore()"> —
		                <input type="text" ng-model="condition.otherCondition.round4[1]" autocomplete="false" ng-change="changeScore()">
		            </div>
		        </div>
		    </div>
		</div>
		<!-- Report dataList: -->
		<div class="search-result">
		    <div class="form-button">
		        <input type="button" class="btn " ng-click="shuffleData()" value="Shuffle" />
		        <input type="button" class="btn " ng-click="moveUp()" value="Move Up" />
		        <input type="button" class="btn " ng-click="moveDown()" value="Move Down" />
		        <input type="button" class="btn " ng-click="print()" value="Printing Preview" />
		        <input type="button" class="btn " ng-show="reportdata.title=='Group Interview Evaluation Sheet'" ng-click="editGroupReport()" value="Edit Group Report" />
		        <div class="user-info">
		            <div class="user-name"></div>
		            <sec:authorize access="hasRole('ROLE_HRM')">
		                <input type="button" class="btn " ng-click="getAllColumns()" data-toggle="modal" data-target="#confirm" value="Reform Table" />
		            </sec:authorize>
		        </div>
		    </div>
		    <table class="table signature-table " id="printContainer" ng-if="reportdata.title!='Group Interview Evaluation Sheet'">
		        <thead ng-show="showHead">
		            <tr>
		                <td style="border:0 !important;" ng-style="checkall"></td>
		                <td style="border:0 !important;" colspan='{{reportdata.theadLength}}'><span>PwC SDC FY<span ng-model="fy" ng-if="!showEdit" ng-click="editYear()">{{oldYear}}</span>
		                    <input ng-model="oldFy" ng-if="showEdit" style="width: 20px" ng-blur="saveData(oldFy)"> Campus Recruitment
		                    <br>{{reportTitle}} Sign-in List</span>
		                </td>
		                <td style="border:0 !important;" colspan='1'>
		                    <img style="width:50px;" src="/wechat/resources/images/logo.png">
		                </td>
		            </tr>
		            <tr>
		                <th class="check-box signature-checkbox" ng-style="checkall">
		                    <input type="checkbox" class="checkbox" id="all.row" ng-model="all.check" ng-click="selectAll(all)" ng-checked="all.check">
		                    <label for="all.row"></label>
		                </th>
		                <!-- <th class="signature-thead">Interview Time</th> -->
		                <th class="signature-thead" ng-repeat="head in reportdata.tableHead track by $index" ng-bind="head"></th>
		            </tr>
		        </thead>
		        <tbody>
		            <tr ng-repeat="row in reportdata.tableBody track by $index" class="checkPrint" ng-style="row.show">
		                <td class="check-box" ng-style="checkeach">
		                    <input type="checkbox" class="checkbox" id="{{$index}}" ng-model="row.check" ng-checked="row.check">
		                    <label for="{{$index}}"></label>
		                </td>
		                <td class="signature-thead" ng-repeat="column in row track by $index" ng-bind="column"></td>
		            </tr>
		        </tbody>
		    </table>
		    <table class="table table_group_ev group-table" ng-if="reportdata.title=='Group Interview Evaluation Sheet' && !all.print">
		        <thead>
		            <tr>
		                <td class="group-thead" ng-style="checkall"></td>
		                <td style="text-align: left;border:0 !important;;" colspan="8"><b>Interviewer:</b></td>
		                <td colspan="6" style="text-align: left;border:0 !important;;"><b>Interview Date:</b></td>
		                <td style="border:0 !important;" colspan="3" rowspan="2">
		                    <img style="width:50px;" src="/wechat/resources/images/logo.png">
		                </td>
		            </tr>
		            <tr>
		                <td class="group-thead" ng-style="checkall" style="border:0 !important;;"></td>
		                <td style="text-align: left;border:0 !important;;" colspan="5"><b>Interviewer evaluation<span style="margin-left:40px">Score:</span></b></td>
		            </tr>
		            <tr></tr>
		            <tr>
		                <th class="group-thead" ng-style="checkall"></th>
		                <td rowspan="3" style="vertical-align : middle;"><b>时间</b></td>
		                <td rowspan="3" style="vertical-align : middle;"><b>组别</b></td>
		                <td rowspan="3" style="vertical-align : middle;width: 180px;"><b>申请者信息</b></td>
		                <td colspan="5"><b>企业文化</b></td>
		                <td colspan="6"><b>能力评估</b></td>
		                <td colspan="2"><b>问答环节</b></td>
		                <td rowspan="3" style="vertical-align : middle;"><b>Rank</b></td>
		            </tr>
		            <tr>
		                <th class="group-thead" ng-style="checkall"></th>
		                <td><b>Global Acumen</b></td>
		                <td><b>Whole Leadership</b></td>
		                <td><b>Relationships</b></td>
		                <td><b>Business Acumen</b></td>
		                <td><b>Technical Capabilities</b></td>
		                <td><b>主动积极</b></td>
		                <td><b>判断理解能力</b></td>
		                <td><b>逻辑条理性</b></td>
		                <td><b>表达能力</b></td>
		                <td><b>应变能力</b></td>
		                <td><b>社交能力</b></td>
		                <td><b>自我评价</b></td>
		                <td><b>IT咨询行业认识</b></td>
		            </tr>
		            <tr>
		                <th class="check-box group-thead" ng-style="checkall1">
		                    <input type="checkbox" class="checkbox" id="all.row" ng-model="all.check" ng-click="selectAll(all)" ng-checked="all.check">
		                    <label for="all.row"></label>
		                </th>
		                <td colspan="5">(弱 1-5 强)</td>
		                <td colspan="6">(弱 1-5 强)</td>
		                <td colspan="2">(弱 1-5 强)</td>
		            </tr>
		        </thead>
		        <tbody>
		            <tr ng-repeat="row in reportdata.tableBody track by $index" class="checkPrint" ng-style=row.show>
		                <td class="check-box group-thead" ng-style="checkeach1">
		                    <input type="checkbox" class="checkbox" id="{{$index}}" ng-model="row.check" ng-checked="row.check">
		                    <label for="{{$index}}"></label>
		                </td>
		                <td></td>
		                <td></td>
		                <td style="width: 180px;">{{reportdata.tableBody[$index][2]}}
		                    <br>{{reportdata.tableBody[$index][3]}}</td>
		                <td></td>
		                <td></td>
		                <td></td>
		                <td></td>
		                <td></td>
		                <td></td>
		                <td></td>
		                <td></td>
		                <td></td>
		                <td></td>
		                <td></td>
		                <td></td>
		                <td></td>
		                <td></td>
		            </tr>
		        </tbody>
		    </table>
		    <!-- group evaluation preview print -->
		    <div id="printContainer" ng-if="reportdata.title=='Group Interview Evaluation Sheet' && all.print">
		        <table ng-repeat="group in groupData track by $index" class="table table_group_ev group-table">
		            <thead>
		                <tr>
		                    <td style="text-align: left;border:0 !important;;" colspan="8"><b>Interviewer:</b></td>
		                    <td colspan="6" style="text-align: left;border:0 !important;;"><b>Interview Date:</b></td>
		                    <td style="border:0 !important;" colspan="3" rowspan="2">
		                        <img style="width:50px;" src="/wechat/resources/images/logo.png">
		                    </td>
		                </tr>
		                <tr>
		                    <td style="text-align: left;border:0 !important;;" colspan="5"><b>Interviewer evaluation<span style="margin-left:40px">Score:</span></b></td>
		                </tr>
		                <tr></tr>
		                <tr>
		                    <td rowspan="3" style="vertical-align : middle;"><b>时间</b></td>
		                    <td rowspan="3" style="vertical-align : middle;"><b>组别</b></td>
		                    <td rowspan="3" style="vertical-align : middle;width: 180px;"><b>申请者信息</b></td>
		                    <td colspan="5"><b>企业文化</b></td>
		                    <td colspan="6"><b>能力评估</b></td>
		                    <td colspan="2"><b>问答环节</b></td>
		                    <td rowspan="3" style="vertical-align : middle;"><b>Rank</b></td>
		                </tr>
		                <tr>
		                    <td><b>Global Acumen</b></td>
		                    <td><b>Whole Leadership</b></td>
		                    <td><b>Relationships</b></td>
		                    <td><b>Business Acumen</b></td>
		                    <td><b>Technical Capabilities</b></td>
		                    <td><b>主动积极</b></td>
		                    <td><b>判断理解能力</b></td>
		                    <td><b>逻辑条理性</b></td>
		                    <td><b>表达能力</b></td>
		                    <td><b>应变能力</b></td>
		                    <td><b>社交能力</b></td>
		                    <td><b>自我评价</b></td>
		                    <td><b>IT咨询行业认识</b></td>
		                </tr>
		                <tr>
		                    <td colspan="5">(弱 1-5 强)</td>
		                    <td colspan="6">(弱 1-5 强)</td>
		                    <td colspan="2">(弱 1-5 强)</td>
		                </tr>
		            </thead>
		            <tbody>
		                <tr ng-repeat="row in group track by $index" class="checkPrint" ng-style=row.show>
		                    <td class="interview-time"></td>
		                    <td class="group-no"></td>
		                    <td style="width: 180px;">{{group[$index][2]}}
		                        <br>{{group[$index][3]}}</td>
		                    <td></td>
		                    <td></td>
		                    <td></td>
		                    <td></td>
		                    <td></td>
		                    <td></td>
		                    <td></td>
		                    <td></td>
		                    <td></td>
		                    <td></td>
		                    <td></td>
		                    <td></td>
		                    <td></td>
		                    <td></td>
		                </tr>
		            </tbody>
		        </table>
		    </div>
		</div>
		<!-- Report Structure Configuration: -->
		<div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="vertical-align: middle; text-align: center;">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-head" style="text-align: center; margin: 20px 0 0px; font-weight: bold">
		                <h3><b>Report Structure Configuration</b></h3>
		            </div>
		            <div class="modal-body">
		                <form role="form" style="height: 75px; ">
		                    <p align="left">Please Select a Report：</p>
		                    <select id="reportkind" class="form-control" style="width: 260px;" ng-model="selectReportConfig" ng-options="reportConfig.title for reportConfig in condition.reportConfigs">
		                    </select>
		                </form>
		                <div class="row">
		                    <div class="col-xs-5">
		                        <select name="from" class="form-control" size="15" multiple="multiple" ng-model="leftSelected" ng-options="colomn for colomn in leftColumns"></select>
		                    </div>
		                    <div class="col-xs-2">
		                        <button type="button" class="btn btn-block" ng-click="allToRight()">
		                            <i class="glyphicon glyphicon-forward"></i>
		                        </button>
		                        <button type="button" class="btn btn-block" ng-click="someToRight()">
		                            <i class="glyphicon glyphicon-chevron-right"></i>
		                        </button>
		                        <button type="button" class="btn btn-block" ng-click="someToLeft()">
		                            <i class="glyphicon glyphicon-chevron-left"></i>
		                        </button>
		                        <button type="button" class="btn btn-block" ng-click="allToLeft()">
		                            <i class="glyphicon glyphicon-backward"></i>
		                        </button>
		                    </div>
		                    <div class="col-xs-5">
		                        <select name="to" class="form-control" size="15" multiple="multiple" ng-model="rightSelected" ng-options="colomn for colomn in selectReportConfig.configs"></select>
		                    </div>
		                </div>
		            </div>
		            <div class="modal-footer">
		                <button type="button" class="btn btn-warning" data-dismiss="modal" data-toggle="modal" data-target="#sure" data-ng-click="confirmReportConfig(selectReportConfig)">Sure</button>
		                <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
		            </div>
		        </div>
		    </div>
		</div>
	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!--<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->
</body>
</html>
<script type="text/javascript">
	$("#side-menu .on").removeClass("on");
</script>