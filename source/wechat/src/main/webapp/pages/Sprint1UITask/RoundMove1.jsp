<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html data-ng-app="MyModule" data-ng-controller="roundmove">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">
<link rel='stylesheet' type="text/css" href="<c:url value="/resources/css/bootstrap.css"/>"/>
<link rel='stylesheet' type="text/css" href="<c:url value="/resources/css/starter-template.css"/>"/>
<link rel='stylesheet' type="text/css" href="<c:url value="/resources/css/font.css"/>"/>
<link rel='stylesheet' type="text/css" href="<c:url value="/resources/css/bootstrap.min.css"/>"/>
<link rel='stylesheet' type="text/css" href="<c:url value="/resources/css/starter-template.css"/>"/>
<link rel='stylesheet' type="text/css" href="<c:url value="/resources/css/WHO_CSS.css"/>"/>
<link rel='stylesheet' type="text/css" href="<c:url value="/resources/css/test.css"/>"/>
<link rel='stylesheet' type="text/css" href="<c:url value="/resources/css/bootstrap-switch.min.css"/>"/>
<link rel='stylesheet' type="text/css" href="/wechat/resources/css/bootstrap.min.css"/>
<link rel='stylesheet' type="text/css" href="/wechat/resources/css/bootstrap-theme.min.css"/>
<title>list</title>



</head>
<body class="container">
<pre>{{selected|json}}</pre>
<pre>{{selectedTags|json}}</pre>
	<div><h3>Please select the conditions</h3></div>
	<ul class="select">
		<li class="select-list">
			<dl id="select1"> 
				<div ng-repeat="select in selections">
					<span>{{select.name}}：</span>
					<span data-ng-repeat="tag in select.tags">
						<label class="checkbox-inline"><input type="checkbox" id={{tag.id}} name="{{tag.name}}" ng-checked="isSelected(tag.id)" ng-click="updateSelection($event,tag.id)" data-ng-model="tag.check"> {{ tag.name}}</label>					
					</span>
				</div> 
			</dl>
		</li>
		</ul>
			</tbody>
		</table>
		</div>
		<div>
			<button type="button" class="btn btn-warning" id="btn1"
				style="padding: 5px 12px;" data-toggle="modal"
				data-target="#myModal3">The First Page</button>
			<button type="button" class="btn btn-warning" id="btn2"
				style="padding: 5px 12px;" data-toggle="modal"
				data-target="#myModal3">Pre Page</button>
			<button type="button" class="btn btn-warning" id="btn3"
				style="padding: 5px 12px;" data-toggle="modal"
				data-target="#myModal3">Next Page</button>
			<button type="button" class="btn btn-warning" id="btn4"
				style="padding: 5px 12px;" data-toggle="modal"
				data-target="#myModal3">The last Page</button>
		</div>
<div class="modal fade" id="confirm" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true"
			style="vertical-align: middle; text-align: center;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="page-header"
						style="text-align: center; margin: 10px 0 20px;">
						<h3>Intern Application Form</h3>
						<h4>PwC SDC</h4>
					</div>
					<div id="personalinformation" style="text-align: center;">
						<h4>Personal information</h4>
					</div>
					<form role="form" style="font-size: 18px;">
						<div class="form-group">
							Chinese Name： <label class="radio-inline"> <strong><span
									style="width: 150px;" id="chineseName"
									data-ng-model="candidate.chineseName">{{candidate.name}}</span></strong></label>
						</div>
						<div class="form-group">
							English Name： <label class="radio-inline"> <strong><span
									style="width: 150px;" id="englishName"
									data-ng-model="candidate.englishName">{{candidate.name}}</span></strong></label>
						</div>
						<div class="form-group">
							Gender: <label class="radio-inline"> <strong><span
									style="width: 150px;" id="gender"
									data-ng-model="candidate.gender">{{candidate.gender}}</span></strong></label>
						</div>
						<div class="form-group">
							ID Number： <label class="radio-inline"> <strong><span
									style="width: 150px;" id="idNumber"
									data-ng-model="candidate.idNumber">{{candidate.nationalId}}</span></strong></label>
						</div>
						<div class="form-group">
							Date of birth： <label class="radio-inline"> <strong><span
									style="width: 150px;" id="dateOfBirth"
									data-ng-model="candidate.dateOfBirth">{{candidate.birthday}}</span></strong></label>
						</div>
						<div class="form-group">
							Education&nbsp;&nbsp;Background： <label class="radio-inline">
								<strong><span style="width: 150px;"
									id="educationBackground"
									data-ng-model="candidate.educationBackground"
									data-ng-repeat="eduExp in candidate.educationExperiences">{{eduExp.degree}}</br></span></strong>
							</label>
							<!-- 			<ul class="nav navbar-nav" data-ng-repeat="eduExp in candidate.educationExperiences"> -->
							<!-- 					<li><a href="#personalinformation"><b>{{eduExp.university}}</b></a></li> -->
							<!-- 					<li><a href="#personalinformation"><b>{{eduExp.period}}</b></a></li> -->
							<!-- 				</ul> -->
						</div>
						<div class="form-group">
							Undergraduate university： <label class="radio-inline"> <strong><span
									style="width: 150px;" id="underGraduateUniversity"
									data-ng-model="candidate.underGraduateUniversity"
									data-ng-repeat="eduExp in candidate.educationExperiences">{{eduExp.university}}</br></span></strong></label>
						</div>
						<div class="form-group">
							Location of university： <label class="radio-inline"> <strong><span
									style="width: 150px;" id="locationOfUniversity"
									data-ng-model="candidate.locationOfUniversity">{{candidate.currentProvince}}</span></strong></label>
						</div>
						<div class="form-group">
							Language skill： <label class="radio-inline"> <strong><span
									style="width: 150px;" id="languageSkill"
									data-ng-model="candidate.languageSkill">{{candidate.languageLevel}}</span></strong></label>
						</div>
						<div class="form-group">
							Major： <label class="radio-inline"> <strong><span
									style="width: 150px;" id="major"
									data-ng-model="candidate.major"
									data-ng-repeat="eduExp in candidate.educationExperiences">{{eduExp.major}}</br></span></strong></label>
						</div>
						<div class="form-group">
							E-mail： <label class="radio-inline"> <strong><span
									style="width: 150px;" id="email"
									data-ng-model="candidate.email">{{candidate.mailAddress}}</span></strong></label>
						</div>
						<div class="form-group">
							Phone&nbsp;&nbsp;Number： <label class="radio-inline"> <strong><span
									style="width: 150px;" id="phoneNumber"
									data-ng-model="candidate.phoneNumber">{{candidate.cellPhone}}</span></strong></label>
						</div>
						<div class="form-group">
							Veteran status： <label class="radio-inline"> <strong><span
									style="width: 150px;" id="veteranStatus"
									data-ng-model="candidate.veteranStatus">Already served</span></strong></label>
						</div>
					</form>
					<form role="form" style="font-size: 18px;">
						<div id="jobinformation" style="text-align: center;">
							<h4>Position information</h4>
						</div>
						<HR
							style="FILTER: alpha(opacity = 100, finishopacity = 0, style = 3); margin-top: 10px; height: 1;"
							width="80%" color=#fd6709 SIZE=3>
						<div class="form-group">
							Position applied to： <label class="radio-inline"> <strong><span
									style="width: 150px;" id="position"
									data-ng-model="candidate.position">Java</span></strong></label>
						</div>
						<div class="form-group">
							Graduation Date： <label class="radio-inline"> <strong><span
									style="width: 150px;" id="graduationDate"
									data-ng-model="candidate.graduationDate"
									data-ng-repeat="eduExp in candidate.educationExperiences">{{eduExp.period}}</br></span></strong></label>
						</div>
						<div class="form-group">
							Attendance days： <label class="radio-inline"> <strong><span
									style="width: 150px;" id="attendanceDays"
									data-ng-model="candidate.attendanceDays">{{candidate.availableSchedule}}</span></strong></label>
						</div>
					</form>

					<div class="modal-footer">
						<button type="button" class="btn btn-warning" data-dismiss="modal"
							data-toggle="modal" data-target="#sure"
							data-ng-click="submitCandidate()">Sure</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		<!-- /.modal -->
		<!-- <div class="footerLogo"></div> -->
	</div>
	<!-- /.container -->


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!--<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->
</body>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-1.11.3.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/roundmove1.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-switch.min.js"/>"></script>
<%-- <script type="text/javascript" src="<c:url value="/resources/js/inquirePage.js"/>"></script> --%>
</html> 