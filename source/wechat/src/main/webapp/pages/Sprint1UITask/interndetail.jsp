<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%
String path = request.getContextPath();
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="zh" data-ng-app="candidatePage">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">
<LINK href="favicon.ico" type="image/x-icon" rel=icon>
<LINK href="favicon.ico" type="image/x-icon" rel="shortcut icon">
<title>Intern Detail</title>

<!-- Bootstrap core CSS -->
<link href="<%=path %>/resources/css/bootstrap.min.css" rel="stylesheet">
<link href="<%=path %>/resources/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<%=path %>/resources/css/starter-template.css" rel="stylesheet">
<script src="resources/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="<%=path %>/resources/js/angular.min.js"></script>
<script type="text/javascript" src="<%=path %>/resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<%=path %>/resources/js/interndetail.js"></script>
</head>
<body data-ng-controller="candidateController">
	<nav class="navbar navbar-inverse navbar-fixed-top white">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<div>
				&nbsp;&nbsp;&nbsp;<img src="<%=path %>/resources/images/footer_PWC_Logo_55_42.png">
				</div>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					
					<li><a href="#personalinformation"><b>Personal information</b></a></li>
					<li><a href="#jobinformation"><b>Position information</b></a></li>
					<li><a href="internlist"><b>Return to list page</b></a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>

	<div class="container">	
		<div class="page-header" style="text-align:center; margin: 10px 0 20px;">
			<h3>Intern Application Form</h3>
			<h4>PwC SDC</h4>
		</div>
	<div id="personalinformation" style="text-align:center;">
		<h4>Personal information</h4>
	</div>
     <HR style="FILTER: alpha(opacity=100,finishopacity=0,style=3);margin-top: 10px;height: 1;" width="80%" color=#fd6709 SIZE=3>
		<form role="form" style="font-size: 18px;">
			<div class="form-group">Chinese Name：
			  <label class="radio-inline">		
				<strong><span style="width:150px;" id="chineseName" data-ng-model="candidate.chineseName">{{candidate.name}}</span></strong></label>			  
		   </div>
			<div class="form-group">English Name：
			<label class="radio-inline">
				<strong><span style="width:150px;" id="englishName" data-ng-model="candidate.englishName">Lucas</span></strong></label>
			</div>
			<div class="form-group">Gender: 
			<label class="radio-inline">
			  	<strong><span style="width:150px;" id="gender" data-ng-model="candidate.gender">Male</span></strong></label>
			</div>
			<div class="form-group">ID Number：
			<label class="radio-inline">
				<strong><span style="width:150px;" id="idNumber" data-ng-model="candidate.idNumber">12345678910</span></strong></label>
			</div>
			<div class="form-group">Date of birth：
			<label class="radio-inline">
				<strong><span style="width:150px;" id="dateOfBirth" data-ng-model="candidate.dateOfBirth">2016.6.1</span></strong></label>
			</div>
			<div class="form-group">Education&nbsp;&nbsp;Background：
			<label class="radio-inline">
				<strong><span style="width:150px;" id="educationBackground" data-ng-model="candidate.educationBackground">Master Degree</span></strong></label>
			</div>
			<div class="form-group">Undergraduate university：
			<label class="radio-inline">
				<strong><span style="width:150px;" id="underGraduateUniversity" data-ng-model="candidate.underGraduateUniversity">USST</span></strong></label>
			</div>	
			<div class="form-group">Location of university：
			<label class="radio-inline">
				<strong><span style="width:150px;" id="locationOfUniversity" data-ng-model="candidate.locationOfUniversity">{{candi}}</span></strong></label>
			</div>	
		  <div class="form-group">Language skill：
				<label class="radio-inline">
				<strong><span style="width:150px;" id="languageSkill" data-ng-model="candidate.languageSkill">CET-6</span></strong></label>
		  </div>	
			<div class="form-group">Major：
			<label class="radio-inline">
				<strong><span style="width:150px;" id="major" data-ng-model="candidate.major">Automation</span></strong></label>
			</div>
			<div class="form-group">E-mail：
			<label class="radio-inline">
				<strong><span style="width:150px;" id="email" data-ng-model="candidate.email">Lucas.lin@pwc.com</span></strong></label>
			</div>
			<div class="form-group">Phone&nbsp;&nbsp;Number：
			<label class="radio-inline">
				<strong><span style="width:150px;" id="phoneNumber" data-ng-model="candidate.phoneNumber">1213311431411</span></strong></label>
			</div>
			<div class="form-group">Veteran status：
			<label class="radio-inline">
			  	<strong><span style="width:150px;" id="veteranStatus" data-ng-model="candidate.veteranStatus">Already served</span></strong></label>
			</div>
		</form>
		<form role="form" style="font-size: 18px;">		
			<div id="jobinformation" style="text-align:center;">
				<h4>Position information</h4>
			</div>
			<HR style="FILTER: alpha(opacity=100,finishopacity=0,style=3);margin-top: 10px;height: 1;" width="80%" color=#fd6709 SIZE=3>
			<div class="form-group">Position applied to：
			<label class="radio-inline">
			 	<strong><span style="width:150px;" id="position" data-ng-model="candidate.position">Java</span></strong></label>
			</div>
			<div class="form-group">Graduation Date：
			<label class="radio-inline">
			 	<strong><span style="width:150px;" id="graduationDate" data-ng-model="candidate.graduationDate">2016.6</span></strong></label>
			</div>
			<div class="form-group">Attendance days：
			<label class="radio-inline">
				<strong><span style="width:150px;" id="attendanceDays" data-ng-model="candidate.attendanceDays">5</span></strong></label>
			</div>
			</form>
<!-- 		<div style="text-align:center;">			 -->
<!-- 			<button class="btn btn-warning" type="button" style="padding: 6px 100px;" >Save</button><br><br> -->
<!-- 		</div> -->
<!-- 		<div class="modal fade" -->
<!-- 			style="vertical-align: middle; text-align: center;" id="mymodal"> -->
<!-- 			<div class="modal-dialog"> -->
<!-- 				<div class="modal-content"> -->
<!-- 					<div class="modal-header"> -->
<!-- 						<button type="button" class="close" data-dismiss="modal"> -->
<!-- 							<span aria-hidden="true">&times;</span><span class="sr-only">Close</span> -->
<!-- 						</button> -->
<!-- 						<h4 class="modal-title">Save</h4> -->
<!-- 					</div> -->
<!-- 					<div class="modal-body"> -->
<!-- 						<p>Are you sure to save?</p> -->
<!-- 					</div> -->
<!-- 					<div class="modal-footer"> -->
<!-- 						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
<!-- 						<a href="javascript:confirmSubmit()"><button type="button" class="btn btn-warning">Sure</button></a> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 				/.modal-content -->
<!-- 			</div> -->
<!-- 			<!-- /.modal-dialog --> -->
<!-- 		</div> -->
		<!-- /.modal -->
	</div>
	<!-- /.container -->
		<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="resources/js/jquery-1.11.3.min.js"></script>
	<script src="resources/js/bootstrap.min.js"></script>
	<script src="resources/js/bootstrap-datetimepicker.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!--<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->
	<script>
	  $(function(){
		$(".btn").click(function(){
		  $("#mymodal").modal("toggle");
		  keyboard:true;
		});
	  });
	</script>
	<script type="text/javascript">
	$('.form_date').datetimepicker({
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0,
		pickerPosition: "bottom-left"
    });
</script>
</body>
</html>