<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
       <%
String path = request.getContextPath();
%>
<!DOCTYPE html>
<html lang="zh-CN" data-ng-app="candidatePage">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">
<link rel='stylesheet' type="text/css" href="<%=path %>/resources/css/WHO_CSS.css">
<link href="<%=path %>/resources/css/starter-template.css" rel="stylesheet">
<link href="<%=path %>/resources/css/font.css" rel="stylesheet">
<link href="<%=path %>/resources/css/bootstrap.min.css" rel="stylesheet">
<script src="resources/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="<%=path %>/resources/js/angular.min.js"></script>
<script type="text/javascript" src="<%=path %>/resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<%=path %>/resources/js/interndetail.js"></script>
<title>list</title>

<!-- Bootstrap core CSS -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="resources/css/starter-template.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<!--<script src="../../assets/js/ie-emulation-modes-warning.js"></script>-->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<script type="text/javascript">
	var selectOrUnselect = false;
	// There used to be a method invoked when the document is ready

	function select_all() {
		var inputs = document.getElementsByTagName("input");
		for (var i = 0; i < inputs.length; i++) {
			if (inputs[i].getAttribute("type") == "checkbox") {
				if (selectOrUnselect) {
					inputs[i].checked = false;
				} else {
					inputs[i].checked = true;
				}
			}
		}
		selectOrUnselect = !selectOrUnselect;
	}
	function selectallornot() {
		var select = document.getElementsByName("select");
		var allbutton = document.getElementById('1');
		for (var i = 0; i < select.length; i++) {
			if (select[i].getAttribute("type") == "checkbox") {
				if (select[i].checked == false) {
					allbutton.checked = false;
					selectOrUnselect = false;
					break;
				} else if (select[i].checked == true) {
					allbutton.checked = true;
					selectOrUnselect = true;
				}
			}
		}
	}
</script>
<body data-ng-controller="candidateController">

	<nav class="navbar navbar-inverse navbar-fixed-top white">

		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar" id="nav">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<div>
				&nbsp;&nbsp;&nbsp;<img src="<%=path %>/resources/images/footer_PWC_Logo_55_42.png">
				</div>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>

	<div class="container">
		<table class="table table-hover">
			<thead>
				<tr>
					<th style="text-align:center;width: 50px">Position</th>
					<th style="text-align:center;width: 50px">Name</th>
					<th style="text-align:center;width: 50px">University</th>
					<th style="text-align:center;width: 50px">Education Background</th>
					<th style="text-align:center;width: 50px">Major</th>
					<th style="text-align:center;width: 20px"><span class="glyphicon glyphicon-trash" data-toggle="modal" data-target="#deleteall"></span></th>
					<th style="text-align:center;width: 20px"><span class="glyphicon glyphicon-pencil"></span></th>
					<th style="text-align:center;width: 20px"><input type="checkbox" value=""  id='1' onclick="select_all()"></th>
				</tr>
			</thead>
			<tbody style="text-align:center;">
				<tr>
					<td style="width: 50px">GW</td>
					<td style="width: 50px"><a data-ng-click="getCandidate()" data-target="#confirm" data-toggle="modal">数据库中数据</a></td>
					<td style="width: 50px">University</td>
					<td style="width: 50px">university diploma</td>
					<td style="width: 50px">CS</td>
					<td style="width: 20px"><span class="glyphicon glyphicon-trash" data-toggle="modal" data-target="#delete"></span></td>
					<td style="width: 20px"><span class="glyphicon glyphicon-pencil"></span></td>
					<td style="width: 20px"><input type="checkbox" value="" name="select" onclick="selectallornot()"></td>
				</tr>
			</tbody>
		</table>
			<div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="vertical-align: middle; text-align: center;">
		   <div class="modal-dialog">
		      <div class="modal-content">
		      	<div class="page-header" style="text-align:center; margin: 10px 0 20px;">
			<h3>Intern Application Form</h3>
			<h4>PwC SDC</h4>
		</div>
	       <div id="personalinformation" style="text-align:center;">
		     <h4>Personal information</h4>
	       </div>
	       <form role="form" style="font-size: 18px;">
			<div class="form-group">Chinese Name：
			  <label class="radio-inline">		
				<strong><span style="width:150px;" id="chineseName" data-ng-model="candidate.chineseName">{{candidate.name}}</span></strong></label>			  
		   </div>
			<div class="form-group">English Name：
			<label class="radio-inline">
				<strong><span style="width:150px;" id="englishName" data-ng-model="candidate.englishName">{{candidate.name}}</span></strong></label>
			</div>
			<div class="form-group">Gender: 
			<label class="radio-inline">
			  	<strong><span style="width:150px;" id="gender" data-ng-model="candidate.gender">{{candidate.gender}}</span></strong></label>
			</div>
			<div class="form-group">ID Number：
			<label class="radio-inline">
				<strong><span style="width:150px;" id="idNumber" data-ng-model="candidate.idNumber">{{candidate.nationalId}}</span></strong></label>
			</div>
			<div class="form-group">Date of birth：
			<label class="radio-inline">
				<strong><span style="width:150px;" id="dateOfBirth" data-ng-model="candidate.dateOfBirth">{{candidate.birthday}}</span></strong></label>
			</div>
			<div class="form-group">Education&nbsp;&nbsp;Background：
			<label class="radio-inline">
				<strong><span style="width:150px;" id="educationBackground" data-ng-model="candidate.educationBackground" data-ng-repeat="eduExp in candidate.educationExperiences">{{eduExp.degree}}</br></span></strong></label>
<!-- 			<ul class="nav navbar-nav" data-ng-repeat="eduExp in candidate.educationExperiences"> -->
<!-- 					<li><a href="#personalinformation"><b>{{eduExp.university}}</b></a></li> -->
<!-- 					<li><a href="#personalinformation"><b>{{eduExp.period}}</b></a></li> -->
<!-- 				</ul> -->
			</div>
			<div class="form-group">Undergraduate university：
			<label class="radio-inline">
				<strong><span style="width:150px;" id="underGraduateUniversity" data-ng-model="candidate.underGraduateUniversity" data-ng-repeat="eduExp in candidate.educationExperiences">{{eduExp.university}}</br></span></strong></label>
			</div>	
			<div class="form-group">Location of university：
			<label class="radio-inline">
				<strong><span style="width:150px;" id="locationOfUniversity" data-ng-model="candidate.locationOfUniversity">{{candidate.currentProvince}}</span></strong></label>
			</div>	
		  <div class="form-group">Language skill：
				<label class="radio-inline">
				<strong><span style="width:150px;" id="languageSkill" data-ng-model="candidate.languageSkill">{{candidate.languageLevel}}</span></strong></label>
		  </div>	
			<div class="form-group">Major：
			<label class="radio-inline">
				<strong><span style="width:150px;" id="major" data-ng-model="candidate.major" data-ng-repeat="eduExp in candidate.educationExperiences">{{eduExp.major}}</br></span></strong></label>
			</div>
			<div class="form-group">E-mail：
			<label class="radio-inline">
				<strong><span style="width:150px;" id="email" data-ng-model="candidate.email">{{candidate.mailAddress}}</span></strong></label>
			</div>
			<div class="form-group">Phone&nbsp;&nbsp;Number：
			<label class="radio-inline">
				<strong><span style="width:150px;" id="phoneNumber" data-ng-model="candidate.phoneNumber">{{candidate.cellPhone}}</span></strong></label>
			</div>
			<div class="form-group">Veteran status：
			<label class="radio-inline">
			  	<strong><span style="width:150px;" id="veteranStatus" data-ng-model="candidate.veteranStatus">Already served</span></strong></label>
			</div>
		</form>
		<form role="form" style="font-size: 18px;">		
			<div id="jobinformation" style="text-align:center;">
				<h4>Position information</h4>
			</div>
			<HR style="FILTER: alpha(opacity=100,finishopacity=0,style=3);margin-top: 10px;height: 1;" width="80%" color=#fd6709 SIZE=3>
			<div class="form-group">Position applied to：
			<label class="radio-inline">
			 	<strong><span style="width:150px;" id="position" data-ng-model="candidate.position">Java</span></strong></label>
			</div>
			<div class="form-group">Graduation Date：
			<label class="radio-inline">
			 	<strong><span style="width:150px;" id="graduationDate" data-ng-model="candidate.graduationDate"  data-ng-repeat="eduExp in candidate.educationExperiences">{{eduExp.period}}</br></span></strong></label>
			</div>
			<div class="form-group">Attendance days：
			<label class="radio-inline">
				<strong><span style="width:150px;" id="attendanceDays" data-ng-model="candidate.attendanceDays">{{candidate.availableSchedule}}</span></strong></label>
			</div>
			</form>
				
		         <div class="modal-footer">
		            <button type="button" class="btn btn-warning" data-dismiss="modal" data-toggle="modal" data-target="#sure" data-ng-click="submitCandidate()" >Sure</button>
		            <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
		         </div>
		      </div><!-- /.modal-content -->
		   </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->	
		
	</div>
	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="resources/js/jquery-1.11.3.min.js"></script>
	<script src="resources/js/bootstrap.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!--<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->
</body>
</html>
