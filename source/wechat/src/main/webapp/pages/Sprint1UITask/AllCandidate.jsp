<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="zh-CN" data-ng-app="allCandidate" data-ng-controller="allCandidate">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">
<link rel='stylesheet' type="text/css"
	href="<%=path %>/resources/css/WHO_CSS.css">
<link href="<%=path %>/resources/css/starter-template.css"
	rel="stylesheet">
<link href="<%=path %>/resources/css/font.css" rel="stylesheet">
<link href="<%=path %>/resources/css/bootstrap.min.css" rel="stylesheet">
<script type="text/javascript" src="<%=path %>/resources/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<%=path %>/resources/js/angular.min.js"/></script>
<script type="text/javascript" src="<%=path %>/resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<%=path %>/resources/js/allcandidate.js"></script>
<script type="text/javascript" src="<%=path %>/resources/js/jquery.jqprint.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.1.0.js"></script>
<title>Print</title>
<div>
				<select  ng-model="sort" ng-options="x for x in selections" ng-change="select()">
					<option value="">Please Select</option>
				</select>
				<button ng-click="disorder()">Disorder</button>
</div>
</head>
  <script language="javascript">
   function print(){
    $("#printContainer").jqprint();
   }
  </script>
<body>
<div id="printContainer">
<div class="container">
		<table class="table table-hover" id="tab1">
			<thead>
				<tr>
					<th style="width: 20px">Name</th>
					<th style="width: 20px">Cell Phone</th>
					<th style="width: 20px">Position</th>
					<th style="width: 20px"></th>
					<th style="width: 50px">University</th>
					<th style="width: 20px">Status</th>
					<th style="width: 20px">Apply Date</th>
					<th style="width: 20px"></th>
					<th style="width: 20px"></th>
				</tr>
					<tr data-ng-repeat="item in items">
						<td data-ng-bind="item.name"></td>
						<td data-ng-bind="item.cell_phone"></td>
						<td data-ng-bind="item.apply_position"></td>
						<td></td>
						<td data-ng-bind="item.university"></td>
						<td data-ng-bind="item.status"></td>
						<td data-ng-bind="item.apply_date"></td>
						<td></td>
				</tr>
			</thead>
		</table>
	</div>
	</div>
	<center><input type="button" onclick="print()" value="Print"/></center>
</body>
</html>
