﻿<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
<title>PwC Wechat - HR Login</title>
<!-- CSS -->
<link rel="stylesheet" href="resources/css/bootstrap.min.css">
<link rel="stylesheet" href="resources/css/font-awesome.min.css">
<link rel="stylesheet" href="resources/css/form-elements.css">
<link rel="stylesheet" href="resources/css/style.css">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
<link rel="icon" href="/wechat/resources/images/icon/favicon.ico">
<LINK href="favicon.ico" type="image/x-icon" rel=icon>
<LINK href="favicon.ico" type="image/x-icon" rel="shortcut icon">
</head>
<body>
	<div class="bg">
		<div class="container">			
			<div class="main col-sm-8 col-md-6 col-md-offset-3 col-sm-offset-2 ">
				<h2><b>HR</b> Login</h2>
				<div class="content">
					<img class="logo" src="resources/images/logo.png">
					<form class="form-horizontal" action="login" method="post">
						<div class="form-group email">
							<label for="username" class="col-md-2 col-sm-2 col-xs-2"><img class="icon" src="resources/images/icon/username.png"></label>
							<div class="col-md-10 col-sm-10 col-xs-10">
								<input name="username" type="text" class="form-control" placeholder="User Name"> 
							</div>							
						</div>
						<div class="form-group password">
							<label for="password" class="col-md-2 col-sm-2 col-xs-2"><img class="icon" src="resources/images/icon/pwd.png"></label>
							<div class="col-md-10 col-sm-10 col-xs-10">
								<input name="password" type="password" class="form-control" placeholder="Password" autocomplete="off"> 
							</div>							
						</div>
						<input class="sign-in btn btn-block" type="submit" value="Sign in">
						<fieldset>
						    <c:if test="${param.error == 'blocked'}">
						        <span class="error-info">Your account has been Blocked ! Please contact admin.</span>
						    </c:if>
						    <c:if test="${param.error == 'wrongPsw'}">
						        <span class="error-info">Invalid username and password. Your have ${5-param.count} chance(s) left.</span>
						    </c:if>
						    <c:if test="${param.error == 'generalUserWrongPsw'}">
						        <span class="error-info">Invalid username and password.</span>
						    </c:if>
						    <c:if test="${param.logout != null}">
						        <span class="error-info">You have been logged out.</span>
						    </c:if>
						</fieldset>
					</form>
				</div>
			</div>			
		</div>
	</div>

	<!-- Javascript -->
	<script src="resources/js/jquery-1.11.3.min.js"></script>
	<script src="resources/js/bootstrap.min.js"></script>
</body>
</html>