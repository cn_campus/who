<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="/wechat/resources/images/icon/favicon.ico">
<link rel='stylesheet' type="text/css"
	href="<c:url value="/resources/css/bootstrap.min.css"/>" />
<link rel='stylesheet' type="text/css"
	href="<c:url value="/resources/css/font.css"/>" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/roundmove.css"/>" />
<title>hrManager</title>
</head>
<body>

	<div class="search-result">
		<div class="form-button">
			<input type="button" class="btn " data-toggle="modal"
				data-target="#myModal3" ng-click="initAddHR()" value="Add a new HR" />
			<input type="button" class="btn "
				ng-click="updateAuthority(userRoles)" value="Save" /> 
		</div>
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Account</th>
					<th>HR</th>
					<th>HRM</th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="userRole in userRoles">
					<td ng-bind="userRole.userName"></td>
					<td><input type="checkbox" class="checkbox"
						id="{{userRole.userName}}-1" ng-model="userRole.hrFlag"
						ng-click="checkHrFlag(userRole)" ng-checked="userRole.hrFlag">
						<label for="{{userRole.userName}}-1"></label></td>
					<td><input type="checkbox" class="checkbox"
						id="{{userRole.userName}}-2" ng-model="userRole.hrmFlag"
						ng-click="checkHrmFlag(userRole)" ng-checked="userRole.hrmFlag">
						<label for="{{userRole.userName}}-2"></label></td>
				</tr>		
			</tbody>
		</table>
		<div class="form-button">
			<input type="button" class="btn "
				ng-click="doUnlockUser(lockedAccounts)" value="Save" /> 
		</div>
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Locked Account</th>
					<th>isUnLocked</th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="lockedAccount in lockedAccounts">
						<td ng-bind="lockedAccount.userName"></td>
						<td><input type="checkbox" class="checkbox"
						id="{{lockedAccount.userName}}" ng-model="lockedAccount.enabled"
						ng-checked="lockedAccount.enabled">
						<label for="{{lockedAccount.userName}}"></label></td>
				</tr>	
			</tbody>
		</table>
	</div>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h2 class="modal-title" id="myModalLabel">
						<font color="#FF8000">Authorization</font>
					</h2>
				</div>
				<div id="checkform" class="modal-body">
					<h3>
						<font color="#FF8040">pwc@pwc.com</font>
					</h3>
					<br> <br>Type: &nbsp;&nbsp;
					<h4>HR</h4>
					<input type="checkBox" class="checkbox" onClick="checkbox(0)">
					<br>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<h4>HRM</h4>
					<input type="checkBox" class="checkbox" checked
						onClick="checkbox(1)">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel
					</button>
					<button type="button" class="btn btn-primary" data-toggle="modal"
						data-target="#myModal2" data-dismiss="modal">Confirm</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
	<div class="modal fade" id="myModal2" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h2 class="modal-title" id="myModalLabel">
						<font color="#FF8000">Authorization</font>
					</h2>
				</div>
				<div>
					<br>
					<h4 class="modal-title" id="myModalLabel" align="center">
						<font color="#FFD306">Authorizate Success!</font>
					</h4>
					<br>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close
					</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
	<div class="modal fade" id="myModal3" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h3 class="modal-title" id="myModalLabel">
						<font color="#FF8000">Add a new HR Account</font>
					</h3>
				</div>
				<form name="newHrForm" method="post">
					<div class="modal-body">
						<ul class="changePwd_ul">
							<li class="changePwd_head">
								<div class="title">Password rules: Must contain at least
									one of each of the following characters</div>
								<div class="title">(1) Upper and lower case letter</div>
								<div class="title">(2) Number and special characters e.g:
									!,@,$,~,%</div>
							</li>
							<li class="changePwd_content">
								<div class="col-md-4">
									<span for="name">*HR Name:</span>
								</div>
								<div class="col-md-4">
									<input name="userName" ng-model="user.userName"
										ng-bind="checkNm" ng-style="namestyle" ng-change="checkName()"
										required placeholder="userName@pwc.com" />
								</div>
								<div class="col-md-4">
									<span for="initialHrName" ng-show="showname">HR name is
										invaild</span>
								</div>
							</li>
							<li class="changePwd_content">
								<div class="col-md-4">
									<span for="initialPassword">*Initial Password:</span>
								</div>
								<div class="col-md-4">
									<input ng-model="user.password" ng-bind="checkPw"
										type="password" ng-style="pwstyle" ng-change="checkPassword()"
										autocomplete="off" />
								</div>
								<div class="col-md-4">
									<span for="initialPassword" ng-show="showpw">Password is
										invaild</span>
								</div>
							</li>
							<li class="changePwd_content">
								<div class="col-md-4">
									<span for="confirmPassword">*Confirm Password:</span>
								</div>
								<div class="col-md-4">
									<input ng-model="confirmpw" ng-bind="checkConfirm"
										type="password" ng-style="confirmstyle"
										ng-change="confirmPw()" autocomplete="off" />
								</div>
								<div class="col-md-4">
									<span for="confirmPassword" ng-show="showconfirm">please
										confirm password</span>
								</div>
							</li>
						</ul>
					</div>
				</form>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel
					</button>
					<button type="button" class="btn btn-warning" data-toggle="modal"
						ng-click="addNewUser(user)"
						ng-disabled="!(checkNm&&checkPw&&checkConfirm)"
						data-target="#myModal4" data-dismiss="modal">Complete</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
	<div class="modal fade" id="myModal4" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h3 class="modal-title" id="myModalLabel">
						<font color="#FF8000">Authorization</font>
					</h3>
				</div>
				<div>
					<br>
					<h4 class="modal-title" id="myModalLabel" align="center">
						<font>Saved!</font>
					</h4>
					<br>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close
					</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
